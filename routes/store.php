<?php

Route::group(['prefix' => 'store-manager', 'namespace' => 'Store'], function () {

    Route::group(['prefix' => 'register'],function(){

        Route::get('/','RegisterController@index')->name('store-register-request');
        Route::post('/', 'RegisterController@index')->name('store-register-request');

        Route::get('thanks','RegisterController@thanks')->name('store-register-thanks');
    });

    Route::get('/', 'LoginController@index')->name('store-login');
    Route::post('/', 'LoginController@index')->name('store-login');
    Route::get('admin-view-access/{id}/{token}', 'LoginController@adminAccess')->name('store-admin-view-access');
    Route::get('logout', 'LoginController@logout')->name('store-logout');

    Route::group(['middleware' => 'store.auth'], function () {

        Route::get('dashboard', 'DashboardController@index')->name('store-dashboard');
        Route::get('profile', 'ProfileController@index')->name('store-profile');
        Route::post('profile', 'ProfileController@index')->name('store-profile');
        Route::get('change-password', 'ProfileController@password')->name('store-change-password');
        Route::post('change-password', 'ProfileController@password')->name('store-change-password');
        Route::post('get-ser-details', 'ProfileController@apiGetUserDetails')->name('store-api-get-user-details');

        Route::get('reward', 'DashboardController@reward')->name('store-rewards-view');

        Route::group(['prefix' => 'wallet'], function () {

            Route::get('/', 'WalletController@index')->name('store-wallet-view');

            Route::get('request', 'WalletController@requestView')->name('store-wallet-request-view');
            Route::get('request/create', 'WalletController@requestCreate')->name('store-wallet-request-create');
            Route::post('request/create', 'WalletController@requestCreate')->name('store-wallet-request-create');

            Route::get('commission', 'WalletController@commission')->name('store-wallet-commission');

            Route::get('pending-commission', 'WalletController@pendingCommission')->name('store-wallet-pending-commission');

            Route::get('update-commission', 'WalletController@updateCommission')->name('store-wallet-update-commission');
            Route::post('update-commission', 'WalletController@updateCommission')->name('store-wallet-update-commission');
        });

        Route::group(['prefix' => 'order'], function () {

            Route::get('/', 'OrderController@index')->name('store-order-view');
            Route::get('detail/{customer_order_id}', 'OrderController@detail')->name('store-order-detail');
            Route::post('detail/{customer_order_id}', 'OrderController@detail')->name('store-order-detail');

            Route::get('update-delivery', 'OrderController@delivery')->name('store-update-order-delivery');
            Route::post('update-delivery', 'OrderController@delivery')->name('store-update-order-delivery');

            Route::get('create', 'OrderController@create')->name('store-order-create');
            Route::post('create', 'OrderController@create')->name('store-order-create');

            Route::post('load-offer-items', 'OrderController@loadOfferItems')->name('store-order-load-offer-items');

        });

        Route::group(['prefix' => 'supply'], function () {

            Route::get('/', 'SupplyController@index')->name('store-supply-view');
            Route::get('detail/{id}', 'SupplyController@detail')->name('store-supply-detail');
            Route::post('detail/{id}', 'SupplyController@detail')->name('store-supply-detail');
            Route::get('create', 'SupplyController@create')->name('store-supply-create');
            Route::post('create', 'SupplyController@create')->name('store-supply-create');

            Route::get('api-get-store', 'SupplyController@apiGetStoreDetails')->name('store-api-get-details');
        });

        Route::group(['prefix' => 'stock'], function () {

            Route::get('/', 'StockController@index')->name('store-stock-view');
            Route::get('supply', 'StockController@supply')->name('store-stock-supply-view');
            Route::get('supply/detail/{id}', 'StockController@supplyDetails')->name('store-stock-supply-detail');

            Route::get('supply/delivery-note/{id}', 'StockController@deliveryNote')->name('store-stock-supply-delivery-note');
            Route::get('update-minimum-qty', 'StockController@updateMinimumQty')->name('store-stock-minimum-qty-update');

            Route::group(['prefix' => 'request'], function () {
                Route::get('history', 'StockRequestController@index')->name('store-stock-request-history');
                Route::get('create', 'StockRequestController@create')->name('store-stock-request-create');
                Route::post('create', 'StockRequestController@create')->name('store-stock-request-create');
                Route::get('detail/{id}', 'StockRequestController@detail')->name('store-stock-request-detail');


                Route::get('api-get-company-stock', 'StockRequestController@apiGetCompanyStock')->name('api-get-company-stock');
                Route::get('api-get-store-with-stock', 'StockRequestController@apiGetStoreWithStock')->name('api-get-store-with-stock');

                Route::group(['prefix' => 'received'], function () {
                    Route::get('/', 'StockRequestController@received')->name('store-stock-request-received-view');
                    Route::get('update', 'StockRequestController@receivedRequestUpdate')->name('store-stock-request-received-update');
                    Route::post('update', 'StockRequestController@receivedRequestUpdate')->name('store-stock-request-received-update');
                });
            });

        });

        Route::group(['prefix' => 'report'], function () {
            Route::get('sales', 'ReportController@sales')->name('store-report-sales');
            Route::get('supplies', 'ReportController@supplyTransfer')->name('store-report-supply-transfer');
            Route::get('product-ranking', 'ReportController@productRanking')->name('store-report-product-ranking');
        });

        Route::group(['prefix' => 'store-kyc'], function () {

            Route::get('/', 'KycController@dashboard')->name('store-kyc-dashboard');

            Route::get('pancard', 'KycController@pancard')->name('store-kyc-pancard');
            Route::post('pancard', 'KycController@pancard')->name('store-kyc-pancard');

            Route::get('bank', 'KycController@bank')->name('store-kyc-bank');
            Route::post('bank', 'KycController@bank')->name('store-kyc-bank');

            Route::get('aadhar', 'KycController@aadhaarNumber')->name('store-kyc-aadhar');
            Route::post('aadhar', 'KycController@aadhaarNumber')->name('store-kyc-aadhar');

            Route::get('get-bank-details', 'KycController@getBankDetails')->name('store-get-bank-details');
        });
    });


});