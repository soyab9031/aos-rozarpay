<?php

Route::group(['namespace' => 'Admin', 'prefix' => 'admin-panel'], function () {


    Route::get('/', 'LoginController@index')->name('admin-login');
    Route::post('/', 'LoginController@index')->name('admin-login');
    Route::get('logout', 'LoginController@logout')->name('admin-logout');

    Route::group(['middleware' => 'admin.auth'], function () {

        Route::get('dashboard', 'DashboardController@index')->name('admin-dashboard');
        Route::get('access-denied', 'DashboardController@accessDenied')->name('admin-access-denied');

        Route::group(['prefix' => 'manager'], function () {

            Route::get('/', 'AdminController@index')->name('admin-manager-view');
            Route::get('create', 'AdminController@create')->name('admin-manager-create');
            Route::post('create', 'AdminController@create')->name('admin-manager-create');
            Route::get('update/{id}', 'AdminController@update')->name('admin-manager-update');
            Route::post('update/{id}', 'AdminController@update')->name('admin-manager-update');

            Route::get('profile', 'AdminController@profile')->name('admin-manager-profile');
            Route::post('profile', 'AdminController@profile')->name('admin-manager-profile');

        });

        Route::group(['prefix' => 'user'], function () {

            Route::get('/', 'UserController@index')->name('admin-user-view');
            Route::get('update/{id}', 'UserController@update')->name('admin-user-update');
            Route::post('update/{id}', 'UserController@update')->name('admin-user-update');
            Route::get('blocked', 'UserController@blockedUsers')->name('admin-user-blocked-view');
            Route::get('payment-stop', 'UserController@paymentStoppedUsers')->name('admin-user-payment-stop-view');
            Route::get('admin-access/{id}', 'UserController@accountAccess')->name('admin-user-account-access');

            /* Api */
            Route::get('get', 'UserController@getUser')->name('admin-api-get-user');

        });

        Route::group(['prefix' => 'customer'], function () {

            Route::get('/', 'CustomerController@index')->name('admin-customer-view');
            Route::get('update/{id}', 'CustomerController@update')->name('admin-customer-update');
            Route::post('update/{id}', 'CustomerController@update')->name('admin-customer-update');
            Route::get('admin-access/{id}', 'CustomerController@accountAccess')->name('admin-customer-account-access');

            /* Api */
            Route::get('get', 'CustomerController@apiGetCustomer')->name('admin-api-get-customer');

        });

        // --- Pin Module --- //
        Route::group(['prefix' => 'pin'], function () {

            Route::get('/', 'PinController@index')->name('admin-pin-view');
            Route::get('create', 'PinController@create')->name('admin-pin-create');
            Route::post('create', 'PinController@create')->name('admin-pin-create');
            Route::get('update/{id}', 'PinController@update')->name('admin-pin-update');
            Route::post('update/{id}', 'PinController@update')->name('admin-pin-update');

            Route::get('transfer-history/{id}', 'PinController@transfer_history')->name('admin-pin-transfer-history');

            Route::get('request-view', 'PinController@requestView')->name('admin-pin-request-view');
            Route::get('request-view/update/{id}', 'PinController@requestUpdate')->name('admin-pin-request-update');
            Route::post('request-view/update/{id}', 'PinController@requestUpdate')->name('admin-pin-request-update');
            Route::post('use','PinController@usePin')->name('admin-pin-use');
        });

        // --- Package Module --- //
        Route::group(['prefix' => 'package'], function () {

            Route::get('/', 'PackageController@index')->name('admin-package-view');
            Route::get('create', 'PackageController@create')->name('admin-package-create');
            Route::post('create', 'PackageController@create')->name('admin-package-create');
            Route::get('update/{id}', 'PackageController@update')->name('admin-package-update');
            Route::post('update/{id}', 'PackageController@update')->name('admin-package-update');

            Route::group(['prefix' => 'items'], function () {

                Route::get('view/{id}', 'PackageController@items')->name('admin-package-items-view');
                Route::post('view/{id}', 'PackageController@items')->name('admin-package-items-view');

                Route::get('update/{id}', 'PackageController@itemUpdate')->name('admin-package-items-update');
                Route::post('update/{id}', 'PackageController@itemUpdate')->name('admin-package-items-update');

            });

        });

        Route::group(['prefix' => 'orders'], function () {

            Route::get('/', 'OrderController@index')->name('admin-shopping-order-view');
            Route::get('details/{customer_order_id}', 'OrderController@details')->name('admin-shopping-order-details');
        });

        Route::group(['prefix' => 'sms'], function() {
            Route::get('report', 'SmsController@index')->name('admin-sms-report');
        });

        Route::group(['prefix' => 'support'], function(){

            Route::get('user', 'SupportController@support')->name('admin-user-support');

            Route::get('user/chat/{id}', 'SupportController@supportChat')->name('admin-user-support-chat');
            Route::post('user/chat/{id}', 'SupportController@supportChat')->name('admin-user-support-chat');

            Route::get('user/category/{id?}', 'SupportController@supportCategory')->name('admin-user-support-category');
            Route::post('user/category/{id?}', 'SupportController@supportCategory')->name('admin-user-support-category');


            Route::get('website', 'SupportController@website')->name('admin-website-support');
            Route::get('grievance', 'SupportController@grievance')->name('admin-website-grievance');
        });

        Route::group(['prefix' => 'business', 'namespace' => 'Business'], function () {

            Route::get('sponsors', 'BusinessController@sponsors')->name('admin-business-sponsors-view');

            Route::group(['prefix' => 'binary'], function () {

                Route::get('tree', 'BinaryController@index')->name('admin-business-binary-tree');
                Route::get('status', 'BinaryController@status')->name('admin-business-binary-status');
                Route::get('api-tree-suggestions', 'BinaryController@searchSuggestion')->name('admin-api-business-binary-tree-suggestions');

            });

        });

        Route::group(['prefix' => 'wallet'], function() {

            Route::get('report', 'WalletController@index')->name('admin-wallet-report');
            Route::get('status', 'WalletController@status')->name('admin-wallet-status');
            Route::get('credit-debit', 'WalletController@creditDebit')->name('admin-wallet-transaction');
            Route::post('credit-debit', 'WalletController@creditDebit')->name('admin-wallet-transaction');
            Route::get('balance-report', 'WalletController@balanceReport')->name('admin-wallet-balance-report');

        });

        Route::group(['prefix' => 'payout'], function () {

            Route::get('/', 'PayoutController@index')->name('admin-payout-view');
            Route::get('update/{id}', 'PayoutController@update')->name('admin-payout-update');
            Route::post('update/{id}', 'PayoutController@update')->name('admin-payout-update');

            Route::get('bulk-upload', 'PayoutController@bulkUpdate')->name('admin-payout-bulk-update');
            Route::post ('bulk-upload', 'PayoutController@bulkUpdate')->name('admin-payout-bulk-update');

            Route::get('create', 'PayoutController@create')->name('admin-payout-create');
            Route::post('create', 'PayoutController@create')->name('admin-payout-create');

            Route::get('tds-report', 'PayoutController@tdsReport')->name('admin-payout-tds-report');

        });


        Route::group(['prefix' => 'document'], function(){
            Route::get('/', 'DocumentController@index')->name('admin-document-view');
            Route::get('update/{id}', 'DocumentController@update')->name('admin-document-update');
            Route::post('update/{id}', 'DocumentController@update')->name('admin-document-update');
        });


        Route::group(['prefix' => 'news'], function(){
            Route::get('/', 'NewsController@index')->name('admin-news');
            Route::get('create', 'NewsController@create')->name('admin-news-create');
            Route::post('create', 'NewsController@create')->name('admin-news-create');
            Route::get('update/{id}', 'NewsController@update')->name('admin-news-update');
            Route::post('update/{id}', 'NewsController@update')->name('admin-news-update');
        });

        Route::group(['prefix' => 'web-manager'], function(){

            Route::get('/', 'WebContentController@index')->name('admin-web-content-view');

            Route::get('update-web-content/{id}', 'WebContentController@update')->name('admin-web-content-update');
            Route::post('update-web-content/{id}', 'WebContentController@update')->name('admin-web-content-update');
        });

        Route::group(['prefix' => 'image-manager'], function(){

            Route::group(['prefix' => 'popup'], function(){
                Route::get('/','ImageController@viewPopup')->name('admin-popup-view');

                Route::get('/create', 'ImageController@createPopup')->name('admin-popup-create');
                Route::post('/create', 'ImageController@createPopup')->name('admin-popup-create');

                Route::get('/update', 'ImageController@updatePopup')->name('admin-popup-update');
                Route::post('/update', 'ImageController@updatePopup')->name('admin-popup-update');
            });

            Route::get('banners', 'ImageController@viewBanner')->name('admin-banner-view');

            Route::get('create-banner', 'ImageController@createBanner')->name('admin-banner-create');
            Route::post('create-banner', 'ImageController@createBanner')->name('admin-banner-create');
            Route::get('update-banner/{id}', 'ImageController@updateBanner')->name('admin-banner-update');
            Route::post('update-banner/{id}', 'ImageController@updateBanner')->name('admin-banner-update');

            Route::group(['prefix' => 'gallery'], function(){

                Route::get('/', 'ImageController@viewGallery')->name('admin-gallery-view');
                Route::get('create', 'ImageController@createGallery')->name('admin-gallery-create');
                Route::post('create', 'ImageController@createGallery')->name('admin-gallery-create');

                Route::get('update/{id}', 'ImageController@updateGallery')->name('admin-gallery-update');
                Route::post('update/{id}', 'ImageController@updateGallery')->name('admin-gallery-update');

            });

            Route::group(['prefix' => 'documents'],function(){

                Route::get('/','CompanyDocumentController@index')->name('admin-company-documents-view');
                Route::get('create','CompanyDocumentController@create')->name('admin-company-documents-create');
                Route::post('create','CompanyDocumentController@create')->name('admin-company-documents-create');

                Route::get('update','CompanyDocumentController@update')->name('admin-company-documents-update');
                Route::post('update','CompanyDocumentController@update')->name('admin-company-documents-update');
            });

        });

        Route::group(['prefix' => 'blogs'], function() {
            Route::get('/', 'BlogController@index')->name('admin-blog-view');
            Route::get('create', 'BlogController@create')->name('admin-blog-create');
            Route::post('create', 'BlogController@create')->name('admin-blog-create');
            Route::get('update/{id}', 'BlogController@update')->name('admin-blog-update');
            Route::post('update/{id}', 'BlogController@update')->name('admin-blog-update');
        });

        /*Products*/
        Route::group(['prefix' => 'products'],function () {

            Route::group(['prefix' => 'category'],function (){

                Route::get('/', 'CategoryController@index')->name('admin-category-view');
                Route::get('create','CategoryController@create')->name('admin-category-create');
                Route::post('create','CategoryController@create')->name('admin-category-create');
                Route::get('update/{id}','CategoryController@update')->name('admin-category-update');
                Route::post('update/{id}','CategoryController@update')->name('admin-category-update');
                Route::get('get-children', 'CategoryController@apiGetCategories')->name('admin-api-category-get-children');


            });

            Route::get('/','ProductController@index')->name('admin-product-view');

            Route::get('create','ProductController@create')->name('admin-product-create');
            Route::post('create','ProductController@create')->name('admin-product-create');

            Route::get('update/{id}','ProductController@update')->name('admin-product-update');
            Route::post('update/{id}','ProductController@update')->name('admin-product-update');

            Route::get('new-arrival','ProductController@newArrival')->name('admin-new-arrival-list');
            Route::post('new-arrival','ProductController@newArrival')->name('admin-new-arrival-list');

            Route::get('deal-of-products','ProductController@DealProducts')->name('admin-deal-of-products-list');
            Route::post('deal-of-products','ProductController@DealProducts')->name('admin-deal-of-products-list')
            ;
            Route::get('recommended','ProductController@Recommended')->name('admin-recommended-list');
            Route::post('recommended','ProductController@Recommended')->name('admin-recommended-list');

        });

        Route::group(['prefix' => 'reports'],function (){

            Route::get('top-earners','ReportController@topEarner')->name('admin-report-top-earners');
            Route::get('sales','ReportController@sales')->name('admin-report-sales');
            Route::get('package-ranking','ReportController@packageRanking')->name('admin-report-package-ranking');

        });

        Route::group(['prefix' => 'settings'], function () {

            Route::get('system/{name}', 'SettingController@index')->name('admin-system-settings');
            Route::post('system/{name}', 'SettingController@index')->name('admin-system-settings');

            Route::get('faqs', 'SettingController@faqsView')->name('admin-setting-faq-view');
            Route::get('faqs/create', 'SettingController@faqsCreate')->name('admin-setting-faq-create');
            Route::post('faqs/create', 'SettingController@faqsCreate')->name('admin-setting-faq-create');
            Route::get('faqs/update/{id}', 'SettingController@faqsUpdate')->name('admin-setting-faq-update');
            Route::post('faqs/update/{id}', 'SettingController@faqsUpdate')->name('admin-setting-faq-update');

        });

        Route::group(['prefix' => 'events'], function() {

            Route::get('/','EventController@index')->name('admin-events-view');

            Route::get('create','EventController@create')->name('admin-event-create');
            Route::post('create','EventController@create')->name('admin-event-create');

            Route::get('update/{id}','EventController@update')->name('admin-event-update');
            Route::post('update/{id}','EventController@update')->name('admin-event-update');

            Route::get('category/{id?}', 'EventController@category')->name('admin-event-category');
            Route::post('category/{id?}', 'EventController@category')->name('admin-event-category');

        });

    });

});
