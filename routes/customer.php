<?php

Route::group(['namespace' => 'Customer', 'prefix' => 'customer'], function () {

    Route::get('register/{referral?}', 'RegisterController@index')->name('customer-register');
    Route::post('register/{referral?}', 'RegisterController@index')->name('customer-register');

    Route::get('/', 'LoginController@index')->name('customer-login');
    Route::post('/', 'LoginController@index')->name('customer-login');

    Route::get('logout', 'LoginController@logout')->name('customer-logout');

    Route::get('forget-password', 'LoginController@forgetPassword')->name('customer-forget-password');
    Route::post('forget-password', 'LoginController@forgetPassword')->name('customer-forget-password');


    Route::get('admin-view-access/{id}/{token}', 'LoginController@adminViewAccess')->name('customer-admin-view-access');


    Route::group(['middleware' => 'customer.auth'], function () {

        Route::get('dashboard', 'DashboardController@index')->name('customer-dashboard');

        Route::group(['prefix' => 'profile'], function () {

            Route::get('/','DashboardController@profile')->name('customer-profile');
            Route::post('/','DashboardController@profile')->name('customer-profile');
        });

        Route::group(['prefix' => 'order'], function () {

            Route::get('/','OrderController@index')->name('customer-shopping-order-view');
            Route::get('details/{customer_order_id}','OrderController@details')->name('customer-shopping-order-details');

//            Route::get('invoice','OrderController@invoiceView')->name('customer-shopping-order-invoice');
//            Route::get('invoice-details/{invoice_number}','OrderController@invoiceDetails')->name('customer-shopping-order-invoice-details');

        });

    });


});