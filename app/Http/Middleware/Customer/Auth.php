<?php

namespace App\Http\Middleware\Customer;

use App\Library\UserWebsite\UwSession;
use Closure;

class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!\Session::has('customer'))
            return redirect()->route('user-login')->with(['error' => 'Session Expired!, Please Login Again']);

        return $next($request);
    }
}
