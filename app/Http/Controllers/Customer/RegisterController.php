<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\User;
use App\Models\UserAddress;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function index(Request $request)
    {
        $referralUser = null;

        if (isset($request->referral)) {
            $referralUser = User::whereTrackingId($request->referral)->first();
        }

        if ($request->isMethod('post')) {

            $this->validate($request, [
//                'referral_user_tracking_id' => 'required|exists:users,tracking_id',
                'name' => 'required',
                'email' => 'required|email',
                'mobile' => 'required|regex:/^[9876][0-9]{9}$/|digits:10|unique:customers,mobile',
                'birth_date' => 'required',
                'password' => 'required|confirmed|min:6'
            ], [
//                'referral_user_tracking_id.required' => 'Referral User ID is required',
//                'referral_user_tracking_id.exists' => 'Invalid Referral User ID, Try again',
                'name.required' => 'Name is Required',
                'email.required' => 'Email Address is required',
                'email.email' => 'Invalid Email Address',
                'mobile.required' => 'Mobile Number is Required',
                'mobile.regex' => 'Invalid Mobile Number',
                'mobile.digits' => 'Mobile Number must be 10 digits',
                'birth_date.required' => 'Date of Birth is Required',
                'password.required' => 'Password is required',
                'password.min' => 'Password length should be 6 or more',
            ]);

//            $referralUser = User::whereTrackingId($request->referral_user_tracking_id)->first();

            $tracking_id = 'AOSC'.rand(1000000, 9999999);

            while(Customer::whereTrackingId($tracking_id)->exists()) {
                $tracking_id = 'AOSC'.rand(1000000, 9999999);
            }

            $customer = Customer::create([
                'tracking_id' => $tracking_id,
                'user_id' => null,
                'name' => $request->name,
                'email' => $request->email,
                'mobile' => $request->mobile,
                'birth_date' => Carbon::parse($request->birth_date),
                'password' => $request->password
            ]);

            UserAddress::create([
                'customer_id' => $customer->id
            ]);

            return redirect()->route('customer-login')
                ->with(['success' => 'Customer Registration has been successfully completed!']);
        }
        return view('website.customer.authentication.register.index', [
            'referralUser' => $referralUser
        ]);
    }
}
