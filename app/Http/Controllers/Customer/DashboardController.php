<?php

namespace App\Http\Controllers\Customer;

use App\Models\Customer;
use App\Models\Order;
use App\Http\Controllers\Controller;
use App\Models\State;
use App\Models\UserAddress;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $customer = Customer::whereId(\Session::get('customer')->id)->first();
        $order_count = Order::whereCustomerId($customer->id)->count();

        return view('customer.dashboard', [
            'customer' => $customer,
            'order_count' => $order_count,
        ]);
    }

    public function profile(Request $request)
    {
        $customer = Customer::with('address')->whereId(\Session::get('customer')->id)->first();

        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'city' => 'required',
                'state_id' => 'required',
            ], [
                'city.required' => 'City is Required',
                'state_id.required' => 'State is Required',
            ]);

            $customer->birth_date = Carbon::parse($request->birth_date)->format('Y-m-d');
            $customer->save();

            $customer_detail = UserAddress::whereCustomerId($customer->id)->first();
            $customer_detail->address = $request->address;
            $customer_detail->landmark = $request->landmark;
            $customer_detail->city = $request->city;
            $customer_detail->district = $request->district;
            $customer_detail->state_id = $request->state_id;
            $customer_detail->pincode = $request->pincode;
            $customer_detail->save();

            return redirect()->route('customer-profile')->with(['success' => 'Your Profile has been Updated!!..']);
        }

        return view('customer.profile.index',[
            'customer' => $customer,
            'states' => State::active()->get(),
        ]);
    }
}
