<?php

namespace App\Http\Controllers\Customer;

use App\Library\Helper;
use App\Library\Sms;
use App\Models\Admin;
use App\Models\Customer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function index(Request $request)
    {
        if ($request->isMethod('post')) {

            $validator = \Validator($request->all(), [
                'mobile' => 'required|exists:customers,mobile|regex:/^[9876][0-9]{9}$/|digits:10',
                'password' => 'required'

            ], [
                'mobile.required' => 'Mobile Number is Required',
                'mobile.exists' => 'Customer Mobile Number is not exists, Create New Account.',
                'mobile.regex' => 'Invalid Mobile Number',
                'password.required' => 'Password is Required'
            ]);

            if ($validator->fails()) {
                return redirect()->route('customer-login')->with(['error' => $validator->errors()->first()]);
            }

            $customer = Customer::whereMobile($request->mobile)->first();

            if (!$customer)
                return redirect()->route('customer-login')->with(['error' => 'Invalid Mobile Number']);

            if ($request->password != $customer->password)
                return redirect()->route('customer-login')->with(['error' => 'Password is Not Match with Mobile Number!']);

            \Session::forget('user');

            \Session::put('customer', $customer);

            Customer::whereId($customer->id)->update([
                'last_logged_in_ip' => Helper::getClientIp(),
                'last_logged_in_at' => Carbon::now()
            ]);

            if (\Session::has('order-checkout') && $request->redirect_route == 'website-checkout-shipping-address') {
                return redirect()->route('website-cart')->with(['success' => 'Click Continue to Process the Order']);
            }

            return redirect()->route('customer-dashboard')->with([
                'success' => 'Welcome to ' . config('project.brand') . ''
            ]);

        }

        return view('website.customer.authentication.login');

    }

    public function forgetPassword(Request $request)
    {
        if ($request->isMethod('post')) {

            $this->validate($request, [
                'email' => 'required|exists:customers,email',
                'mobile' => 'required|exists:customers,mobile|regex:/^[9876][0-9]{9}$/|digits:10',
            ], [
                'email.required' => 'Email ID is required to get New Password',
                'email.exists' => 'Invalid Email ID',
                'mobile.required' => 'Mobile No. is required to get New Password',
                'mobile.exists' => 'Invalid mobile no.',
                'mobile.digits' => 'Mobile Number must be 10 digits',
            ]);


            if (!$customer = Customer::whereEmail($request->email)->whereMobile($request->mobile)->first())
                return redirect()->back()->withInput()->with(['error' => 'This Number is not registered with this Customer']);

            $new_password = strtoupper(\Str::random(8));

            $customer->password = $new_password;
            $customer->save();

//            (new Sms())->to($customer)->template('forgot_password_customer', [
//                $new_password, $customer->mobile
//            ])->sendOne();

            return redirect()->route('customer-login')
                ->with(['success' => 'New Password has been sent on Your Registered Mobile Number.']);

        }
    }

    public function logout()
    {
        \Session::forget('customer');

        return redirect()->route('customer-login');
    }

    public function adminViewAccess(Request $request)
    {
        if (!\Session::has('admin'))
            return redirect()->back()->with(['error' => 'Invalid Credentials for Customer Access']);

        if (!Admin::whereToken($request->token)->exists())
            return redirect()->back()->with(['error' => 'Invalid token for Customer Access']);

        \Session::forget('user');
        \Session::forget('customer');
        \Session::forget('wallet_session');

        if (!$customer = Customer::whereId($request->id)->first())
            return redirect()->back()->with(['error' => 'Invalid Credentials for Customer Access']);

        \Session::put('customer', $customer);

        return redirect()->route('customer-dashboard');
    }
}
