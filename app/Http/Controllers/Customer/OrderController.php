<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Setting;
use App\Models\State;
use Illuminate\Http\Request;
use Knp\Snappy\Pdf;

class OrderController extends Controller
{
    public function index()
    {
        $customer_id = \Session::get('customer')->id;

        $orders = Order::whereCustomerId($customer_id)->where('created_at', '>', '2022-01-11 00:00:00')->with(['user.detail', 'customer'])->latest()->paginate(30);

        return view('customer.orders.index', [
            'orders' => $orders
        ]);
    }

    public function details(Request $request)
    {
        $customer_id = \Session::get('customer')->id;

        $order = Order::whereCustomerId($customer_id)->with(['customer','details.product_price.product'])
            ->whereCustomerOrderId($request->customer_order_id)->first();

        if (!$order)
            return redirect()->back()->with(['error' => 'Not able to find the Order details, try again']);


        return view('customer.orders.details', [
            'order' => $order
        ]);
    }
}
