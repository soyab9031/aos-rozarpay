<?php

namespace App\Http\Controllers\Store;

use App\Models\State;
use App\Models\StoreManager\Store;
use App\Models\StoreManager\StoreType;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function index(Request $request)
    {
        $store = Store::whereId(\Session::get('store')->id)->first();

        if ($request->isMethod('post')) {

            $this->validate($request, [
                'address' => 'required',
                'city' => 'required',
                'state_id' => 'required',
                'pincode' => 'required|digits:6',
            ], [
                'address.required' => 'Address is Required',
                'city.required' => 'City is Required',
                'state_id.required' => 'State is Required',
                'pincode.required' => 'Pincode is Required',
                'pincode.digits' => 'Pincode Must be 6 digit number',
            ]);

            $store->address = $request->address;
            $store->city = $request->city;
            $store->pincode = $request->pincode;
            $store->state_id = $request->state_id;
            $store->save();

            return redirect()->route('store-profile')->with(['success' => 'Store Profile is Updated Successfully']);

        }

        return view('store-manager.profile.index', [
            'store' => $store,
            'states' => State::active()->get(),
        ]);
    }

    public function password(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'old_password' => 'required|min:6|exists:stores,password,id,'.\Session::get('store')->id,
                'new_password' => 'required|min:6'
            ], [
                'old_password.required' => 'Old Password is required',
                'old_password.min' => 'Old Password length should be 6 or more',
                'old_password.exists' => 'Invalid Old Password',
                'new_password.required' => 'New Password is required',
                'new_password.min' => 'New Password length should be 6 or more'
            ]);

            $user = Store::find(\Session::get('store')->id);
            $user->password = $request->new_password;
            $user->save();

            return redirect()->route('store-dashboard')->with(['success' => 'Password has been Updated.']);
        }

        return view('store-manager.profile.change-password');
    }

    public function apiGetUserDetails(Request $request)
    {
        if (!$user = User::whereTrackingId($request->tracking_id)->with(['detail:user_id,title,first_name,last_name'])->first())
            return response()->json(['status' => false, 'message' => 'Invalid User/Member Details, try again']);

        return response()->json([
            'status' => true,
            'user' => [
                'id' => $user->id,
                'tracking_id' => $user->tracking_id,
                'name' => $user->detail->full_name,
                'mobile' => $user->mobile,
            ]
        ]);
    }
}
