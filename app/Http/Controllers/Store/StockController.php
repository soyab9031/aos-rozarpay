<?php

namespace App\Http\Controllers\Store;

use App\Models\Setting;
use App\Models\StoreManager\MinimumStock;
use App\Models\StoreManager\StoreStockTransaction;
use App\Models\StoreManager\StoreSupply;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Knp\Snappy\Pdf;

class StockController extends Controller
{
    public function index(Request $request)
    {
        $difference_query = 'SUM(COALESCE(CASE WHEN type = 1 THEN qty END,0)) - SUM(COALESCE(CASE WHEN type = 2 THEN qty END,0))';

        $records = StoreStockTransaction::selectRaw('product_price_id, ' . $difference_query .' as balance')->with([
            'product_price.product:id,name',
            'minimum_stock' => function($q) { $q->whereStoreId(\Session::get('store')->id);}
        ])->whereStoreId(\Session::get('store')->id)->search($request->search, [
            'product_price.code', 'product_price.product.name'
        ])->orderBy('balance', 'desc')->groupBy('product_price_id');

        return view('store-manager.stock.index', [
            'records' => $records->paginate(30)
        ]);
    }

    public function updateMinimumQty(Request $request)
    {
        if(!MinimumStock::where('product_price_id', $request->product_price_id)->whereStoreId(\Session::get('store')->id)->whereNotNull('store_id')->exists()){

            if($request->qty < 1)
                return redirect()->back()->with(['error' => 'Invalid Quantity, Try Again']);

            MinimumStock::create([
                'store_id' => \Session::get('store')->id,
                'product_price_id' =>  $request->product_price_id,
                'qty' => $request->qty
            ]);

            return redirect()->route('store-stock-view')->with(['success' => 'Product Minimum Quantity Updated Successfully']);

        }
        else{

            if($request->qty < 1)
                return redirect()->back()->with(['error' => 'Invalid Quantity, Try Again']);

            $product_price = MinimumStock::where('product_price_id',$request->product_price_id)->whereStoreId(\Session::get('store')->id)->whereNotNull('store_id')->first();

            $product_price->store_id = \Session::get('store')->id;
            $product_price->qty = $request->qty;
            $product_price->save();

            return redirect()->route('store-stock-view')->with(['success' => 'Product Minimum Quantity Updated Successfully']);
        }
    }

    public function supply(Request $request)
    {
        $supplies = StoreSupply::with(['senderStore:id,name,tracking_id', 'admin:id,name'])
            ->whereStoreId(\Session::get('store')->id)->filterDate($request->dateRange)->latest()->paginate(30);

        return view('store-manager.stock.supply', [
            'supplies' => $supplies
        ]);
    }


    public function deliveryNote(Request $request)
    {
        if (!$supply = StoreSupply::with(['store', 'admin:id,name', 'details.product_price.product:id,name'])->whereId($request->id)->first())
            return redirect()->back()->with(['error' => 'Invalid Record, Try Again']);

        $company_profile = Setting::where('name','Company Profile')->first()->value;

        $filename =  $supply->id;

        $snappy = new Pdf(base_path('vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64'));

        $snappy->generateFromHtml(
            view('admin.store-manager.store.supply.delivery-note', [
                'filename' => $filename,
                'supply' => $supply,
                'company_profile' => collect($company_profile)
            ])->__toString(), storage_path("app/".$filename.".pdf"), [
            'orientation' => 'Portrait',
            'page-height' => 297,
            'page-width'  => 210,
        ], true);

        return response()->download(storage_path("app/".$filename.".pdf"))->deleteFileAfterSend(true);
    }

    public function supplyDetails(Request $request)
    {
        $supply = StoreSupply::with(['senderStore:id,name,tracking_id', 'admin:id,name', 'details.product_price.product:id,name', 'stockTransactions'])
            ->whereStoreId(\Session::get('store')->id)
            ->whereId($request->id)->first();

        if (!$supply)
            return redirect()->back()->with(['error' => 'Not able to find the Supply Record']);

        return view('store-manager.stock.supply-details', [
            'supply' => $supply
        ]);
    }
}
