<?php

namespace App\Http\Controllers\Store;

use App\Library\Helper;
use App\Models\StoreManager\StockTransaction;
use App\Models\StoreManager\Store;
use App\Models\StoreManager\StoreStockTransaction;
use App\Models\StoreManager\StoreSupply;
use App\Models\StoreManager\StoreSupplyDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SupplyController extends Controller
{
    public function index(Request $request)
    {
        if (\Session::get('store')->type == Store::BASIC_TYPE)
            return redirect()->route('store-dashboard')->with(['error' => 'Access Denied']);

        $supplies = StoreSupply::with(['store:id,name,tracking_id'])
            ->whereSenderStoreId(\Session::get('store')->id)->filterDate($request->dateRange)->latest()->paginate(30);

        return view('store-manager.supply.index', [
            'supplies' => $supplies
        ]);
    }

    public function create(Request $request)
    {
        if (\Session::get('store')->type == Store::BASIC_TYPE)
            return redirect()->route('store-dashboard')->with(['error' => 'Access Denied']);

        $balance_query = 'COALESCE(SUM(CASE WHEN type = 1 THEN qty END),0) - COALESCE(SUM(CASE WHEN type = 2 THEN qty END),0)';

        if ($request->isMethod('post'))
        {
            $validator = \Validator::make($request->all(), [
                'store_id' => 'required|exists:stores,id',
                'delivery_type' => 'required',
            ], [
                'store_id.required' => 'Select Store or warehouse to create Stock Supply',
                'store_id.exists' => 'Invalid Store or Warehouse',
            ]);

            if ($validator->fails())
                return response()->json(['status' => false, 'message' => $validator->errors()->first()]);

            $form_request = Helper::arrayToObject($request->all());

            $order_items = collect($form_request->order_items)->filter(function ($order_item) {
                return $order_item->selected_qty > 0;
            });

            if (count($order_items) == 0)
                return response()->json(['status' => false, 'message' => 'Select at least one Item to Create Stock Supply']);

            /* Stock Check Start */
            $exist_stock_items = StoreStockTransaction::selectRaw($balance_query . ' as balance, product_price_id')
                ->groupBy('product_price_id')->havingRaw($balance_query . '> 0')
                ->whereIn('product_price_id', collect($order_items)->pluck('id')->toArray())
                ->whereStoreId(\Session::get('store')->id)->get();

            $item_not_exists = collect($order_items)->filter(function ($order_item) use ($exist_stock_items) {

                $current_item = collect($exist_stock_items)->where('product_price_id', $order_item->id)->first();

                if (!$current_item)
                    return !$current_item;

                return $order_item->selected_qty > $current_item->balance;
            })->first();

            if ($item_not_exists) {
                return response()->json(['status' => false, 'message' => $item_not_exists->name . ' has not enough Stock, Remove This Item or Try again']);
            }
            /* Stock Check End */
            $store = Store::whereId($form_request->store_id)->first();

            if ($form_request->order_details->total_amount > $store->wallet_balance) {
                return response()->json(['status' => false, 'message' => $store->name . ' Store has not enough balance to transfer the Stock Please try again']);
            }

            $supply = \DB::transaction(function () use ($form_request, $order_items, $store) {

                $supply = StoreSupply::create([
                    'sender_store_id' => \Session::get('store')->id,
                    'store_id' => $store->id,
                    'delivery_type' => $form_request->delivery_type,
                    'amount' => $form_request->order_details->amount,
                    'tax_amount' => $form_request->order_details->gst,
                    'total' => $form_request->order_details->total_amount,
                    'courier_name' => $form_request->courier_name,
                    'courier_docket_number' => $form_request->courier_docket_number,
                    'remarks' => $form_request->remarks
                ]);

                collect($order_items)->map(function ($order_item) use ($supply, $store,$form_request) {

                    StoreSupplyDetail::create([
                        'supply_id' => $supply->id,
                        'product_price_id' => $order_item->id,
                        'supply_price' =>  round($order_item->distributor_price/$order_item->selected_qty, 2),
                        'price' => $order_item->price,
                        'distributor_price' => $order_item->distributor_price,
                        'qty' => $order_item->selected_qty,
                        'gst' => json_encode([
                            'percentage' => $order_item->tax_percentage,
                            'code' => $order_item->tax_code,
                        ])
                    ]);


                    \Session::get('store')->stockDebit(collect([
                        'supply_id' => $supply->id,
                        'product_price_id' => $order_item->id,
                        'qty' => $order_item->selected_qty
                    ]));

                    $store->stockCredit(collect([
                        'supply_id' => $supply->id,
                        'product_price_id' => $order_item->id,
                        'qty' => $order_item->selected_qty
                    ]));

                });

                $store->debit(collect([
                    'admin_id' => \Session::get('admin')->id,
                    'amount' => $form_request->order_details->amount,
                    'remarks' => 'Debit towards Supply from Company #' . $supply->id
                ]));

                \Session::get('store')->credit(collect([
                    'admin_id' => \Session::get('admin')->id,
                    'amount' => $form_request->order_details->amount,
                    'remarks' => 'Credit towards Supply from Company #' . $supply->id
                ]));

                return $supply;

            });

            $request->session()->flash('success', 'New Supply Created to Store ' . $supply->store->name);

            return response()->json(['status' => true, 'route' => route('store-supply-detail', ['id' => $supply->id])]);

        }

        $items = StoreStockTransaction::selectRaw($balance_query . ' as balance, product_price_id')
            ->groupBy('product_price_id')
            ->havingRaw($balance_query . '> 0')
            ->where('store_id', \Session::get('store')->id)
            ->with(['product_price.product:id,name'])->get()->map(function ($transaction) {

                return [
                    'id' => $transaction->product_price->id,
                    'code' => $transaction->product_price->code,
                    'name' => $transaction->product_price->product->name,
                    'selected_qty' => 0,
                    'balance' => $transaction->balance,
                    'price' => $transaction->product_price->price,
                    'distributor_price' => $transaction->product_price->distributor_price,
                    'tax_percentage' => $transaction->product_price->gst->percentage,
                    'tax_code' => $transaction->product_price->gst->code,
                ];

            });

        $stores = Store::selectRaw('id as value, name as label, name, city, id')
            ->where('type', '>', \Session::get('store')->type)->get();

        return view('store-manager.supply.create', [
            'stores' => $stores,
            'items' => $items
        ]);
    }

    public function detail(Request $request)
    {
        if (\Session::get('store')->type == Store::BASIC_TYPE)
            return redirect()->route('store-dashboard')->with(['error' => 'Access Denied']);

        $supply = StoreSupply::with(['store:id,name,tracking_id', 'details.product_price.product:id,name'])
            ->whereSenderStoreId(\Session::get('store')->id)
            ->whereId($request->id)->first();

        if ($request->isMethod('post')) {

            $this->validate($request, [
                'delivery_type' => 'required',
                'status' => 'required',
            ],[
                'delivery_type.required' => 'Delivery Type is required field',
                'status.required' => 'Status is required field',
            ]);

            $supply->delivery_type = $request->delivery_type;
            $supply->courier_name = $request->courier_name;
            $supply->courier_docket_number = $request->courier_docket_number;
            $supply->remarks = $request->remarks;
            $supply->status = $request->status;
            $supply->save();

            return redirect()->route('store-supply-detail', ['id' => $supply->id ])->with(['success' => 'Update Successfully']);
        }
        if (!$supply)
            return redirect()->back()->with(['error' => 'Not able to find the Supply Record']);

        return view('store-manager.supply.details',[
            'supply' => $supply
        ]);
    }

    public function apiGetStoreDetails(Request $request)
    {
        if (!$store = Store::whereId($request->store_id)->first())
            return response()->json(['status' => false, 'message' => 'Invalid Store request, try again']);

        return response()->json([
            'status' => true,
            'store' => [
                'id' => $store->id,
                'tracking_id' => $store->tracking_id,
                'name' => $store->name,
                'city' => $store->city,
                'type' => $store->type,
                'wallet_balance' => $store->wallet_balance
            ]
        ]);
    }
}
