<?php

namespace App\Http\Controllers\Store;

use App\Http\Controllers\Controller;
use App\Library\ImageUpload;
use App\Models\StoreManager\StoreBank;
use App\Models\StoreManager\StoreStatus;
use App\Models\StoreManager\StoreDocument;
use Illuminate\Http\Request;
use Razorpay\IFSC\Client;
use Razorpay\IFSC\IFSC;

class KycController extends Controller
{
    public function dashboard()
    {
        $store_status = StoreStatus::whereStoreId(\Session::get('store')->id)->first();

        return view('store-manager.kyc.dashboard', [
            'store_status' => $store_status
        ]);
    }

    public function pancard(Request $request)
    {
        $store_status = StoreStatus::whereStoreId(\Session::get('store')->id)->first();

        if ($request->isMethod('post')) {
            $this->validate($request, [
                'image' => 'required|mimes:jpeg,jpg,png|max:4000',
                'pan_number' => 'required|regex:/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/',
            ], [
                'image.image' => 'Invalid image',
                'image.max' => 'Image Size should be less than 2MB',
                'image.required' => 'Upload File is Required',
                'pan_number.regex' => 'Invalid Pancard Number',
                'pan_number.required' => 'Pan Number is required'
            ]);

            $image_uploader = (new ImageUpload());
            if (!$image_uploader->process($request->file('image'), ImageUpload::DOCUMENT_TYPE))
                return redirect()->back()->with(['error' => 'Unable to fetch Image, Upload another image']);

            $image_name = $image_uploader->store(env('DOCUMENT_IMAGE_PATH'));

            StoreDocument::create([
                'store_id' => \Session::get('store')->id,
                'image' => $image_name,
                'type' => StoreDocument::PAN_CARD,
                'number' => $request->pan_number,
                'status' => StoreDocument::PENDING
            ]);

            $store_status->update(['pancard' => StoreStatus::KYC_INPROGRESS]);

            return redirect()->route('store-kyc-dashboard')->with(['success' => 'You Pan card details are submitted, It will be reviewed soon']);
        }

        $documents = StoreDocument::whereType(StoreDocument::PAN_CARD)->latest()->whereStoreId(\Session::get('store')->id)->get();

        return view('store-manager.kyc.pancard', [
            'store_status' => $store_status,
            'documents' => $documents
        ]);
    }

    public function aadhaarNumber(Request $request)
    {
        $store_status = StoreStatus::whereStoreId(\Session::get('store')->id)->first();

        if ($request->isMethod('post')) {
            $this->validate($request, [
                'image' => 'required|mimes:jpeg,jpg,png|max:4000',
                'secondary_image' => 'required|mimes:jpeg,jpg,png|max:4000',
                'aadhar_number' => 'required|regex:/^[0-9]{12}?$/',
            ], [
                'image.image' => 'Invalid Front image',
                'image.max' => 'Front Image Size should be less than 4MB',
                'secondary_image.image' => 'Invalid Back image',
                'secondary_image.max' => 'Back Image Size should be less than 4MB',
                'image.required' => 'Aadhar Card Front Image is Required',
                'secondary_image.required' => 'Aadhar Card Back image is Required',
                'aadhar_number.regex' => 'Invalid Aadhar Number',
                'aadhar_number.required' => 'Aadhar Number is required'
            ]);

            $image_uploader = (new ImageUpload());
            if (!$image_uploader->process($request->file('image'), ImageUpload::DOCUMENT_TYPE))
                return redirect()->back()->with(['error' => 'Invalid / Broken or Corrupted Front Image, Try Another']);

            $image_name = $image_uploader->store(env('DOCUMENT_IMAGE_PATH'));

            if (!$image_uploader->process($request->file('secondary_image'), ImageUpload::DOCUMENT_TYPE))
                return redirect()->back()->with(['error' => 'Invalid / Broken or Corrupted Back Image, Try Another']);

            $secondary_image = $image_uploader->store(env('DOCUMENT_IMAGE_PATH'));


            StoreDocument::create([
                'store_id' => \Session::get('store')->id,
                'image' => $image_name,
                'secondary_image' => $secondary_image,
                'type' => StoreDocument::AADHAR_CARD,
                'number' => $request->aadhar_number,
                'status' => StoreDocument::PENDING
            ]);

            $store_status->update(['aadhar_card' => StoreStatus::KYC_INPROGRESS]);

            return redirect()->route('store-kyc-dashboard')->with(['success' => 'You Aadhaar card details are submitted, It will be reviewed soon']);
        }

        $documents = StoreDocument::whereType(StoreDocument::AADHAR_CARD)->latest()->whereStoreId(\Session::get('store')->id)->get();

        return view('store-manager.kyc.aadhar-number', [
            'store_status' => $store_status,
            'documents' => $documents
        ]);
    }

    public function bank(Request $request)
    {
        $store_status = StoreStatus::whereStoreId(\Session::get('store')->id)->first();
        $store_bank = StoreBank::whereStoreId(\Session::get('store')->id)->first();

        $allow_update = in_array($store_status->bank_account, [StoreStatus::KYC_PENDING, StoreStatus::KYC_REJECTED]);

        if ($request->isMethod('post')) {
            if ($allow_update == false) {
                return redirect()->back()->with(['error' => 'Your Bank KYC Details is Under Reviewed, Will Update Soon']);
            }

            $this->validate($request, [
                'type' => 'required',
                'image' => 'required|mimes:jpeg,jpg,png|max:4000',
                'bank_name' => 'required',
                'account_name' => 'required|min:3',
                'account_number' => 'required|numeric',
                'branch' => 'required|min:3',
                'ifsc' => 'required|min:3',
                'account_type' => 'required|numeric',
                'city' => 'required|min:3'
            ], [
                'type.required' => 'Bank Document Type is required',
                'image.image' => 'Invalid image',
                'image.max' => 'Image Size should be less than 2MB',
                'image.required' => 'Upload Cancel Cheque Cheque or Bank Passbook is Required'
            ]);

            $image_uploader = (new ImageUpload());
            if (!$image_uploader->process($request->file('image'), ImageUpload::DOCUMENT_TYPE))
                return redirect()->back()->with(['error' => 'Unable to fetch Image, Upload another image']);

            $image_name = $image_uploader->store(env('DOCUMENT_IMAGE_PATH'));

            StoreDocument::create([
                'store_id' => \Session::get('store')->id,
                'image' => $image_name,
                'type' => $request->type,
                'number' => $request->account_number,
                'status' => StoreDocument::PENDING
            ]);

            $store_bank->account_name = $request->account_name;
            $store_bank->bank_name = $request->bank_name;
            $store_bank->account_number = $request->account_number;
            $store_bank->branch = $request->branch;
            $store_bank->ifsc = $request->ifsc;
            $store_bank->type = $request->account_type;
            $store_bank->city = $request->city;
            $store_bank->save();

            $store_status->update(['bank_account' => StoreStatus::KYC_INPROGRESS]);

            return redirect()->route('store-kyc-dashboard')->with(['success' => 'You Bank details are submitted, It will be reviewed soon']);
        }

        $documents = StoreDocument::whereIn('type', [StoreDocument::CANCEL_CHEQUE, StoreDocument::BANK_PASSBOOK])->latest()->whereStoreId(\Session::get('store')->id)->get();

        return view('store-manager.kyc.bank', [
            'allow_update' => $allow_update,
            'store_bank' => $store_bank,
            'store_status' => $store_status,
            'documents' => $documents
        ]);
    }

    public function getBankDetails(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'ifsc_code' => 'required|min:5'
        ], [
            'ifsc_code.required' => 'IFSC Code is required',
        ]);

        if ($validator->fails())
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);

        if (!IFSC::validate($request->ifsc_code))
            return response()->json(['status' => false, 'message' => 'Invalidate IFSC Code']);

        $response = (new Client())->lookupIFSC($request->ifsc_code);

        return response()->json([
            'status' => true,
            'bank' => [
                'name' => $response->bank,
                'code' => $response->code,
                'branch' => $response->branch,
                'city' => $response->city,
                'district' => $response->district,
                'state' => $response->state
            ]
        ]);
    }
}
