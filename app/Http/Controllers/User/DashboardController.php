<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Library\Helper;
use App\Models\NestedSetUser;
use App\Models\Payout;
use App\Models\Pin;
use App\Models\Support;
use App\Models\User;
use App\Models\Wallet;

class DashboardController extends Controller
{

    public function index()
    {
        $user = User::whereId(\Session::get('user')->id)->first();

        $shareLinks = \Share::page($user->referral_link, 'Join Me & Earn Income')
            ->facebook()->twitter()->whatsapp()->telegram()->getRawLinks();

        $wallet =  Wallet::whereUserId($user->id)->whereType(Wallet::CREDIT_TYPE);

        $income = [
            'etb_income' => $wallet->whereIncomeType(Wallet::ENROLLMENT_TEAM_BONUS)->sum('amount'),
            'rtb_income' => $wallet->whereIncomeType(Wallet::RECURRING_TEAM_BONUS)->sum('amount'),
            'cmb_income' => $wallet->whereIncomeType(Wallet::CHEQUE_MATCHING_BONUS)->sum('amount'),
            'rank_income' => $wallet->whereIncomeType(Wallet::RANK_BONUS)->sum('amount'),
            'ocb_income' => $wallet->whereIncomeType(Wallet::OUTLET_CHEQUE_BONUS)->sum('amount'),
            'mpb_income' => $wallet->whereIncomeType(Wallet::MONTHLY_PERFORMANCE_BONUS)->sum('amount'),

            'earned_etb_income' => $wallet->whereNotNull('payout_id')->whereIncomeType(Wallet::ENROLLMENT_TEAM_BONUS)->sum('amount'),
            'earned_rtb_income' => $wallet->whereNotNull('payout_id')->whereIncomeType(Wallet::RECURRING_TEAM_BONUS)->sum('amount'),
            'earned_cmb_income' => $wallet->whereNotNull('payout_id')->whereIncomeType(Wallet::CHEQUE_MATCHING_BONUS)->sum('amount'),
            'earned_rank_income' => $wallet->whereNotNull('payout_id')->whereIncomeType(Wallet::RANK_BONUS)->sum('amount'),
            'earned_ocb_income' => $wallet->whereNotNull('payout_id')->whereIncomeType(Wallet::OUTLET_CHEQUE_BONUS)->sum('amount'),
            'earned_mpb_income' => $wallet->whereNotNull('payout_id')->whereIncomeType(Wallet::MONTHLY_PERFORMANCE_BONUS)->sum('amount'),
        ];

        return view('user.dashboard', [
            'user' => $user,
            'shareLinks' => Helper::arrayToObject($shareLinks),
            'user_status' => $user->status,
            'total_payout' => Payout::whereUserId($user->id)->sum('amount'),
            'total_income' => Wallet::whereUserId($user->id)->whereType(Wallet::CREDIT_TYPE)->whereNull('payout_id')->sum('amount'),
            'sponsors' => User::whereSponsorBy($user->id)->count(),
            'children' => NestedSetUser::childrenCount($user->id),
            'open_supports' => Support::whereNull('parent_id')->whereUserId($user->id)->whereStatus(Support::OPEN)->count(),
            'unused_pins' => Pin::whereStatus(Pin::UNUSED)->whereUserId(\Session::get('user')->id)->count(),
            'binary_total' => $user->current_binary_status,
            'active_sponsors' => User::whereSponsorBy($user->id)->whereNotNull('paid_at')->count(),
            'wallet' => Helper::arrayToObject($income)
        ]);
    }
}
