<?php

namespace App\Http\Controllers\User;

use App\Library\Calculation\Binary\Helper;
use App\Library\Sms;
use App\Models\State;
use App\Models\User;
use App\Models\UserAddress;
use App\Models\UserBank;
use App\Models\UserDetail;
use App\Models\UserStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    public function details(Request $request)
    {
        \Session::forget('user_registration');

        $sponsorUser = null;
        $leg = null;

        if ($request->referral){
            $referral_link = explode('leg',base64_decode(strtr($request->referral, '-_,', '+/=')));
            $tracking_id = $referral_link[0];
            $referral_leg = $referral_link[1];
        }

        if (isset($tracking_id))
        {
            if (!$sponsorUser = User::whereTrackingId($tracking_id)->first()) {
                return redirect()->route('user-register')->with(['error' => 'Invalid Referral Upline Data']);
            }
        }

        if (isset($referral_leg)) {
//            $leg = $request->leg;
            $leg = $referral_leg;
            if (!in_array($leg, ['L', 'R'])) {
                return redirect()->route('user-register')->with(['error' => 'Invalid Position Under Parent']);
            }
        }

        $nominees = \File::get(public_path('data/nominees.json'));
        $states = State::active()->get();

        if ($request->isMethod('post'))
        {
            $this->validate($request,[
                'tracking_id' => 'required|exists:users,tracking_id',
                'leg' => 'required',
                'title' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'birth_date' => 'required',
                'email' => 'required|email',
                'mobile' => 'required|regex:/^[9876][0-9]{9}$/|digits:10',
                'username' => 'required|unique:users,username',
                'password' => 'required|confirmed|min:6',
                'terms' => 'required',
                'aadhaar_number' =>'required|regex:/^[0-9]{12}?$/'
            ], [
                'tracking_id.required' => 'Tracking Id is Required',
                'tracking_id.exists' => 'Invalid Tracking Id',
                'leg.required' => 'Kindly Select your Placement under Upline',
                'title.required' => 'Title is Required',
                'first_name.required' => 'First Name is Required',
                'last_name.required' => 'Last Name is Required',
                'birth_date.required' => 'Date of Birth is Required',
                'email.required' => 'Email Address is required',
                'email.email' => 'Invalid Email Address',
                'mobile.required' => 'Mobile Number is Required',
                'mobile.regex' => 'Invalid Mobile Number',
                'mobile.digits' => 'Mobile Number must be 10 digits',
                'username.required' => 'Username is Required',
                'username.unique' => 'This Username is already exist',
                'password.required' => 'Password is required',
                'password.min' => 'Password length should be 6 or more',
                'terms.required' => 'Terms & Conditions is Required',
                'aadhaar_number.required' => 'Aadhaar Number is Required',
                'aadhaar_number.regex' => 'Aadhaar Number is Invalid'
            ]);

            if (!$sponsorUser = User::whereTrackingId($request->tracking_id)->first()) {
                return redirect()->route('user-register')->with(['error' => 'Invalid Tracking ID, Try again']);
            }

            \Session::push('user_registration', [
                    'sponsor_user' => $sponsorUser,
                    'selected_leg' => $leg,
                    'user' => $request->all()
                ]
            );

            return redirect()->route('user-register-overview');
        }

        return view('website.authentication.register.details', [
            'sponsor_upline' => $sponsorUser,
            'nominee_relation' => json_decode($nominees),
            'states' => $states,
            'selected_leg' => $leg
        ]);
    }

    public function overview(Request $request)
    {
        if(!\Session::has('user_registration'))
            return redirect()->route('user-register')->with(['error' => 'Not able to retrieve the User Details, Try again']);

        $user_detail = (object) (\Arr::collapse(\Session::get('user_registration')));

        if ($request->isMethod('post'))
        {
            $response = (new Helper())->findTheUpline($user_detail->sponsor_user, $user_detail->selected_leg);

            if ($response->status == false) {

                return redirect()->route('user-register')->with([
                    'error' => 'We can not place this user to Leg: ' . $user_detail->leg . ' Try Again'
                ]);
            }

            $user = \DB::transaction(function () use($user_detail, $response) {

                $parent = $response->upline;

                $tracking_id = 'AOS'.rand(1000000, 9999999);

                while(User::whereTrackingId($tracking_id)->exists()) {
                    $tracking_id = 'AOS'.rand(1000000, 9999999);
                }

                $details = \App\Library\Helper::arrayToObject($user_detail->user);

                $user = User::create([
                    'parent_id' => $parent->id,
                    'sponsor_by' => $user_detail->sponsor_user->id,
                    'tracking_id' => $tracking_id,
                    'username' => $details->username,
                    'password' => $details->password,
                    'wallet_password' => strtoupper(\Str::random(6)),
                    'leg' => $details->leg,
                    'email' => $details->email,
                    'mobile' => $details->mobile,
                    'token' => strtoupper(\Str::random(15))
                ]);

                UserDetail::create([
                    'user_id' => $user->id,
                    'title' => $details->title,
                    'first_name' => $details->first_name,
                    'last_name' => $details->last_name,
                    'gender' => in_array($details->title, ['Mr', 'Sri', 'Dr']) ? 1 : 2,
                    'birth_date' => Carbon::parse($details->birth_date),
                    'aadhaar_number' => $details->aadhaar_number
                ]);

                UserStatus::create([
                    'user_id' => $user->id,
                ]);

                UserBank::create([
                    'user_id' => $user->id
                ]);

                UserAddress::create([
                    'user_id' => $user->id
                ]);

                return $user;

            });

            \Session::put('registered_user', $user);

            (new Sms())->to($user)->template('registration', [
                $user->detail->first_name, $user->tracking_id, $user->password, $user->wallet_password
            ])->sendOne();

            return redirect()->route('user-register-thanks')->with([
                'success' => 'Your Registration has been successfully completed!',
            ]);
        }


        return view('website.authentication.register.overview', [
            'session_detail' => $user_detail,
            'user_detail' => \App\Library\Helper::arrayToObject($user_detail->user)
        ]);
    }

    public function thanks()
    {
        if (!\Session::has('user_registration') || !\Session::has('registered_user')) {
            return redirect()->route('user-login');
        }

        $user = User::with('detail')->whereId(\Session::get('registered_user')->id)->first();

        \Session::forget('user_registration');
        \Session::forget('registered_user');

        return view('website.authentication.register.thanks', [
            'user' => $user
        ]);
    }

    public function getUser(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'tracking_id' => 'required|exists:users,tracking_id',
        ], [
            'tracking_id.required' => 'Tracking Id is Required',
            'tracking_id.exists' => 'Invalid Tracking Id',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
        }

        $user = User::with(['detail'])->whereTrackingId($request->tracking_id)->first();

        return response()->json([
            'status' => true,
            'user' => [
                'id' => $user->id,
                'name' => $user->detail->full_name,
                'tracking_id' => $user->tracking_id,
            ]
        ]);
    }
}
