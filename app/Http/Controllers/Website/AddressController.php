<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\UserAddress;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function create(Request $request)
    {
        $validator = \Validator($request->all(), [
            'name' => 'required',
            'mobile' => 'required|regex:/^[9876][0-9]{9}$/|digits:10',
            'email' => 'email',
            'address' => 'required|min:10|max:90',
            'city' => 'required|min:3',
            'state_id' => 'required',
            'pincode' => 'required|digits:6'
        ], [
            'name.required' => 'Name is required',
            'mobile.required' => 'Mobile Number is required',
            'mobile.regex' => 'Invalid Mobile Number',
            'mobile.digits' => 'Mobile Number must be 10 Digits',
            'email.email' => 'Invalid Email Format',
            'address.required' => 'Your Address is required',
            'address.min' => 'Address should be more than 10 character',
            'city.required' => 'Your City is required',
            'city.min' => 'City should be more than 3 character',
            'state_id.required' => 'State is required',
            'pincode.required' => 'Pincode is required',
            'pincode.digits' => 'Pincode should be in 6 digits',
        ]);

        if ($validator->fails())
            return back()->withInput()->with(['address_errors' => $validator->errors()->all()]);

        UserAddress::create([
            'user_id' => \Session::get('user')->id,
            'name' => $request->name,
            'mobile' => $request->mobile,
            'email' => $request->email,
            'address' => $request->address,
            'landmark' => $request->landmark,
            'city' => $request->city,
            'state_id' => $request->state_id,
            'pincode' => $request->pincode,
            'type' => $request->type
        ]);

        return redirect()->route($request->page)->with(['success' => 'New address has been added']);
    }

    public function update(Request $request)
    {
        $validator = \Validator($request->all(), [
            'name' => 'required',
            'mobile' => 'required|regex:/^[9876][0-9]{9}$/|digits:10',
            'email' => 'email',
            'address' => 'required|min:10|max:90',
            'city' => 'required|min:3',
            'state_id' => 'required',
            'pincode' => 'required|digits:6'
        ], [
            'name.required' => 'Name is required',
            'mobile.required' => 'Mobile Number is required',
            'mobile.regex' => 'Invalid Mobile Number',
            'mobile.digits' => 'Mobile Number must be 10 Digits',
            'email.email' => 'Invalid Email Format',
            'address.required' => 'Your Address is required',
            'address.min' => 'Address should be more than 10 character',
            'city.required' => 'Your City is required',
            'city.min' => 'City should be more than 3 character',
            'state_id.required' => 'State is required',
            'pincode.required' => 'Pincode is required',
            'pincode.digits' => 'Pincode should be in 6 digits',
        ]);

        if ($validator->fails())
            return back()->withInput()->with(['address_errors' => $validator->errors()->all()]);

        if (!$address = UserAddress::whereId($request->id)->first())
            return back()->withInput()->with(['address_errors' => ['Invalid Address Data for Edit']]);

        $address->name = $request->name;
        $address->mobile = $request->mobile;
        $address->email = $request->email;
        $address->address = $request->address;
        $address->landmark = $request->landmark;
        $address->city = $request->city;
        $address->state_id = $request->state_id;
        $address->pincode = $request->pincode;
        $address->type = $request->type;
        $address->save();

        return redirect()->route($request->page)->with(['success' => 'Your Address is updated']);

    }
}
