<?php

namespace App\Http\Controllers\Website;

use App\Models\Banner;
use App\Models\Blog;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\UserWishList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {

        $banners = Banner::active()->whereType(Banner::BANNER)->get();
        $popup = Banner::active()->whereType(Banner::POPUP)->first();

        $categories = Category::active()->with(['parent.children'])->inRandomOrder()->take(4)->orderBy('id','asc')->get();

        $customer_favorites = UserWishList::with('product_price.product')->whereHas('product_price.product', function ($q) {
            $q->where('status', Product::ACTIVE);
        })->groupBy('product_price_id')->get();

        $deal_products = ProductPrice::with(['product.category.parent'])->whereHas('product', function ($q) {
            $q->where('status', Product::ACTIVE);
        })->inRandomOrder()->whereDeal(ProductPrice::DEAL)->get();

        $new_arrival_products = ProductPrice::with(['product.category.parent'])->whereHas('product', function ($q) {
            $q->where('status', Product::ACTIVE);
        })->inRandomOrder()->whereNewArrival(ProductPrice::NEW_ARRIVAL)->latest()->get();

        $recommended_products = ProductPrice::with(['product.category.parent'])->whereHas('product', function ($q) {
            $q->where('status', Product::ACTIVE);
        })->inRandomOrder()->whereRecommended(ProductPrice::RECOMMENDED)->get();

        $blog = Blog::whereStatus(Blog::ACTIVE)->inRandomOrder()->take(4)->get();

        return view('website.home',[
            'banners' => $banners,
            'popup' => $popup,
            'categories' => $categories,
            'customer_favorites' => $customer_favorites,
            'deal_products' => $deal_products,
            'new_arrival_products' => $new_arrival_products,
            'recommended_products' => $recommended_products,
            'blog' => $blog
        ]);
    }

    public function autoComplete(Request $request) {

        $data = Product::select("name")
            ->where("name","LIKE","%{$request->get('query')}%")
            ->whereStatus(Product::ACTIVE)
            ->get();

        return response()->json($data);

    }
    public function productDetail(Request $request) {

        $search = $request->get('search');

        $product_price = ProductPrice::with(['product.category'])->whereHas('product', function ($q) use ($search) {
            $q->where('status', '<>', Product::INACTIVE)->where('name',$search);
        })->first();


        if (!$product_price)
            return redirect()->back()->with(['error' => 'Product / Item is not available']);

        $parent_category = null;

        return response()->json([
            "status" => true,
            "url" => "/product-details/".$product_price->product->slug."/".$product_price->code
        ]);

    }
}
