<?php

namespace App\Http\Controllers\Website;

use App\Library\Helper;
use App\Models\StoreManager\StoreType;
use App\Models\Grievance;
use App\Models\UserWishList;
use App\Models\WebContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompanyController extends Controller
{
    public function about()
    {
        $content = [
            'about' => WebContent::active()->whereType(WebContent::ABOUT)->first(),
            'mission' => WebContent::active()->whereType(WebContent::MISSION)->first(),
            'vision' => WebContent::active()->whereType(WebContent::VISION)->first(),
        ];

        return view('website.about', [
            'content' => Helper::arrayToObject($content)
        ]);
    }

    public function legals()
    {
        return view('website.legals');
    }

    public function vision()
    {
        return view('website.vision');
    }

    public function mission()
    {
        return view('website.mission');
    }

    public function terms()
    {
        return view('website.terms');
    }

    public function privacyPolicy()
    {
        return view('website.privacy-policy');
    }

    public function cancellationPolicy()
    {
        return view('website.cancellation-policy');
    }

    public function outlet()
    {
        $outlets = StoreType::whereStatus(StoreType::ACTIVE)->orderBy('id', 'desc')->get();

        return view('website.outlet-price', [
            'outlets' => $outlets
        ]);
    }

    public function support(Request $request)
    {
        if ($request->isMethod('post'))
        {
            $this->validate($request,[
                'name' => 'required',
                'email' => 'required',
                'subject' => 'required',
                'message' => 'required'
            ],[
                'name.required' => 'Name is Required',
                'email.required' => 'Email is Required',
                'subject.required' => 'Subject is Required',
                'message.required' => 'Message is Required'
            ]);

            Grievance::create([
                'name' => $request->name,
                'email' => $request->email,
                'subject' => $request->subject,
                'message' => $request->message
            ]);

            return redirect()->route('website-support')->with(['success' => 'Your Complaints Request has been Sent, We will contact you soon..!!']);
        }

        return view('website.support');
    }

    public function promotions()
    {
        $promotion = WebContent::active()->whereType(WebContent::PROMOTIONS)->first();

        return view('website.promotions',[
            'promotion' => $promotion
        ]);
    }

    public function businessOpportunity()
    {
        $business = WebContent::active()->whereType(WebContent::BUSINESS_OPPORTUNITY)->first();

        return view('website.business-opportunity', [
            'business' => $business
        ]);
    }

    public function loginInfo()
    {
        return view('website.login-info');
    }

    public function customer()
    {
        return view('website.customer');
    }

    public function wishListView(Request $request)
    {
        $userWishList = [];

        if (\Session::has('user')){
            $userWishList = UserWishList::with('product_price')->whereUserId(\Session::get('user')->id)->orderBy('id', 'desc')->paginate(20);
        }elseif (\Session::has('customer')){
            $userWishList = UserWishList::with('product_price')->whereCustomerId(\Session::get('customer')->id)->orderBy('id', 'desc')->paginate(20);
        }

        return view('website.wish-list-view', [
            'userWishList' => $userWishList
        ]);
    }

    public function addRemoveWishList(Request $request)
    {
        if (!(\Session::has('customer') || \Session::has('user'))) {
            return response()->json([
                'warning' => 'Kindly login to add product in wishlist'
            ]);
        }

        $user = \Session::get('user');
        $customer = \Session::get('customer');

        if ($customer) {

            if ($wishlist = UserWishList::whereCustomerId($customer->id)->whereProductPriceId($request->product_price_id)->first()) {

                $wishlist->delete();
                return response()->json([
                    'status' => true, 'message' => 'Item is removed from Wishlist', 'type' => 'remove'
                ]);
            }
            else {

                UserWishList::create([
                    'customer_id' => $customer->id, 'product_price_id' => $request->product_price_id
                ]);
                return response()->json([
                    'status' => true, 'message' => 'Item is added to wishlist', 'type' => 'add'
                ]);
            }

        }
        else {

            if ($wishlist = UserWishList::whereUserId($user->id)->whereProductPriceId($request->product_price_id)->first()) {

                $wishlist->delete();

                return response()->json([
                    'status' => true, 'message' => 'Item is removed from Wishlist', 'type' => 'remove'
                ]);
            }
            else {

                UserWishList::create([
                    'user_id' => $user->id, 'product_price_id' => $request->product_price_id
                ]);

                return response()->json([
                    'status' => true, 'message' => 'Item is added to wishlist', 'type' => 'add'
                ]);
            }
        }
    }
}
