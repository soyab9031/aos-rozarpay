<?php

namespace App\Http\Controllers\Admin;

use App\Models\ContactInquiry;
use App\Models\Grievance;
use App\Models\Support;
use App\Models\SupportCategory;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class SupportController extends Controller
{
    public function website()
    {
        return view('admin.support.website-inquiry',[
            'inquiries' => ContactInquiry::orderBy('id', 'desc')->paginate(50)
        ]);
    }

    public function grievance()
    {
        return view('admin.support.grievance',[
            'grievances' => Grievance::orderBy('id', 'desc')->paginate(50)
        ]);

    }

    public function supportCategory(Request $request)
    {
        $category = null;

        if ($request->isMethod('post') && $request->id) {

            $category = SupportCategory::whereId($request->id)->first();

            $this->validate($request, [
                'name' => 'required|unique:support_categories,name,' . $category->id
            ], [
                'name.required' => 'Category Name is required',
                'name.unique' => 'Category Name should be unique'
            ]);

            $category->name = $request->name;
            $category->status = $request->status;
            $category->save();

            return redirect()->route('admin-user-support-category')->with(['success' => $category->name . ' Category has been updated.']);

        }
        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'name' => 'required|unique:support_categories,name'
            ], [
                'name.required' => 'Category Name is required',
                'name.unique' => 'Category Name should be unique'
            ]);

            SupportCategory::create([
                'name' => $request->name
            ]);

            return redirect()->route('admin-user-support-category')->with(['success' => 'New Support Category has been added']);
        }
        else if ($request->id) {

            $category = SupportCategory::whereId($request->id)->first();
        }


        return view('admin.support.user.category', [
            'category' => $category,
            'categories' => SupportCategory::orderBy('id', 'desc')->get()
        ]);
    }

    public function support(Request $request)
    {
        $supports = Support::with(['admin', 'category', 'user.detail'])->search($request->search, [
            'user.tracking_id', 'user.username', 'user.detail.first_name', 'user.mobile'
        ])->filterDate($request->dateRange)->whereNull('parent_id');

        if ($request->category_id)
            $supports = $supports->where('category_id', $request->category_id);

        return view('admin.support.user.index', [
            'categories' => SupportCategory::orderBy('id', 'desc')->get(),
            'supports' => $supports->orderBy('id', 'desc')->paginate(20)
        ]);
    }

    public function supportChat(Request $request)
    {
        if (!$support = Support::with(['category', 'user.detail'])->whereId($request->id)->whereNull('parent_id')->first())
            return redirect()->back()->with(['error' => 'Invalid Data for Support Chat']);

        if ($request->close_ticket)
        {
            Support::whereIn('id', $support->descendantsAndSelf()->get()->pluck('id')->toArray())->update([
                'status' => Support::CLOSED
            ]);

            return redirect()->route('admin-user-support-chat', ['id' => $support->id])->with(['success' => 'Ticket has been closed']);
        }

        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'message' => 'required|min:1'
            ]);

            if ($support->admin_id == null) {

                $support->admin_id = \Session::get('admin')->id;
                $support->save();

            }

            $support->children()->create([
                'category_id' => $support->category_id,
                'admin_id' => \Session::get('admin')->id,
                'user_id' => $support->user_id,
                'message' => $request->message,
                'type' => Support::ADMIN_TO_USER
            ]);

            return redirect()->route('admin-user-support-chat', ['id' => $support->id])->with(['success' => 'Message is sent']);
        }

        return view('admin.support.user.chat', [
            'support' => $support,
            'details' => $support->descendantsAndSelf()->with(['admin'])->get()
        ]);
    }



}
