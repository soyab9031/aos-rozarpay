<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index(Request $request){

        $blogs = Blog::search($request->search, [
            'title'
        ])->filterDate($request->dateRange);

        return view('admin.blog.index',[
            "blogs" =>  $blogs->orderBy('id','desc')->paginate(50)
        ]);
    }

    public function create(Request $request)
    {

        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'title' => 'required',
                'description' => 'min:5',
                'image' => 'mimes:jpeg,jpg,png|max:2000'
            ],[
                'title.required' => 'Title is Required',
                'description.min' => 'Minimum 5 character are compulsory',
                'image.mimes' => 'Image must have .jpg,jpeg and png type',
                'image.max' => 'Upload an image less then 2MB'
            ]);

            if($request->image) {
                if ($request->hasFile('image') && $request->file('image')->isValid()) {
                    $disk = \Storage::disk('spaces');
                    $image = (string)\Str::uuid() . "." . $request->file('image')->getClientOriginalExtension();

                    $disk->delete(env('BLOG_IMAGE_PATH') . $request->image);
                    $disk->put(env('BLOG_IMAGE_PATH') . $image, file_get_contents($request->file('image')->path()));

                    Blog::create([
                        'title' => $request->title,
                        'image' => $image,
                        'description' => $request->description,
                        'status' => $request->status
                    ]);
                }
            }
            return redirect()->route('admin-blog-view')->with(['success' => 'New Blog has been Created.']);
        }
        return view('admin.blog.create');
    }

    public function update(Request $request)
    {
        $blog = Blog::whereId($request->id)->first();

        if($request->deleteImage == 'Yes'){
            $disk = \Storage::disk('spaces');
            $disk->delete(env('BLOG_IMAGE_PATH'). $blog->image);
            $blog->image = null;
            $blog->save();
            return redirect()->route('admin-blog-update',['id' => $blog->id])->with(['success' => 'Blog Image has been Deleted Successfully...']);
        }
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'title' => 'required',
                'description' => 'min:5',
                'image' => 'mimes:jpeg,jpg,png|max:2000'
            ], [
                'title.required' => 'Title is Required',
                'description.min' => 'Minimum 5 character are compulsory',
                'image.mimes' => 'Image must have .jpg,jpeg and png type',
                'image.max' => 'Upload an image less then 2MB'
            ]);

            if($request->image)
            {
                if($request->hasFile('image') && $request->file('image')->isValid())
                {
                    $disk = \Storage::disk('spaces');
                    $image = (string) \Str::uuid() . "." .$request->file('image')->getClientOriginalExtension();
                    $disk->delete(env('BLOG_IMAGE_PATH'). $request->image);
                    $disk->put(env('BLOG_IMAGE_PATH').$image, file_get_contents($request->file('image')->path()));
                    $blog->image = $image;
                }
            }

            $blog->title = $request->title;
            $blog->description = $request->description;
            $blog->status = $request->status;
            $blog->save();

            return redirect()->route('admin-blog-view')->with(['success' => 'Blog has been Updated...']);
        }

        return view('admin.blog.update',[
            'blog' => $blog
        ]);
    }
}
