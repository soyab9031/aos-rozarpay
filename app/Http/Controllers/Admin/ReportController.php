<?php

namespace App\Http\Controllers\Admin;

use App\Models\Package;
use App\Models\PackageOrder;
use App\Models\Payout;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    public function topEarner(Request $request)
    {
        $payouts = Payout::with(['user.detail'])->selectRaw('sum(total) as gross_total, user_id')->groupBy('user_id')->search($request->search, [
           'user.tracking_id', 'user.username', 'user.mobile'
        ])->filterDate($request->dateRange)->orderby('gross_total', 'desc')->paginate(30);

        return view('admin.reports.top-earners', [
            'payouts' => $payouts
        ]);
    }

    public function sales(Request $request)
    {

        if($request->dateRange) {

            $dates = explode('-', $request->dateRange);

            $start_at = Carbon::parse($dates[0])->startOfDay();
            $end_at = Carbon::parse($dates[1])->endOfDay();
        }
        else {

            $start_at = Carbon::now()->subDays(15)->startOfDay();
            $end_at = Carbon::now()->endOfDay();

        }

        $period = CarbonPeriod::create($start_at, $end_at);

        $package_orders =  PackageOrder::selectRaw('sum(amount) as total_amount, DATE(created_at) as created_at')->where([
            ['created_at', '>=', $start_at],
            ['created_at', '<=', $end_at],
        ])->groupBy(\DB::raw('DATE(created_at)'))->get();


        $data = collect($period)->map(function ($date) use ($package_orders, $request) {

            $day_turnover = collect($package_orders)->filter(function ($order) use ($date) {
                return $order->created_at->format('Y-m-d') == $date->format('Y-m-d');
            })->first();

            return (object) [
                'date' => $date,
                'amount' => $day_turnover ? $day_turnover->total_amount : 0,
            ];

        })->sortByDesc('date');

        return view('admin.reports.sales',[
            'turnover' => $data,
        ]);
    }

    public function packageRanking(Request $request)
    {

        $orders = PackageOrder::select(\DB::raw('COUNT(id) as total, package_id'))->groupBy('package_id')->filterDate($request->dateRange)->get();

        $packages = Package::get()->map( function ($package) use ($orders) {

            $sold_package = collect($orders)->filter( function ($order) use ($package) {
                return $order->package_id == $package->id;
            })->first();

            $package->sold_qty = $sold_package ? $sold_package->total : 0;
            return $package;

        })->sortByDesc( function ($package) {
            return $package->sold_qty;
        })->values();


        return view('admin.reports.package-ranking',[
            'packages' => $packages
        ]);
    }
}
