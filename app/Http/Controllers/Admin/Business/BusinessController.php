<?php

namespace App\Http\Controllers\Admin\Business;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BusinessController extends Controller
{
    public function sponsors(Request $request)
    {

        $children = [];
        $searched_user = null;

        if ($request->tracking_id) {

            if (!$searched_user = User::whereTrackingId($request->tracking_id)->first())
                return redirect()->back()->with(['error' => 'Invalid User Id / Tracking ID']);

            $children = User::with(['detail', 'package', 'parentBy.detail'])->search($request->search, [
                'tracking_id', 'detail.first_name', 'detail.last_name', 'mobile'
            ])->filterDate($request->dateRange)->whereSponsorBy($searched_user->id)->get();
        }

        return view('admin.business.sponsors', [
            'searched_user' => $searched_user,
            'children' => $children
        ]);
    }
}
