<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index(Request $request)
    {
        $customers = Customer::with(['user.detail'])->search($request->search, [
            'mobile', 'username', 'email'
        ])->filterDate($request->dateRange);

        return view('admin.customer.index', [
            'customers' => $customers->orderBy('id', 'desc')->paginate(50)
        ]);
    }

    public function update(Request $request)
    {
        if (!$customer = Customer::whereId($request->id)->first())
            return redirect()->back();

        if ($request->isMethod('post')) {

            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email',
                'mobile' => 'required|regex:/^[9876][0-9]{9}$/|digits:10|unique:customers,mobile,' . $customer->id,
                'birth_date' => 'required',
            ], [
                'name.required' => 'Name is Required',
                'email.required' => 'Email Address is required',
                'email.email' => 'Invalid Email Address',
                'mobile.required' => 'Mobile Number is Required',
                'mobile.regex' => 'Invalid Mobile Number',
                'mobile.digits' => 'Mobile Number must be 10 digits',
                'birth_date.required' => 'Date of Birth is Required',
            ]);

            $customer->name = $request->name;
            $customer->email = $request->email;
            $customer->mobile = $request->mobile;
            $customer->birth_date = Carbon::parse($request->birth_date);
            if ($request->password) {
                $customer->password = \Hash::make($customer->password);
            }
            $customer->save();

            return redirect()->route('admin-customer-view')
                ->with(['success' => 'Customer Details has been updated']);
        }

        return view('admin.customer.update', [
            'customer' => $customer
        ]);
    }
    public function accountAccess(Request $request)
    {
        if(!$customer = Customer::whereId($request->id)->first())
            return redirect()->back()->with(['error' => 'Invalid Customer Details for View']);

        return redirect()->route('customer-admin-view-access', ['id' => $customer->id, 'token' => \Session::get('admin')->token]);
    }

    public function apiGetCustomer(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'customer_mobile' => 'required|exists:customers,mobile'
        ]);

        if ($validator->fails())
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);

        $customer = Customer::whereMobile($request->customer_mobile)->with(['user.detail:user_id,first_name,last_name'])->first();

        return response()->json([
            'status' => true,
            'customer' => [
                'id' => $customer->id,
                'mobile' => $customer->mobile,
                'name' => $customer->name,
                'user' => [
                    'id' => $customer->user_id,
                    'name' => $customer->user->detail->full_name,
                    'tracking_id' => $customer->user->tracking_id
                ]
            ]
        ]);
    }
}
