<?php

namespace App\Http\Controllers\Admin;

use App\Models\Event;
use App\Models\EventCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    public function index(Request $request)
    {
        $events = Event::with(['category', 'admin', 'state'])->search($request->search, [
            'name'
        ]);

        if ($request->category_id) {
            $events = $events->whereCategoryId($request->category_id);
        }

        return view('admin.events.index', [
            'events' => $events->latest()->paginate(20),
            'categories' => EventCategory::active()->latest()->get()
        ]);

    }

    public function create(Request $request)
    {
        if ($request->isMethod('post')) {

            $this->validate($request, [
                'category_id' => 'required',
                'name' => 'required',
                'start_at' => 'required',
                'end_at' => 'required',
                'location' => 'required',
                'state_id' => 'required',
            ], [
                'category_id.required' => 'Select Event Category',
                'state_id.required' => 'Select State for event',
            ]);

            Event::create([
                'category_id' => $request->category_id,
                'admin_id' => \Session::get('admin')->id,
                'name' => $request->name,
                'location' => $request->location,
                'state_id' => $request->state_id,
                'description' => $request->description,
                'start_at' => Carbon::parse($request->start_at),
                'end_at' => Carbon::parse($request->end_at),
            ]);

            return redirect()->route('admin-events-view')->with(['success' => 'New Event is Created Successfully']);

        }

        return view('admin.events.create', [
            'categories' => EventCategory::active()->latest()->get()
        ]);
    }

    public function update(Request $request)
    {
        $event = Event::findOrFail($request->id);

        if ($request->isMethod('post')) {

            $this->validate($request, [
                'category_id' => 'required',
                'name' => 'required',
                'start_at' => 'required',
                'end_at' => 'required',
                'location' => 'required',
                'state_id' => 'required',
            ], [
                'category_id.required' => 'Select Event Category',
                'state_id.required' => 'Select State for event',
            ]);

            $event->category_id = $request->category_id;
            $event->admin_id = \Session::get('admin')->id;
            $event->name = $request->name;
            $event->location = $request->location;
            $event->state_id = $request->state_id;
            $event->description = $request->description;
            $event->status = $request->status;
            $event->start_at = Carbon::parse($request->start_at);
            $event->end_at = Carbon::parse($request->end_at);
            $event->save();

            return redirect()->route('admin-events-view')->with(['success' => 'Event is updated successfully']);

        }

        return view('admin.events.update', [
            'event' => $event,
            'categories' => EventCategory::active()->latest()->get()
        ]);

    }

    public function category(Request $request)
    {
        $category = null;

        if ($request->isMethod('post') && $request->id) {

            $category = EventCategory::whereId($request->id)->first();

            $this->validate($request, [
                'name' => 'required|unique:event_categories,name,' . $category->id
            ], [
                'name.required' => 'Category Name is required',
                'name.unique' => 'Category Name should be unique'
            ]);

            $category->name = $request->name;
            $category->status = $request->status;
            $category->save();

            return redirect()->route('admin-event-category')->with(['success' => $category->name . ' Category has been updated.']);

        }
        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'name' => 'required|unique:event_categories,name'
            ], [
                'name.required' => 'Category Name is required',
                'name.unique' => 'Category Name should be unique'
            ]);

            EventCategory::create([
                'name' => $request->name
            ]);

            return redirect()->route('admin-event-category')->with(['success' => 'New Support Category has been added']);
        }
        else if ($request->id) {

            $category = EventCategory::whereId($request->id)->first();
        }


        return view('admin.events.category', [
            'category' => $category,
            'categories' => EventCategory::latest()->get()
        ]);
    }
}
