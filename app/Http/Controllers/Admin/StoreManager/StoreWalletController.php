<?php

namespace App\Http\Controllers\Admin\StoreManager;

use App\Library\ImageUpload;
use App\Models\StoreManager\Store;
use App\Models\StoreManager\StoreWalletRequest;
use App\Models\StoreManager\StoreWalletTransaction;
use App\Models\StorePendingCommission;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StoreWalletController extends Controller
{
    public function index(Request $request)
    {
        $transactions = StoreWalletTransaction::with(['store:id,name,city,tracking_id', 'admin:id,name'])
            ->where('status',StoreWalletTransaction::ACTIVE)
            ->filterDate($request->dateRange);

        if ($request->store_id) {
            $transactions = $transactions->whereStoreId($request->store_id);
        }

        return view('admin.store-manager.store.wallet.index', [
            'stores' => Store::select(['name', 'city', 'id'])->get(),
            'transactions' => $transactions->latest()->paginate(30)
        ]);
    }


    public function createTransaction(Request $request)
    {
        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'store_id' => 'required|exists:stores,id',
                'type' => 'required',
                'amount' => 'required|numeric',
                'remarks' => 'required',
            ],[
                'store_id.required' => 'Store is required',
                'store_id.exists' => 'Invalid Store',
                'type.required' => 'Type is Required',
                'amount.required' => 'Amount is Required',
                'amount.numeric' => 'Amount is Must be Numeric',
                'remarks.required' => 'Remarks is Required'
            ]);

            $store = Store::whereId($request->store_id)->first();

            if ($request->type == StoreWalletTransaction::DEBIT_TYPE && $store->wallet_balance  == 0 )
                return redirect()->back()->with(['error' => 'Wallet cannot debited due to low balance.']);

            if($request->type == StoreWalletTransaction::CREDIT_TYPE)
            {
                $store->credit(collect([
                    'admin_id' => \Session::get('admin')->id,
                    'amount' => $request->amount,
                    'remarks' => $request->remarks,
                ]));
            }
            else
            {
                $store->debit(collect([
                    'admin_id' => \Session::get('admin')->id,
                    'amount' => $request->amount,
                    'remarks' => $request->remarks,
                ]));
            }

            return redirect()->route('admin-store-manager-store-wallet-view', ['store_id' => $store->id])->with(['success' => 'Wallet Transaction is Completed.']);
        }

        return view('admin.store-manager.store.wallet.transaction', [
            'stores' => Store::select(['name', 'tracking_id', 'id'])->get(),
        ]);
    }

    public function requestView(Request $request)
    {
        $wallet_requests = StoreWalletRequest::with(['store:id,name,city,tracking_id', 'admin:id,name'])->filterDate($request->dateRange);

        if ($request->store_id) {
            $wallet_requests = $wallet_requests->whereStoreId($request->store_id);
        }

        return view('admin.store-manager.store.wallet.request.index', [
            'stores' => Store::select(['name', 'city', 'id'])->get(),
            'wallet_requests' => $wallet_requests->latest()->paginate(50)
        ]);
    }

    public function requestUpdate(Request $request)
    {
        $wallet_request = StoreWalletRequest::with(['store:id,name,city,tracking_id', 'admin:id,name'])->whereId($request->id)->first();

        if($request->isMethod('post'))
        {
            if ($request->instant_transfer == 'YES' && $request->status == StoreWalletRequest::APPROVED) {

                $wallet = $wallet_request->store->credit(collect([
                    'admin_id' => \Session::get('admin')->id,
                    'amount' => $wallet_request->amount,
                    'remarks' => 'Credit Against Wallet Request - ' . $request->remarks,
                ]));

                $wallet_request->wallet_id = $wallet->id;
            }

            $wallet_request->admin_id = \Session::get('admin')->id;
            $wallet_request->status = $request->status;
            $wallet_request->remarks = $request->remarks;
            $wallet_request->save();

            return redirect()->route('admin-store-manager-wallet-request-view')->with(['success' => 'Store Wallet Request has been Updated..']);
        }

        return view('admin.store-manager.store.wallet.request.update', [
            'wallet_request' => $wallet_request
        ]);
    }


}
