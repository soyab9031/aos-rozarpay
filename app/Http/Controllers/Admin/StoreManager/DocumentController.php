<?php

namespace App\Http\Controllers\Admin\StoreManager;

use App\Exports\Admin\DocumentExport;
use App\Models\StoreManager\StoreDocument;
use App\Models\StoreManager\StoreStatus;
use App\Models\UserDetail;
use App\Models\UserStatus;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class DocumentController extends Controller
{

    public function index(Request $request)
    {
        $documents = StoreDocument::with(['store'])
            ->search($request->search, [
                'store.tracking_id', 'store.name'
            ])->filterDate($request->dateRange);

        if($request->status)
            $documents->whereStatus($request->status);

//        if ($request->export)
//        {
//            if (empty($request->status) && empty($request->dateRange) && empty($request->package_id) && empty($request->search))
//                return redirect()->back()->with(['error' => 'At least one filter is required to export the report']);
//
//            return \Excel::download(new DocumentExport($documents->get()), 'documents.xlsx');
//        }

        return view('admin.store-manager.store.documents.index',[
            'documents' => $documents->orderBy('id','desc')->paginate(50)
        ]);
    }


    public function update(Request $request)
    {
        $document = StoreDocument::with(['store','store.bank'])->whereId($request->id)->first();

        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'status' => 'required'
            ],[
                'status.required' => 'Status is Required'
            ]);

            \DB::transaction(function () use ($document, $request) {

                if($request->status == StoreDocument::PENDING)
                    $status = StoreStatus::KYC_INPROGRESS;
                elseif($request->status == StoreDocument::VERIFIED)
                    $status = StoreStatus::KYC_VERIFIED;
                else
                    $status = StoreStatus::KYC_REJECTED;

                $document->status = $request->status;
                $document->remarks = $request->remarks;
                $document->save();

                if ($document->type == StoreDocument::PAN_CARD) {
                    StoreStatus::whereStoreId($document->store_id)->update(['pancard' => $status]);
                }

                if ($document->type == StoreDocument::AADHAR_CARD){
                    StoreStatus::whereStoreId($document->store_id)->update(['aadhar_card' => $status]);
                }
                if (in_array($document->type, [StoreDocument::BANK_PASSBOOK, StoreDocument::CANCEL_CHEQUE])){
                    StoreStatus::whereStoreId($document->store_id)->update(['bank_account' => $status]);
                }

            });

            return redirect()->route('admin-store-manager-document-view')->with(['success' => 'Document Update has been successfully..']);

        }

        return view('admin.store-manager.store.documents.update', [
            'document' => $document
        ]);
    }


}
