<?php

namespace App\Http\Controllers\Admin\StoreManager;

use App\Models\StoreManager\StockTransaction;
use App\Models\StoreManager\Store;
use App\Models\StoreManager\StoreStockRequest;
use App\Models\StoreManager\StoreSupply;
use App\Models\StoreManager\StoreSupplyDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StoreStockRequestController extends Controller
{
    public function index(Request $request)
    {
        $stock_requests = StoreStockRequest::with(['store:id,name,tracking_id', 'details:request_id','receiverBy'])->filterDate($request->dateRange)->latest();

        if ($request->store_id) {
            $stock_requests = $stock_requests->whereStoreId($request->store_id);
        }

        return view('admin.store-manager.store.stock-request.index', [
            'stores' => Store::select(['name', 'city', 'id'])->get(),
            'stock_requests' => $stock_requests->paginate(30)
        ]);
    }

    public function details(Request $request)
    {
        $stock_request = StoreStockRequest::with(['store', 'details.product_price.product:id,name'])->whereId($request->id)->first();

        if (!$stock_request)
            return redirect()->back()->with(['error' => 'Not able to find the request, try again']);

        return view('admin.store-manager.store.stock-request.detail', [
            'stock_request' => $stock_request
        ]);
    }

    public function transferToSupply(Request $request)
    {
        $stock_request = StoreStockRequest::with(['store', 'details.product_price.product:id,name'])->whereId($request->id)->first();

        $this->validate($request, [
            'status' => 'required',
            'delivery_type' => 'required_if:direct_supply,1',
            'remarks' => 'required_if:direct_supply,1'
        ], [
            'delivery_type.required_if' => 'Delivery Type is required for Direct Supply',
            'remarks.required_if' => 'Your Message or Remarks is required for Direct Supply'
        ]);

        if ($request->direct_supply == 2) {

            $stock_request->status = $request->status;
            $stock_request->save();

            return redirect()->route('admin-store-manager-store-request-details', ['id' => $stock_request->id])->with(['success' => 'Stock Request is Updated Successfully']);
        }

        $store = Store::whereId($stock_request->store_id)->first();

        if ($store->wallet_balance < $stock_request->amount)
            return redirect()->back()->with(['error' => 'Store has not enough balance to Complete the Supply']);

        $balance_query = 'COALESCE(SUM(CASE WHEN type = 1 THEN qty END),0) - COALESCE(SUM(CASE WHEN type = 2 THEN qty END),0)';

        /* Stock Check Start */
        $exist_stock_items = StockTransaction::selectRaw($balance_query . ' as balance, product_price_id')
            ->groupBy('product_price_id')->havingRaw($balance_query . '> 0')
            ->whereIn('product_price_id', collect($stock_request->details)->pluck('product_price_id')->toArray())->get();

        $item_not_exists = collect($stock_request->details)->filter(function ($detail) use ($exist_stock_items) {

            $current_item = collect($exist_stock_items)->where('product_price_id', $detail->product_price_id)->first();

            if (!$current_item)
                return !$current_item;

            return $detail->qty > $current_item->balance;
        })->first();

        if ($item_not_exists) {
            return redirect()->back()->with(['error' => $item_not_exists->product_price->product->name . ' has not enough Stock, Remove This Item or Try again']);
        }
        /* Stock Check End */

        $supply = \DB::transaction(function () use ($stock_request, $store, $request) {

            $supply = StoreSupply::create([
                'admin_id' => \Session::get('admin')->id,
                'store_id' => $store->id,
                'delivery_type' => $request->delivery_type,
                'amount' => $stock_request->amount,
                'tax_amount' => 0,
                'total' => $stock_request->amount,
                'courier_name' => '',
                'courier_docket_number' => '',
                'remarks' => $request->remarks
            ]);

            collect($stock_request->details)->map(function ($detail) use ($supply, $store) {

                StoreSupplyDetail::create([
                    'supply_id' => $supply->id,
                    'product_price_id' => $detail->product_price_id,
                    'price' => $detail->price,
                    'distributor_price' => $detail->distributor_price,
                    '',
                    'qty' => $detail->qty,
                    'gst' => json_encode($detail->product_price->gst)
                ]);

                StockTransaction::create([
                    'admin_id' => \Session::get('admin')->id,
                    'supply_id' => $supply->id,
                    'store_id' => $supply->store_id,
                    'product_price_id' => $detail->product_price_id,
                    'type' => StockTransaction::DEBIT,
                    'qty' => $detail->qty
                ]);

                $store->stockCredit(collect([
                    'admin_id' => \Session::get('admin')->id,
                    'supply_id' => $supply->id,
                    'product_price_id' => $detail->product_price_id,
                    'qty' => $detail->qty
                ]));

            });

            $store->debit(collect([
                'admin_id' => \Session::get('admin')->id,
                'amount' => $stock_request->amount,
                'remarks' => 'Debit towards Supply from Company #' . $supply->id
            ]));

            $stock_request->status = $request->status;
            $stock_request->save();

            return $supply;

        });

        return redirect()->route('admin-store-manager-store-supply-detail', ['id' => $supply->id])->with(['success' => 'New Supply Successfully Transferred']);
    }
}
