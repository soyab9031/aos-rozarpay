<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\WebContent;
use Illuminate\Http\Request;

class WebContentController extends Controller
{
    public function index()
    {
        $web_contents = WebContent::with('admin')->get();

        return view('admin.web-content.index',[
            'web_contents' => $web_contents
        ]);
    }

    public function update(Request $request)
    {
        $web_content = WebContent::with('admin')->whereId($request->id)->first();

        if($request->isMethod('post'))
        {

            $this->validate($request, [
                'title' => 'required',
                'image' => 'mimes:jpeg,jpg,png|max:2000',
                'content' => 'min:5',
            ],[
                'title.required' => 'Title is required',
                'image.mimes' => 'Image must have .jpg,jpeg and png type',
                'image.max' => 'Upload an image less then 2MB',
                'content.min' => 'Minimum 5 character are compulsory',
            ]);

            if($request->image)
            {
                if($request->hasFile('image') && $request->file('image')->isValid())
                {
                    $disk = \Storage::disk('spaces');
                    $image = (string) \Str::uuid() . "." .$request->file('image')->getClientOriginalExtension();
                    $disk->delete(env('WEB_CONTENT_IMAGE_PATH'). $request->image);
                    $disk->put(env('WEB_CONTENT_IMAGE_PATH').$image, file_get_contents($request->file('image')->path()));
                    $web_content->image = $image;
                }
            }

            $web_content->admin_id = \Session::get('admin')->id;
            $web_content->title = $request->title;
            $web_content->description = $request->description;
            $web_content->save();

            return redirect()->route('admin-web-content-view')->with(['success' => 'Website Content has been Uploaded.']);
        }

        return view('admin.web-content.update',[
            'web_content' => $web_content
        ]);
    }
}
