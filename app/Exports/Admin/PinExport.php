<?php

namespace App\Exports\Admin;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PinExport implements FromCollection, WithHeadings
{
    protected $pins;

    function __construct($pins)
    {
        $this->pins = $pins;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return collect($this->pins)->map(function ($pin) {

            return [
                $pin->created_at->format('M d, Y'),
                $pin->user->detail->full_name,
                $pin->user->tracking_id,
                $pin->package->name,
                $pin->package->amount,
                $pin->number,
                $pin->package_order ? $pin->package_order->detail->full_name . ' (' . $pin->package_order->tracking_id . ')' : '',
                $pin->status_label
            ];

        });
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Created Date',
            'User',
            'Tracking Id',
            'Package',
            'Package Amount',
            'Pin',
            'Used By',
            'Status',
        ];
    }
}
