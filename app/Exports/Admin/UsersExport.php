<?php

namespace App\Exports\Admin;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection, WithHeadings
{
    protected $users;

    public function __construct($users)
    {
        $this->users = $users;
    }

    public function collection(): \Illuminate\Support\Collection
    {
        return collect($this->users)->map(function ($user) {
            return [
                Carbon::parse($user->created_at)->format('M d, Y h:i A'),
                $user->paid_at ? Carbon::parse($user->paid_at)->format('M d, Y h:i A') : 'N.A',
                $user->package ? $user->package->name : 'N.A',
                $user->detail->full_name,
                $user->username,
                $user->tracking_id,
                $user->sponsorBy ? $user->sponsorBy->detail->first_name . ' ' . $user->sponsorBy->detail->last_name . ' (' . $user->sponsorBy->tracking_id . ')' : '',
                $user->parentBy ? $user->parentBy->detail->first_name . ' ' . $user->parentBy->detail->last_name . ' (' . $user->parentBy->tracking_id . ')' : '',
                $user->leg,
                $user->password,
                $user->wallet_password,
                $user->detail->pan_number,
                $user->mobile,
                $user->email,
                $user->bank ? $user->bank->bank_name : '--',
                $user->bank ? $user->bank->account_number : '--',
                $user->bank ? $user->bank->ifsc : '--',
                $user->bank ? $user->bank->branch : '--',
                $user->address->address,
                $user->address->landmark,
                $user->address->city,
                $user->address->district,
                $user->address->pincode,
                $user->address->state->name
            ];
        });
    }

    public function headings(): array
    {
        return ['Joining Date', 'Activation Date', 'Package', 'Name', 'Username', 'tracking_id', 'Sponsor', 'Upline', 'Leg', 'Password', 'TXN Password', 'PAN No.', 'Mobile', 'Email', 'Bank Name', 'Account Number', 'IFSC', 'Branch', 'Address', 'Landmark', 'City', 'District', 'Pincode', 'State',
        ];
    }
}
