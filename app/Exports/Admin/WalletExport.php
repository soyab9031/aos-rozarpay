<?php

namespace App\Exports\Admin;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class WalletExport implements FromCollection, WithHeadings
{

    protected $wallets;

    public function __construct($wallets)
    {
        $this->wallets = $wallets;
    }

    public function collection(): \Illuminate\Support\Collection
    {
        return collect($this->wallets)->map(function ($wallet) {

            return [
                $wallet->created_at->format('M d, Y'),
                $wallet->user->detail->full_name . " (" . $wallet->user->tracking_id . ")",
                $wallet->total,
                $wallet->tds,
                $wallet->admin_charge,
                $wallet->amount,
                $wallet->income_type_name,
                $wallet->remarks
            ];

        });
    }

    public function headings(): array
    {
        return [
          'Created Date', 'User', 'Total', 'TDS', 'Amount', 'Income Types', 'Remarks'
        ];
    }
}
