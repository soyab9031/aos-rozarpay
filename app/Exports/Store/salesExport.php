<?php

namespace App\Exports\Store;

use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class salesExport implements FromCollection, WithHeadings
{

    protected $records;

    public function __construct($records)
    {
        $this->records = $records;
    }

    public function collection(): \Illuminate\Support\Collection
    {
        return collect($this->records)->map(function ($record) {

            return [
                Carbon::parse($record->date)->format('M d, Y'),
                number_format($record->amount),
                number_format($record->order_count)
            ];

        });
    }

    public function headings(): array
    {
        return [
            'Date', 'Amount', 'Orders'
        ];
    }
}
