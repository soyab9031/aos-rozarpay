<?php

namespace App\Library\Calculation\Binary;


use App\Models\Binary\BinaryAchievement;
use App\Models\Binary\BinaryCalculation;
use App\Models\Binary\BinaryCalculationPvLog;
use App\Models\PackageOrder;
use App\Models\UserStatus;
use Carbon\Carbon;

class Builder
{
    /**
     * Add Member / User to Binary Tree
     * Generally used while registration completed, in cron
     */
    public function addToTree()
    {
        UserStatus::with(['user'])->pendingTreeCalculation()->chunk(20, function ($user_statuses) {

            \DB::transaction(function () use ($user_statuses) {

                collect($user_statuses)->map( function ($user_status) {

                    (new Helper())->parents($user_status->user_id, $parents);

                    $leg = $user_status->user->leg;
                    foreach ($parents as $index => $parent) {

                        if($exist_in_calculation = BinaryCalculation::whereUserId($parent->id)->open()->first())
                        {
                            if($leg == 'L')
                                BinaryCalculation::whereId($exist_in_calculation->id)->increment('left', 1);

                            elseif($leg == 'R')
                                BinaryCalculation::whereId($exist_in_calculation->id)->increment('right', 1);

                        }
                        else
                        {
                            BinaryCalculation::create([
                                'user_id' => $parent->id, 'left' => $leg == 'L' ? 1 : 0, 'right' => $leg == 'R' ? 1 : 0
                            ]);
                        }

                        $leg = $parent->leg;
                    }

                    $user_status->tree_calculation = UserStatus::TREE_CALC_YES;
                    $user_status->save();

                });
            });

        });
    }

    /**
     * Add PV / Points to Binary Tree
     * Usage in Cron or Manually at User View
     */
    public function addPvToTree()
    {
        PackageOrder::with(['user:leg,id'])->where('pv', '>', 0)->wherePvCalculation(PackageOrder::NO)->chunk(20, function ($package_orders) {

            \DB::transaction(function () use ($package_orders) {

                collect($package_orders)->map( function ($package_order) {

                    if ($package_order->user_id == 1) {
                        PackageOrder::whereId($package_order->id)->update(['pv_calculation' => UserStatus::YES]);
                        return false;
                    }

                    (new Helper())->parents($package_order->user_id, $parents);

                    $pv = $package_order->pv;
                    $leg = $package_order->user->leg;

                    foreach ($parents as $index => $parent) {

                        if($exist_in_calculation = BinaryCalculation::whereUserId($parent->id)->open()->first())
                        {
                            if($leg == 'L')
                            {
                                BinaryCalculation::whereId($exist_in_calculation->id)->update([
                                    'current_left' => \DB::raw("current_left + $pv"),
                                    'total_left' => \DB::raw("total_left + $pv")
                                ]);
                            }
                            elseif($leg == 'R')
                            {
                                BinaryCalculation::whereId($exist_in_calculation->id)->update([
                                    'current_right' => \DB::raw("current_right + $pv"),
                                    'total_right' => \DB::raw("total_right + $pv")
                                ]);
                            }
                        }
                        else
                        {
                            BinaryCalculation::create([
                                'user_id' => $parent->id,
                                'current_left' => $leg == 'L' ? $pv : 0,
                                'total_left' => $leg == 'L' ? $pv : 0,
                                'current_right' => $leg == 'R' ? $pv : 0,
                                'total_right' => $leg == 'R' ? $pv : 0
                            ]);

                        }

                        /* == PV Log == */
                        (new BinaryCalculationPvLog())->createLog($package_order, $parent->id, $leg);

                        $leg = $parent->leg;

                    }

                    PackageOrder::whereId($package_order->id)->update(['pv_calculation' => UserStatus::YES]);

                });

            });

        });
    }

    /**
     * Binary Qualification: Binary Calculation only occur
     * if User is Binary Qualified
     */
    private function makeQualify()
    {

    }

    /**
     * Binary Calculation, Match the paid between Left & Right Leg
     * TODO: Carry Forward PV Base is 1, We can increase as Requirement
     * 2:1 or 1:2 then After Words 1:1
     */
    public function calculation()
    {
        /* Make Binary Qualify */
        $this->makeQualify();

        /* Case 1: (2:1 or 1:2) */
        BinaryCalculation::whereHas('user.status', function ($q) {
            $q->whereNotNull('binary_qualified_at');
        })->whereStatus(BinaryCalculation::OPEN)
            ->whereRaw('( ( (forward_left+current_left) >= 2 AND (forward_right+current_right) >= 1 ) OR ( (forward_right+current_right) >= 2 AND (forward_left+current_left) >= 1 ) )')->get()->map( function ($calculation) {


                if (BinaryAchievement::whereUserId($calculation->user_id)->count() == 0)
                {
                    $A = 2; $B = 1;

                    $total_left = $calculation->forward_left+$calculation->current_left;
                    $total_right = $calculation->forward_right+$calculation->current_right;

                    if ($total_left >= $A && $total_right >= $B)
                    {
                        if ($total_left > $total_right)
                        {
                            $forward_left = $total_left - $A;
                            $forward_right = $total_right - $B;
                        }
                        else {
                            $forward_left = $total_left - $B;
                            $forward_right = $total_right - $A;
                        }

                        $this->closedCalculation($calculation, $forward_left, $forward_right, 1);
                    }
                    elseif ($total_left >= $B && $total_right >= $A)
                    {
                        if ($total_left > $total_right)
                        {
                            $forward_left = $total_left - $A;
                            $forward_right = $total_right - $B;
                        }
                        else {
                            $forward_left = $total_left - $B;
                            $forward_right = $total_right - $A;
                        }

                        $this->closedCalculation($calculation, $forward_left, $forward_right, 1);
                    }
                }

            });

        /* Case 2: (1:1) */
        BinaryCalculation::whereHas('user.status', function ($q) {
            $q->whereNotNull('binary_qualified_at');
        })->whereStatus(BinaryCalculation::OPEN)
            ->whereRaw('( ( (forward_left+current_left) >= 1 AND (forward_right+current_right) >= 1 ) )')
            ->get()->map( function ($calculation) {

                if (BinaryAchievement::whereUserId($calculation->user_id)->count() >= 1)
                {
                    $total_achievements = BinaryAchievement::whereUserId($calculation->user_id)->count();

                    $total_left = $calculation->forward_left+$calculation->current_left;
                    $total_right = $calculation->forward_right+$calculation->current_right;

                    $forward_left = $total_left - 1;
                    $forward_right = $total_right - 1;

                    $this->closedCalculation($calculation, $forward_left, $forward_right, $total_achievements+1);
                }

            });
    }

    /**
     * @param BinaryCalculation $calculation
     * @param integer $forward_left
     * @param integer $forward_right
     * @param integer $calculation_reference_id
     */
    private function closedCalculation($calculation, $forward_left, $forward_right, $calculation_reference_id)
    {

        BinaryAchievement::create([
            'user_id' => $calculation->user_id,
            'calculation_reference_id' => $calculation_reference_id,
            'calculation_id' => $calculation->id,
            'status' => BinaryAchievement::ACTIVE
        ]);

        BinaryCalculation::whereId($calculation->id)->update(['status' => BinaryCalculation::CLOSED, 'closed_at' => Carbon::now()]);

        BinaryCalculation::create([
            'user_id' => $calculation->user_id,
            'left' => $calculation->left,
            'right' => $calculation->right,
            'total_left' => $calculation->total_left,
            'total_right' => $calculation->total_right,
            'forward_left' => $forward_left,
            'forward_right' => $forward_right,
        ]);
    }


}
