<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class WebContent extends Model
{
    use Eloquence;
    const ACTIVE = 1, INACTIVE = 2;
    const ABOUT = 1, MISSION = 2, VISION = 3, PROMOTIONS = 4, BUSINESS_OPPORTUNITY = 5;

    protected $fillable = ['admin_id', 'title', 'image', 'description', 'type', 'status'];

    public function admin(){
        return $this->belongsTo(Admin::class);
    }

    public function scopeActive($q){
        return $q->where('status',self::ACTIVE);
    }
}
