<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class PaymentGatewayTransaction extends Model
{
    use Eloquence;

    const PENDING = 1, SUCCESS = 2, FAILED = 3, CANCEL = 4;

    protected $casts = [
        'response' => 'object'
    ];


    protected $fillable = [
        'order_id', 'provider', 'reference_id', 'response', 'status'
    ];
}
