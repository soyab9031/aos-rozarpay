<?php

namespace App\Models\StoreManager;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class StoreType extends Model
{
    use Eloquence;
    const ACTIVE = 1, INACTIVE = 2;

    protected $fillable = [
        'name', 'amount', 'branding_investment', 'status'
    ];

    public function store()
    {
        $this->belongsTo(Store::class,'type','id');
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::ACTIVE);
    }
}
