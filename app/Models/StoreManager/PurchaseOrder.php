<?php

namespace App\Models\StoreManager;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\StoreManager\PurchaseOrder
 *
 * @property int $id
 * @property int $admin_id
 * @property int $supplier_id
 * @property string|null $customer_order_id
 * @property int|null $delivery_days
 * @property float $amount
 * @property float $shipping_amount
 * @property float $tax_amount
 * @property float $total
 * @property string|null $remarks
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Admin $admin
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\StoreManager\PurchaseOrderDetail[] $details
 * @property-read int|null $details_count
 * @property-read \App\Models\StoreManager\Supplier $supplier
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\PurchaseOrder newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\PurchaseOrder newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\PurchaseOrder query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\PurchaseOrder whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\PurchaseOrder whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\PurchaseOrder whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\PurchaseOrder whereCustomerOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\PurchaseOrder whereDeliveryDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\PurchaseOrder whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\PurchaseOrder whereRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\PurchaseOrder whereShippingAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\PurchaseOrder whereSupplierId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\PurchaseOrder whereTaxAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\PurchaseOrder whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\PurchaseOrder whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PurchaseOrder extends Model
{
    use Eloquence;

    protected $fillable = [
        'admin_id', 'supplier_id', 'customer_order_id', 'delivery_days', 'amount', 'shipping_amount', 'tax_amount', 'total', 'remarks'
    ];

    public function details()
    {
        return $this->hasMany(PurchaseOrderDetail::class, 'order_id');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
