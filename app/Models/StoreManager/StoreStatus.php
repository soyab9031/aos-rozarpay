<?php

namespace App\Models\StoreManager;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class StoreStatus extends Model
{
    use Eloquence;

    CONST FAKE_TYPE = 1, REAL_TYPE = 0;
    CONST YES = 1, NO = 0, PENDING = 2;
    const KYC_PENDING = 1, KYC_VERIFIED = 2, KYC_INPROGRESS = 3, KYC_REJECTED = 4;

    protected $fillable = [
        'store_id',
        'bank_account',
        'pancard',
        'aadhar_card'
    ];

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public static function  getKycStatuses()
    {
        return [
            self::KYC_PENDING => 'PENDING',
            self::KYC_VERIFIED => 'VERIFIED',
            self::KYC_INPROGRESS => 'IN PROGRESS',
            self::KYC_REJECTED => 'REJECTED',
        ];
    }
}
