<?php

namespace App\Models\StoreManager;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class StoreBank extends Model
{
    use Eloquence;

    protected $fillable = [
        'store_id', 'bank_name', 'account_name', 'account_number', 'branch', 'ifsc', 'type', 'city'
    ];
}
