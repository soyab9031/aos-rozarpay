<?php

namespace App\Models\StoreManager;

use App\Library\Helper;
use App\Models\Order;
use App\Models\State;
use App\Models\StoreStatus;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\StoreManager\Store
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $parent_id
 * @property string $tracking_id
 * @property string $password
 * @property string $name
 * @property string $mobile
 * @property string|null $email
 * @property string $address
 * @property string $city
 * @property string $pincode
 * @property int|null $state_id
 * @property int $status 1: Active, 2: Inactive
 * @property int $type 1: Master, 2: Mini, 3: Warehouse
 * @property string|null $last_logged_in_ip
 * @property string|null $last_logged_in_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $wallet_balance
 * @property-read \App\Models\State|null $state
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Store active()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\Store newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\Store newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\Store query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Store whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Store whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Store whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Store whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Store whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Store whereLastLoggedInAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Store whereLastLoggedInIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Store whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Store whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Store whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Store wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Store wherePincode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Store whereStateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Store whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Store whereTrackingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Store whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Store whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Store whereUserId($value)
 * @mixin \Eloquent
 * @property int|null $reference_user_id
 * @property-read \App\Models\User|null $reference_user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\Store whereReferenceUserId($value)
 */
class Store extends Model
{
    use Eloquence;

    CONST ACTIVE = 1, INACTIVE = 2;
    CONST PREMIUM_TYPE = 1, PRO_TYPE = 2, STANDARD_TYPE = 3, BASIC_TYPE = 4;

    protected $fillable = [
        'user_id', 'parent_id', 'tracking_id', 'password', 'name', 'mobile', 'email', 'address', 'city', 'pincode', 'state_id',
        'amount', 'branding_investment',
        'status', 'type', 'last_logged_in_ip', 'last_logged_in_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function bank()
    {
        return $this->hasOne(StoreBank::class);
    }

    public function reference_user()
    {
        return $this->belongsTo(User::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function scopeActive($q)
    {
        $q->where('status', self::ACTIVE);
    }

    public function getWalletBalanceAttribute()
    {
        $wallet = StoreWalletTransaction::selectRaw('COALESCE(SUM(CASE WHEN type = 1 THEN amount END),0) - COALESCE(SUM(CASE WHEN type = 2 THEN amount END),0) as balance')->where('status',StoreWalletTransaction::ACTIVE)->whereStoreId($this->id)->first();

        return $wallet->balance;
    }

    /**
     * Store's Wallet Credit
     *
     * @param  Collection  $details
     * @return StoreWalletTransaction
     */
    public function credit($details)
    {
        $amount = $details->get('amount');

        return StoreWalletTransaction::create([
            'admin_id' => $details->get('admin_id'),
            'order_id' => $details->get('order_id'),
            'store_id' => $this->id,
            'opening_amount' => $this->wallet_balance,
            'amount' => $amount,
            'closing_amount' => $this->wallet_balance + $amount,
            'type' => StoreWalletTransaction::CREDIT_TYPE,
            'remarks' => $details->get('remarks'),
            'status' => $details->get('status') ? $details->get('status') : StoreWalletTransaction::ACTIVE
        ]);

    }

    /**
     * Store`s Wallet Debit
     *
     * @param  Collection  $details
     * @return StoreWalletTransaction
     */
    public function debit($details)
    {
        $amount = $details->get('amount');

        $wallet = StoreWalletTransaction::create([
            'admin_id' => $details->get('admin_id'),
            'store_id' => $this->id,
            'opening_amount' => $this->wallet_balance,
            'amount' => $amount,
            'closing_amount' => $this->wallet_balance - $amount,
            'type' => StoreWalletTransaction::DEBIT_TYPE,
            'remarks' => $details->get('remarks'),
            'status' => $details->get('status') ? $details->get('status') : StoreWalletTransaction::ACTIVE
        ]);

        return $wallet;
    }

    public function getProductStock($id)
    {
        $stock_transaction = StoreStockTransaction::selectRaw('COALESCE(SUM(CASE WHEN type = 1 THEN qty END), 0) - COALESCE(SUM(CASE WHEN type = 2 THEN qty END), 0) as balance')
            ->whereStoreId($this->id)->where('product_price_id', $id)->first();

        return $stock_transaction->balance;
    }

    /**
     * Stock stock Credit
     * @param Collection $details
     * @return StoreStockTransaction
     */
    public function stockCredit($details)
    {

        $current_balance = $this->getProductStock($details->get('product_price_id'));

        return StoreStockTransaction::create([
            'store_id' => $this->id,
            'admin_id' => $details->get('admin_id') ? $details->get('admin_id') : null,
            'order_id' => $details->get('order_id'),
            'product_price_id' => $details->get('product_price_id'),
            'opening' => $current_balance,
            'qty' => $details->get('qty'),
            'closing' => $details->get('qty') + $current_balance,
            'type' => StoreStockTransaction::CREDIT,
            'remarks' => $details->get('remarks')
        ]);

    }

    /**
     * Stock stock Debit
     * @param Collection $details
     * @return StoreStockTransaction
     */
    public function stockDebit($details)
    {
        $current_balance = $this->getProductStock($details->get('product_price_id'));

        return StoreStockTransaction::create([
            'store_id' => $this->id,
            'admin_id' => $details->get('admin_id') ? $details->get('admin_id') : null,
            'order_id' => $details->get('order_id'),
            'product_price_id' => $details->get('product_price_id'),
            'opening' => $current_balance,
            'qty' => $details->get('qty'),
            'closing' => $current_balance - $details->get('qty'),
            'type' => StoreStockTransaction::DEBIT,
            'remarks' => $details->get('remarks')
        ]);

    }

    public function sendStockRequestOptions()
    {
        $options = null;
        if($this->type == self::BASIC_TYPE || $this->type == self::STANDARD_TYPE){
            $options = [
                ['value' => 2, 'label' => 'Premium'],
                ['value' => 3, 'label' => 'Pro'],
            ];
        }elseif ($this->type == self::PRO_TYPE){
            $options = [
                ['value' => 1, 'label' => 'Company'],
                ['value' => 2, 'label' => 'Premium'],
            ];
        }else{
            $options = [
                ['value' => 1, 'label' => 'Company'],
            ];
        }

        return $options;
    }

    public static function generateTrackingId($type)
    {
        return self::getStoreTypes($type)->prefix . rand(1000000, 9999999);
    }

    public static function getStoreTypes($id = null)
    {
        $types = [
            ['id' => 1, 'name' => 'Premium', 'prefix' => 'PRE'],
            ['id' => 2, 'name' => 'Pro', 'prefix' => 'PRO'],
            ['id' => 3, 'name' => 'Standard', 'prefix' => 'STD'],
            ['id' => 4, 'name' => 'Basic', 'prefix' => 'BAS'],
        ];

        $types = Helper::arrayToObject($types);

        if ($id) {
            return collect($types)->where('id', $id)->first();
        }

        return $types;
    }

}
