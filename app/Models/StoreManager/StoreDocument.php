<?php

namespace App\Models\StoreManager;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class StoreDocument extends Model
{
    use Eloquence;
    const PAN_CARD = 1, AADHAR_CARD = 2, CANCEL_CHEQUE = 3, BANK_PASSBOOK = 4;
    const PENDING = 1, VERIFIED = 2, REJECTED = 3;

    protected $fillable = [
        'store_id', 'image', 'secondary_image', 'type', 'status', 'number'
    ];

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function getDocumentNameAttribute()
    {
        $documents = collect(self::documents())->flatten()->toArray();

        return isset($documents[($this->type - 1)]) ? $documents[($this->type - 1)] : 'Other';
    }

    public static function documents($type = null)
    {
        $document_names =  [
            'PanCard' => [
                1 => 'PAN CARD'
            ],
            'Aadhar' =>  [
                2 => 'AADHAR CARD',
            ],
            'Bank' =>  [
                3 => 'CANCEL CHEQUE',
                4 => 'BANK PASSBOOK',
            ]
        ];

        return $type ? (isset($document_names[$type]) ? $document_names[$type] : null) : $document_names;
    }
}
