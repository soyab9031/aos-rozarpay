<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class UserWishList extends Model
{
    use Eloquence;

    protected $fillable = [
        'user_id', 'customer_id', 'product_price_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function product_price()
    {
        return $this->belongsTo(ProductPrice::class);
    }
}
