<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\PackageItem
 *
 * @property int $id
 * @property int $package_id
 * @property string|null $gst
 * @property string $name
 * @property float $amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $actual_amount
 * @property-read mixed $tax_amount
 * @property-read \App\Models\Package $package
 * @method static \Sofa\Eloquence\Builder|\App\Models\PackageItem newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\PackageItem newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\PackageItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageItem whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageItem whereGst($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageItem whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageItem wherePackageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageItem whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|PackageItem aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|PackageItem avg($column)
 * @method static \Sofa\Eloquence\Builder|PackageItem callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|PackageItem count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|PackageItem filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|PackageItem getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|PackageItem joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|PackageItem leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|PackageItem lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|PackageItem max($column)
 * @method static \Sofa\Eloquence\Builder|PackageItem min($column)
 * @method static \Sofa\Eloquence\Builder|PackageItem orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|PackageItem orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|PackageItem orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|PackageItem orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|PackageItem orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|PackageItem orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|PackageItem orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|PackageItem prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|PackageItem rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|PackageItem search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|PackageItem select($columns = [])
 * @method static \Sofa\Eloquence\Builder|PackageItem setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|PackageItem setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|PackageItem sum($column)
 * @method static \Sofa\Eloquence\Builder|PackageItem whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|PackageItem whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PackageItem whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PackageItem whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|PackageItem whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|PackageItem whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PackageItem whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PackageItem whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PackageItem whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PackageItem whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|PackageItem whereYear($column, $operator, $value, $boolean = 'and')
 */
class PackageItem extends Model
{
    use Eloquence;

    protected $fillable = [
        'package_id', 'name', 'amount', 'gst' , 'qty'
    ];

    public function package()
    {
        return $this->belongsTo(Package::class);
    }

    public function getGstAttribute($value)
    {
        return $value ? json_decode($value) : null;
    }

    public function getTaxAmountAttribute()
    {
        return round(($this->amount*$this->gst->percentage)/((float)100+(float)$this->gst->percentage), 4);
    }

    public function getActualAmountAttribute()
    {
        return round(($this->amount - $this->tax_amount), 2);
    }

    public static function getGstPercentage()
    {
        return [0, 5, 12, 18, 28];
    }
}
