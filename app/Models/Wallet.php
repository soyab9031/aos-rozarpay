<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Wallet
 *
 * @property int $id
 * @property int|null $admin_id
 * @property int $user_id
 * @property int|null $payout_id
 * @property int $type 1:Credit, 2:Debit
 * @property int $income_type 1:Binary, 2:Sponsor, 3:Re-Purchase, 4:Level, 5:Royalty, 6:Reward, 7:ROI, 8:Matrix
 * @property float $total
 * @property float $tds
 * @property float|null $admin_charge
 * @property float|null $service_charge
 * @property float $amount
 * @property string|null $remarks
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\Admin|null $admin
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereAdminCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereIncomeType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet wherePayoutId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereServiceCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereTds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereUserId($value)
 * @mixin \Eloquent
 * @property-read mixed $income_type_name
 * @method static \Sofa\Eloquence\Builder|\App\Models\Wallet newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Wallet newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Wallet query()
 * @method static \Sofa\Eloquence\Builder|Wallet aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|Wallet avg($column)
 * @method static \Sofa\Eloquence\Builder|Wallet callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|Wallet count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|Wallet filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|Wallet getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|Wallet joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|Wallet leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|Wallet lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|Wallet max($column)
 * @method static \Sofa\Eloquence\Builder|Wallet min($column)
 * @method static \Sofa\Eloquence\Builder|Wallet orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|Wallet orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|Wallet orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|Wallet orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|Wallet orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|Wallet orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|Wallet orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|Wallet prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|Wallet rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|Wallet search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|Wallet select($columns = [])
 * @method static \Sofa\Eloquence\Builder|Wallet setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|Wallet setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|Wallet sum($column)
 * @method static \Sofa\Eloquence\Builder|Wallet whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Wallet whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Wallet whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Wallet whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Wallet whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Wallet whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Wallet whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Wallet whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Wallet whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Wallet whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Wallet whereYear($column, $operator, $value, $boolean = 'and')
 */
class Wallet extends Model
{
    use Eloquence;

    const CREDIT_TYPE = 1, DEBIT_TYPE = 2;
    const BINARY_TYPE = 1, SPONSOR_TYPE = 2, OTHER_TYPE = 3, LEVEL_TYPE = 4, REWARD_TYPE = 5, ENROLLMENT_TEAM_BONUS = 6 , RECURRING_TEAM_BONUS = 7, CHEQUE_MATCHING_BONUS = 8, RANK_BONUS = 9, OUTLET_CHEQUE_BONUS = 10, MONTHLY_PERFORMANCE_BONUS= 11;

    protected $fillable = [
        'admin_id', 'payout_id', 'user_id', 'total', 'tds','admin_charge', 'service_charge', 'amount', 'type', 'income_type', 'remarks'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function getIncomeTypeNameAttribute()
    {
        return $this->income_type > 0 ? self::incomeTypes($this->income_type) : 'N.A';
    }

    public static function incomeTypes($type = null)
    {
        $income_types =  [
            0 => 'OTHER',
            1 => 'BINARY',
            2 => 'SPONSOR',
            3 => 'OTHER',
            4 => 'LEVEL',
            5 => 'REWARD',
            6 => 'ENROLLMENT-TEAM-BONUS',
            7 => 'RECURRING-TEAM-BONUS',
            8 => 'CHEQUE-MATCHING-BONUS',
            9 => 'RANK-BONUS',
            10 => 'OUTLET-CHEQUE-BONUS',
            11 => 'MONTHLY-PERFORMANCE-BONUS'
        ];

        return $type ? (isset($income_types[$type]) ? $income_types[$type] : null) : $income_types;
    }

    /**
     * @param string $type
     * @return mixed
     */
    public static function getIncomeTypeId($type)
    {
        return array_search(strtoupper($type), self::incomeTypes());
    }


}
