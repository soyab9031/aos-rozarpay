<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Sofa\Eloquence\Eloquence;

class Customer extends Model
{
    use Eloquence;

    protected $fillable = [
        'tracking_id', 'user_id', 'name', 'email', 'mobile', 'birth_date', 'password', 'gender', 'last_logged_in_ip', 'last_logged_in_at'
    ];

    protected $hidden = [
        'password'
    ];

    public function address()
    {
        return $this->hasOne(UserAddress::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'customer_id');
    }
}
