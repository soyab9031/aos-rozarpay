<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\EventCategory
 *
 * @property int $id
 * @property string $name
 * @property int $status 1: Active, 2: Inactive
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventCategory active()
 * @method static \Sofa\Eloquence\Builder|\App\Models\EventCategory newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\EventCategory newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\EventCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventCategory whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|EventCategory aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|EventCategory avg($column)
 * @method static \Sofa\Eloquence\Builder|EventCategory callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|EventCategory count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|EventCategory filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|EventCategory getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|EventCategory joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|EventCategory leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|EventCategory lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|EventCategory max($column)
 * @method static \Sofa\Eloquence\Builder|EventCategory min($column)
 * @method static \Sofa\Eloquence\Builder|EventCategory orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|EventCategory orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|EventCategory orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|EventCategory orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|EventCategory orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|EventCategory orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|EventCategory orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|EventCategory prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|EventCategory rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|EventCategory search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|EventCategory select($columns = [])
 * @method static \Sofa\Eloquence\Builder|EventCategory setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|EventCategory setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|EventCategory sum($column)
 * @method static \Sofa\Eloquence\Builder|EventCategory whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|EventCategory whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|EventCategory whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|EventCategory whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|EventCategory whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|EventCategory whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|EventCategory whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|EventCategory whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|EventCategory whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|EventCategory whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|EventCategory whereYear($column, $operator, $value, $boolean = 'and')
 */
class EventCategory extends Model
{
    use Eloquence;

    const ACTIVE = 1, INACTIVE = 2;

    protected $fillable = [
        'name', 'status'
    ];

    public function scopeActive($query)
    {
        return $query->where('status', self::ACTIVE);
    }
}
