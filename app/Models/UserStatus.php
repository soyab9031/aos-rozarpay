<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;
use Spatie\Activitylog\Models\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\UserStatus
 *
 * @property int $id
 * @property int $user_id
 * @property int $address 1: Pending, 2: Verified, 3:InProgress, 4:Rejected
 * @property int $bank_account 1: Pending, 2: Verified, 3:InProgress, 4:Rejected
 * @property int $pancard 1: Pending, 2: Verified, 3:InProgress, 4:Rejected
 * @property int $invoice 1: Pending, 2: Verified, 3:InProgress, 4:Rejected
 * @property int $aadhar_card 1: Pending, 2: Verified, 3:InProgress, 4:Rejected
 * @property int $fake 0: No, 1: Yes (Consider as Fake ID)
 * @property string|null $binary_qualified_at
 * @property string|null $sponsor_qualified_at
 * @property int $tree_calculation 0: No, 1: Yes
 * @property int $nested_set_calculated 0: No, 1: Yes
 * @property int $payment 0: Disallow, 1: Allow, (User Payment Status)
 * @property int $terminated 0: No, 1: Yes
 * @property int $dispatch 0: No, 1: Yes (Package Dispatch Status)
 * @property int $email 0: No, 1: Yes (Welcome Email)
 * @property int $sms 0: No, 1: Yes (Welcome SMS)
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Sofa\Eloquence\Builder|UserStatus aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|UserStatus avg($column)
 * @method static \Sofa\Eloquence\Builder|UserStatus callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|UserStatus count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|UserStatus filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|UserStatus getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|UserStatus joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|UserStatus leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|UserStatus lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|UserStatus max($column)
 * @method static \Sofa\Eloquence\Builder|UserStatus min($column)
 * @method static \Sofa\Eloquence\Builder|UserStatus newModelQuery()
 * @method static \Sofa\Eloquence\Builder|UserStatus newQuery()
 * @method static \Sofa\Eloquence\Builder|UserStatus orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|UserStatus orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|UserStatus orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|UserStatus orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|UserStatus orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|UserStatus orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|UserStatus orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|UserStatus pendingTreeCalculation()
 * @method static \Sofa\Eloquence\Builder|UserStatus prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|UserStatus query()
 * @method static \Sofa\Eloquence\Builder|UserStatus rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|UserStatus search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|UserStatus select($columns = [])
 * @method static \Sofa\Eloquence\Builder|UserStatus setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|UserStatus setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|UserStatus sum($column)
 * @method static \Sofa\Eloquence\Builder|UserStatus whereAadharCard($value)
 * @method static \Sofa\Eloquence\Builder|UserStatus whereAddress($value)
 * @method static \Sofa\Eloquence\Builder|UserStatus whereBankAccount($value)
 * @method static \Sofa\Eloquence\Builder|UserStatus whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|UserStatus whereBinaryQualifiedAt($value)
 * @method static \Sofa\Eloquence\Builder|UserStatus whereCreatedAt($value)
 * @method static \Sofa\Eloquence\Builder|UserStatus whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserStatus whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserStatus whereDispatch($value)
 * @method static \Sofa\Eloquence\Builder|UserStatus whereEmail($value)
 * @method static \Sofa\Eloquence\Builder|UserStatus whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|UserStatus whereFake($value)
 * @method static \Sofa\Eloquence\Builder|UserStatus whereId($value)
 * @method static \Sofa\Eloquence\Builder|UserStatus whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|UserStatus whereInvoice($value)
 * @method static \Sofa\Eloquence\Builder|UserStatus whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserStatus whereNestedSetCalculated($value)
 * @method static \Sofa\Eloquence\Builder|UserStatus whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserStatus whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserStatus whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserStatus whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|UserStatus wherePancard($value)
 * @method static \Sofa\Eloquence\Builder|UserStatus wherePayment($value)
 * @method static \Sofa\Eloquence\Builder|UserStatus whereSms($value)
 * @method static \Sofa\Eloquence\Builder|UserStatus whereSponsorQualifiedAt($value)
 * @method static \Sofa\Eloquence\Builder|UserStatus whereTerminated($value)
 * @method static \Sofa\Eloquence\Builder|UserStatus whereTreeCalculation($value)
 * @method static \Sofa\Eloquence\Builder|UserStatus whereUpdatedAt($value)
 * @method static \Sofa\Eloquence\Builder|UserStatus whereUserId($value)
 * @method static \Sofa\Eloquence\Builder|UserStatus whereYear($column, $operator, $value, $boolean = 'and')
 * @mixin \Eloquent
 */
class UserStatus extends Model
{
    use Eloquence;

    CONST TREE_CALC_NO = 0, TREE_CALC_YES = 1;
    CONST FAKE_TYPE = 1, REAL_TYPE = 0;
    CONST YES = 1, NO = 0, PENDING = 2;

    const KYC_PENDING = 1, KYC_VERIFIED = 2, KYC_INPROGRESS = 3, KYC_REJECTED = 4;

    protected $fillable = [
        'user_id',
        'address',
        'invoice',
        'address',
        'bank_account',
        'pancard',
        'aadhar_card',
        'payment',
        'fake',
        'binary_qualified_at',
        'sponsor_qualified_at',
        'tree_calculation',
        'nested_set_calculated',
        'dispatch',
        'terminated'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopePendingTreeCalculation($query)
    {
        return $query->where('tree_calculation', 0);
    }

    public static function  getKycStatuses()
    {
        return [
            self::KYC_PENDING => 'PENDING',
            self::KYC_VERIFIED => 'VERIFIED',
            self::KYC_INPROGRESS => 'IN PROGRESS',
            self::KYC_REJECTED => 'REJECTED',
        ];
    }
}
