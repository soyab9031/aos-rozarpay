<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Blog extends Model
{
    use Eloquence;
    const ACTIVE = 1, INACTIVE = 2;

    protected $fillable  = [
        'title','image','description','status'
    ];

    public function scopeActive($q){
        return $q->where(self::ACTIVE);
    }
}
