<?php

namespace App\Models\Binary;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Binary\BinaryCalculation
 *
 * @property int $id
 * @property int $user_id
 * @property int $left
 * @property int $right
 * @property float $total_left
 * @property float $total_right
 * @property float $current_left
 * @property float $current_right
 * @property float $forward_left
 * @property float $forward_right
 * @property int $status 0: Open, 1: Closed, 2: Rejected
 * @property string|null $remarks
 * @property string|null $closed_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation closed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation open()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereClosedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereCurrentLeft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereCurrentRight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereForwardLeft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereForwardRight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereLeft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereRight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereTotalLeft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereTotalRight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculation whereUserId($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|\App\Models\Binary\BinaryCalculation newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Binary\BinaryCalculation newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Binary\BinaryCalculation query()
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation avg($column)
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation max($column)
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation min($column)
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation select($columns = [])
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation sum($column)
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|BinaryCalculation whereYear($column, $operator, $value, $boolean = 'and')
 */
class BinaryCalculation extends Model
{
    use Eloquence;

    CONST OPEN = 0, CLOSED = 1, REJECTED = 2;

    protected $fillable = [
        'user_id',
        'left',
        'right',
        'total_left',
        'total_right',
        'current_left',
        'current_right',
        'forward_left',
        'forward_right',
        'status',
        'remarks',
        'closed_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeOpen($query)
    {
        return $query->where('status', 0);
    }

    public function scopeClosed($query)
    {
        return $query->where('status', 1);
    }
}
