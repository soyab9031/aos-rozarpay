<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class CompanyDocument extends Model
{
    use Eloquence;

    const ACTIVE = 1, INACTIVE = 2;
    const PDF = 1, IMAGE = 2;

    protected $fillable = [
        'admin_id','name','document','type','status'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function scopeActive($q){
        return $q->where('status',self::ACTIVE);
    }
}
