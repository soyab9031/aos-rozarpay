<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\PinPaymentDetail
 *
 * @property int $id
 * @property string|null $payment_mode
 * @property int $qty
 * @property string|null $bank_name
 * @property string $payment_at
 * @property string|null $remarks
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Sofa\Eloquence\Builder|\App\Models\PinPaymentDetail newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\PinPaymentDetail newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\PinPaymentDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinPaymentDetail whereBankName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinPaymentDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinPaymentDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinPaymentDetail wherePaymentAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinPaymentDetail wherePaymentMode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinPaymentDetail whereQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinPaymentDetail whereRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PinPaymentDetail whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail avg($column)
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail max($column)
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail min($column)
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail select($columns = [])
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail sum($column)
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|PinPaymentDetail whereYear($column, $operator, $value, $boolean = 'and')
 */
class PinPaymentDetail extends Model
{
    use Eloquence;

    protected $fillable = [
        'payment_mode', 'qty', 'bank_name', 'payment_at', 'remarks'
    ];
}
