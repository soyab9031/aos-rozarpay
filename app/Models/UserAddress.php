<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\UserAddress
 *
 * @property int $id
 * @property int $user_id
 * @property string $address
 * @property string|null $landmark
 * @property string $city
 * @property string|null $district
 * @property int|null $state_id
 * @property string|null $pincode
 * @property int $type 1: Default, 2: Another
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\State|null $state
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereDistrict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereLandmark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress wherePincode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereStateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereUserId($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserAddress newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserAddress newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserAddress query()
 * @method static \Sofa\Eloquence\Builder|UserAddress aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|UserAddress avg($column)
 * @method static \Sofa\Eloquence\Builder|UserAddress callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|UserAddress count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|UserAddress filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|UserAddress getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|UserAddress joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|UserAddress leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|UserAddress lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|UserAddress max($column)
 * @method static \Sofa\Eloquence\Builder|UserAddress min($column)
 * @method static \Sofa\Eloquence\Builder|UserAddress orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|UserAddress orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|UserAddress orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|UserAddress orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|UserAddress orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|UserAddress orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|UserAddress orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|UserAddress prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|UserAddress rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|UserAddress search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|UserAddress select($columns = [])
 * @method static \Sofa\Eloquence\Builder|UserAddress setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|UserAddress setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|UserAddress sum($column)
 * @method static \Sofa\Eloquence\Builder|UserAddress whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|UserAddress whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserAddress whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserAddress whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|UserAddress whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|UserAddress whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserAddress whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserAddress whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserAddress whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|UserAddress whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|UserAddress whereYear($column, $operator, $value, $boolean = 'and')
 */
class UserAddress extends Model
{
    use Eloquence;

    protected $fillable = [
        'user_id', 'customer_id', 'name', 'mobile', 'email', 'address', 'landmark', 'city', 'district', 'state_id', 'pincode', 'type'
    ];

    public function state()
    {
        return $this->belongsTo(State::class, 'state_id', 'id');
    }
}
