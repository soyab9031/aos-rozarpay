<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Category
 *
 * @property int $id
 * @property int|null $parent_id
 * @property string $name
 * @property string $slug
 * @property int $status 1: Active, 2: Inactive
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category[] $children
 * @property-read int|null $children_count
 * @property-read \App\Models\Category $parent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category active()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Category newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Category newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|Category aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|Category avg($column)
 * @method static \Sofa\Eloquence\Builder|Category callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|Category count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|Category filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|Category getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|Category joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|Category leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|Category lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|Category max($column)
 * @method static \Sofa\Eloquence\Builder|Category min($column)
 * @method static \Sofa\Eloquence\Builder|Category orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|Category orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|Category orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|Category orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|Category orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|Category orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|Category orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|Category prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|Category rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|Category search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|Category select($columns = [])
 * @method static \Sofa\Eloquence\Builder|Category setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|Category setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|Category sum($column)
 * @method static \Sofa\Eloquence\Builder|Category whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Category whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Category whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Category whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Category whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Category whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Category whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Category whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Category whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Category whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Category whereYear($column, $operator, $value, $boolean = 'and')
 */
class Category extends Model
{
    use Sluggable, Eloquence {
        Eloquence::replicate insteadof Sluggable;
    }

    const ACTIVE = 1, INACTIVE = 2;

    protected $fillable = [
        'name', 'parent_id', 'type', 'slug', 'image','description','status'
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function parent()
    {
        return $this->hasOne(Category::class, 'id', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::ACTIVE);
    }

    public static function getParents($category_id, &$categories)
    {
        $category = self::with(['parent'])->whereId($category_id)->first();

        if ($category->parent) {
            $categories[] = $category->parent;
            self::getParents($category->parent_id, $categories);
        }
        else {
            return $categories;
        }

    }
}
