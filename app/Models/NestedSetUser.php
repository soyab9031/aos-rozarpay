<?php

namespace App\Models;

use Baum\Node;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\NestedSetUser
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $parent_id
 * @property int|null $lft
 * @property int|null $rgt
 * @property int|null $depth
 * @property string|null $leg
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NestedSetUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NestedSetUser whereDepth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NestedSetUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NestedSetUser whereLeg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NestedSetUser whereLft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NestedSetUser whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NestedSetUser whereRgt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NestedSetUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NestedSetUser whereUserId($value)
 * @mixin \Eloquent
 * @property-read \Baum\Extensions\Eloquent\Collection|\App\Models\NestedSetUser[] $children
 * @property-read \App\Models\NestedSetUser|null $parent
 * @method static \Illuminate\Database\Eloquent\Builder|\Baum\Node limitDepth($limit)
 * @method static \Illuminate\Database\Eloquent\Builder|\Baum\Node withoutNode($node)
 * @method static \Illuminate\Database\Eloquent\Builder|\Baum\Node withoutRoot()
 * @method static \Illuminate\Database\Eloquent\Builder|\Baum\Node withoutSelf()
 * @property-read int|null $children_count
 * @method static \Sofa\Eloquence\Builder|\App\Models\NestedSetUser newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\NestedSetUser newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\NestedSetUser query()
 * @method static \Sofa\Eloquence\Builder|NestedSetUser aggregate($function, array $columns = [])
 * @method static \Baum\Extensions\Eloquent\Collection|static[] all($columns = ['*'])
 * @method static \Sofa\Eloquence\Builder|NestedSetUser avg($column)
 * @method static \Sofa\Eloquence\Builder|NestedSetUser callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|NestedSetUser count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|NestedSetUser filterDate($date_range, $column = 'created_at')
 * @method static \Baum\Extensions\Eloquent\Collection|static[] get($columns = ['*'])
 * @method static \Sofa\Eloquence\Builder|NestedSetUser getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|NestedSetUser joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|NestedSetUser leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|NestedSetUser lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|NestedSetUser max($column)
 * @method static \Sofa\Eloquence\Builder|NestedSetUser min($column)
 * @method static \Sofa\Eloquence\Builder|NestedSetUser orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|NestedSetUser orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|NestedSetUser orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|NestedSetUser orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|NestedSetUser orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|NestedSetUser orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|NestedSetUser orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|NestedSetUser prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|NestedSetUser rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|NestedSetUser search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|NestedSetUser select($columns = [])
 * @method static \Sofa\Eloquence\Builder|NestedSetUser setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|NestedSetUser setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|NestedSetUser sum($column)
 * @method static \Sofa\Eloquence\Builder|NestedSetUser whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|NestedSetUser whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|NestedSetUser whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|NestedSetUser whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|NestedSetUser whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|NestedSetUser whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|NestedSetUser whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|NestedSetUser whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|NestedSetUser whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|NestedSetUser whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|NestedSetUser whereYear($column, $operator, $value, $boolean = 'and')
 */
class NestedSetUser extends Node
{
    use Eloquence;

    protected $fillable = [
        'user_id', 'parent_id', 'lft', 'rgt', 'depth', 'leg', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function childrenCount($user_id)
    {
        $nested_user = self::whereUserId($user_id)->first();

        return $nested_user ? $nested_user->descendants()->count() : 0;
    }

    public function setData()
    {
        User::select(['id', 'parent_id', 'leg', 'created_at'])->whereHas('status', function ($q) {
            $q->where('nested_set_calculated', UserStatus::NO);
        })->get()->map(function ($user) {

            if ($user->parent_id == null) {

                $node = self::create([
                    'user_id' => $user->id
                ]);

                $node->makeRoot();

                UserStatus::whereUserId($node->user_id)->update([
                    'nested_set_calculated' => UserStatus::YES
                ]);
            }
            else
            {
                if ($parentNode = self::whereUserId($user->parent_id)->first()) {

                    $parentNode->children()->create([
                        'user_id' => $user->id, 'leg' => $user->leg
                    ]);

                    UserStatus::whereUserId($user->id)->update([
                        'nested_set_calculated' => UserStatus::YES
                    ]);

                }

            }

        });

    }
}
