<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Product
 *
 * @property int $id
 * @property int $admin_id
 * @property int $category_id
 * @property string $name
 * @property string $slug
 * @property string|null $description
 * @property int $status 1: Active, 2: Inactive
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Admin $admin
 * @property-read \App\Models\Category $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductPrice[] $prices
 * @property-read int|null $prices_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product active()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Product newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Product newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|Product aggregate($function, array $columns = [])
 * @method static \Sofa\Eloquence\Builder|Product avg($column)
 * @method static \Sofa\Eloquence\Builder|Product callParent($method, array $args)
 * @method static \Sofa\Eloquence\Builder|Product count($columns = '*')
 * @method static \Sofa\Eloquence\Builder|Product filterDate($date_range, $column = 'created_at')
 * @method static \Sofa\Eloquence\Builder|Product getLikeOperator()
 * @method static \Sofa\Eloquence\Builder|Product joinRelations($relations, $type = 'inner')
 * @method static \Sofa\Eloquence\Builder|Product leftJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|Product lists($column, $key = null)
 * @method static \Sofa\Eloquence\Builder|Product max($column)
 * @method static \Sofa\Eloquence\Builder|Product min($column)
 * @method static \Sofa\Eloquence\Builder|Product orWhereBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|Product orWhereIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|Product orWhereNotBetween($column, array $values)
 * @method static \Sofa\Eloquence\Builder|Product orWhereNotIn($column, $values)
 * @method static \Sofa\Eloquence\Builder|Product orWhereNotNull($column)
 * @method static \Sofa\Eloquence\Builder|Product orWhereNull($column)
 * @method static \Sofa\Eloquence\Builder|Product orderBy($column, $direction = 'asc')
 * @method static \Sofa\Eloquence\Builder|Product prefixColumnsForJoin()
 * @method static \Sofa\Eloquence\Builder|Product rightJoinRelations($relations)
 * @method static \Sofa\Eloquence\Builder|Product search($query, $columns = null, $fulltext = true, $threshold = null)
 * @method static \Sofa\Eloquence\Builder|Product select($columns = [])
 * @method static \Sofa\Eloquence\Builder|Product setJoinerFactory(\Sofa\Eloquence\Contracts\Relations\JoinerFactory $factory)
 * @method static \Sofa\Eloquence\Builder|Product setParserFactory(\Sofa\Eloquence\Contracts\Searchable\ParserFactory $factory)
 * @method static \Sofa\Eloquence\Builder|Product sum($column)
 * @method static \Sofa\Eloquence\Builder|Product whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Product whereDate($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Product whereDay($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Product whereExists(\Closure $callback, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Product whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Product whereMonth($column, $operator, $value, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Product whereNotBetween($column, array $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Product whereNotIn($column, $values, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Product whereNotNull($column, $boolean = 'and')
 * @method static \Sofa\Eloquence\Builder|Product whereNull($column, $boolean = 'and', $not = false)
 * @method static \Sofa\Eloquence\Builder|Product whereYear($column, $operator, $value, $boolean = 'and')
 */
class Product extends Model
{
    use Eloquence;
    const ACTIVE = 1, INACTIVE = 2;

    protected $fillable = [
        'admin_id', 'category_id', 'name', 'slug', 'image', 'description', 'status'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function prices()
    {
        return $this->hasMany(ProductPrice::class);
    }

    public function scopeActive($q)
    {
        return $q->where('status', self::ACTIVE);
    }

}
