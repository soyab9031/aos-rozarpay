<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Project Settings
    |--------------------------------------------------------------------------
    |
    | This option controls the default Project Information & Calculation that gets used
    | during the running Project / Website
    |
    */

    'url' => env('APP_URL'),
    'company' => env('COMPANY_NAME'),
    'brand' => env('BRAND_NAME'),
    'sms_sender' => 'DEMOCA',

    /*
    |--------------------------------------------------------------------------
    | Charges which are using in Payout / Wallet Section
    |--------------------------------------------------------------------------
    |
    */
    'admin_charge' => 5,
    'pan_card_charge' => 0,
    'service_charge' => 5,

];