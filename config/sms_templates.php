<?php

return [
    /*
   |--------------------------------------------------------------------------
   | SMS Templates
   |--------------------------------------------------------------------------
   |
   */
    'registration' => [
        'id' => 1207164146460939366,
        'message' => 'Welcome :first_name to AOS Lifestyle, Your Username- :tracking_id & Password- :password & Your Transaction Password- :wallet_password. Visit our website https://aoslifestyle.com'
    ],
    'forget_password' => [
        'id' => 1207164146357913453,
        'message' => 'Dear User, Your New password :password for Your Tracking Id :tracking_id at https://aoslifestyle.com . Kindly Change it after login'
    ]
];



