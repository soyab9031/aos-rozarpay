@extends('admin.template.layout')

@section('title', 'Update Admin Manager')

@section('content')
    @breadcrumb(Dashboard:admin-dashboard,Admin Manager:admin-manager-view,Update:active)

    <div class="container-fluid container-fixed-lg">

        <div class="card">
            <div class="card-body">
                <form action="" method="post" role="form" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-group-default">
                                <label>Manager Name</label>
                                <input type="text" name="name" class="form-control" value="{{ $admin->name }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Mobile</label>
                                <input type="text" name="mobile" class="form-control" value="{{ $admin->mobile }}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Status</label>
                                <select name="status" class="full-width" data-init-plugin="select2" data-disable-search="true">
                                    <option value="1" {{ $admin->status == 1 ? 'selected' : '' }}>Active</option>
                                    <option value="2" {{ $admin->status == 2 ? 'selected' : '' }}>Inactive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Email</label>
                                <input type="email" name="email" class="form-control" value="{{ $admin->email }}" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Set New Password</label>
                                <input type="password" name="password" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Role Type</label>
                                <select name="type" class="full-width" data-init-plugin="select2" data-disable-search="true">
                                    <option value="1" {{ $admin->type == 1 ? 'selected' : '' }}>Master</option>
                                    <option value="2" {{ $admin->type == 2 ? 'selected' : '' }}>Manager</option>
                                    <option value="3" {{ $admin->type == 3 ? 'selected' : '' }}>Employee</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 text-center"> <hr> </div>
                        <div class="form-group col-md-12">
                            <label>Permission on Pages</label>
                            <select name="routes[]" class="listbox" multiple="multiple">
                                @foreach ($routes as $route)
                                    <option value="{{ $route->key }}" {{ $route->selected ? 'selected' : '' }}>{{ $route->value }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-12 text-center">
                            <button class="btn btn-danger"> Update </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

@stop

@section('import-css')
    <link href="/plugins/bootstrap-duallistbox/bootstrap-duallistbox.css"  rel="stylesheet" type="text/css" />
@stop

@section('import-javascript')
    <script type="text/javascript" src="/plugins/bootstrap-duallistbox/jquery.bootstrap-duallistbox.js"></script>
@stop

@section('page-javascript')
    <script>
        $(function() {
            $('.listbox').bootstrapDualListbox({
                selectorMinimalHeight: 300
            });
        })
    </script>
@stop