@extends('admin.template.layout')

@section('title', 'News')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard, News:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-12 pull-right">
                            <a href="{{ route('admin-news-create') }}" class="btn btn-success btn-sm pull-right"> Create New </a>
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Created at</th>
                            <th>Admin</th>
                            <th>Title</th>
                            <th>Image</th>
                            <th>Message</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($items as $index => $item)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($items, $index) }}</td>
                                <td>{{ $item->created_at->format('M d, Y') }}</td>
                                <td>{{ $item->admin->name }}</td>
                                <td>{{ $item->title }}</td>
                                <td>{{ $item->message }}</td>
                                <td>
                                    @if ($item->status == 1)
                                        <span class="label label-theme"> Active</span>
                                    @else
                                        <span class="label label-danger"> Inactive</span>
                                    @endif
                                </td>
                                <td><a href="{{ env('NEWS_IMAGE_URL').$item->image }}" target="_blank" class="btn-sm btn-primary"> View Image </a></td>
                                <td>
                                    <a href="{{ route('admin-news-update', ['id' => $item->id]) }}" class="btn btn-theme btn-xs m-b-5">Update</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $items->links() }}
                </div>
            </div>
        </div>
    </div>
@stop