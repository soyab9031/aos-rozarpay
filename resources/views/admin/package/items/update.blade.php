@extends('admin.template.layout')

@section('title', 'Package Item Update')

@section('content')

    <div class="" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{ route('admin-dashboard') }}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('admin-package-view') }}">Packages</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('admin-package-items-view', ['id' => $package_item->package_id]) }}">Items</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a href="javascript:void(0)">Update</a>
                    </li>
                </ol>
            </div>
        </div>
    </div>

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <ul class="list-group">
                            <li class="list-group-item">
                                Package: <span class="pull-right">{{ $package_item->package->name }}</span>
                            </li>
                            <li class="list-group-item">
                                Package Amount: <span class="pull-right text-danger">Rs. {{ number_format($package_item->package->amount) }}</span>
                            </li>
                            <li class="list-group-item">
                                Package PV: <span class="pull-right text-danger">{{ $package_item->package->pv }}</span>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <h6>Update Item of {{ $package_item->package->name }}</h6>
                        <form action="" method="post">
                            {{ csrf_field() }}
                            <div class="form-group form-group-default">
                                <label>Item Name</label>
                                <input type="text" name="item_name" class="form-control" value="{{ $package_item->name }}" required>
                            </div>
                            <div class="form-group form-group-default">
                                <label>Item Amount</label>
                                <input type="number" min="0" name="item_amount" class="form-control" value="{{ $package_item->amount }}" required>
                            </div>
                            <div class="form-group form-group-default">
                                <label>Item Qty</label>
                                <input type="number" min="0" name="item_qty" class="form-control" value="{{ $package_item->qty }}" required>
                            </div>
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Gst Rate</label>
                                <select class="full-width" data-init-plugin="select2" name="gst_rate" data-disable-search="true" required>
                                    <option value="">Select</option>
                                    @foreach(\App\Models\PackageItem::getGstPercentage() as $percentage)
                                        <option value="{{ $percentage }}" {{ $package_item->gst->percentage == $percentage ? 'selected' : null }}>{{ $percentage }}%</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group form-group-default">
                                <label>HSN Code</label>
                                <input type="text" name="hsn_code" class="form-control" value="{{ $package_item->gst->code }}" required>
                            </div>
                            <small><a href="https://cleartax.in/s/gst-hsn-lookup" class="pull-right" target="_blank">Don't Know the Code? Click Here</a></small>
                            <div class="text-center">
                                <button class="btn btn-danger">Update Item</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop