@extends('admin.template.layout')

@section('title', 'Packages')

@section('content')
    @breadcrumb(Dashboard:admin-dashboard,Packages:active)

    <div class="container-fluid container-fixed-lg">
    <div class="card">
        <div class="card-body">
            <form action="" method="get">
                <div class="row">
                    <div class="col-md-12 pull-right">
                        <a href="{{ route('admin-package-create') }}" class="btn btn-success btn-sm pull-right"> Create New </a>
                    </div>
                </div>
            </form>
            <div class="table-responsive m-t-10">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th width="30%">Name</th>
                        <th>Amount</th>
                        <th>Capping</th>
                        <th>Sponsor Income</th>
                        <th>PV</th>
                        <th>Status</th>
                        <th>Created At</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($packages as $index => $package)
                        <tr>
                            <td>{{ $index+1 }}</td>
                            <td>{{ $package->name }}</td>
                            <td>{{ $package->amount }}</td>
                            <td>{{ $package->capping }}</td>
                            <td>{{ $package->sponsor_income }}</td>
                            <td>{{ $package->pv }}</td>
                            <td>
                                @if ($package->status == 1)
                                    <span class="label label-theme"> Active</span>
                                @else
                                    <span class="label label-danger"> In Active</span>
                                @endif
                            </td>
                            <td>{{ \Carbon\Carbon::parse($package->created_at)->format('d M Y') }}</td>
                            <td>
                                <a href="{{ route('admin-package-update', ['id' => $package->id ]) }}" class="btn btn-info btn-xs"> Edit</a>
                                <br>
                                <a href="{{ route('admin-package-items-view', ['id' => $package->id ]) }}" class="btn btn-theme btn-xs m-t-5">
                                    Items <i class="fa fa-arrow-right"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>

@stop