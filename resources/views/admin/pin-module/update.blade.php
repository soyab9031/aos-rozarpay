@extends('admin.template.layout')

@section('title', 'Update Pin')

@section('content')
    @breadcrumb(Dashboard:admin-dashboard,Pin:admin-pin-view,Update:active)
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6 offset-md-3 offset-lg-3">
                <div class="card">
                    <div class="card-body">
                        <form action="" method="post" role="form" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
                            {{ csrf_field() }}
                            <div class="form-group form-group-default">
                                <label>Pin Number</label>
                                <input type="text" name="name" class="form-control" value="{{ $pin->number }}" readonly>
                            </div>
                            <div class="form-group form-group-default">
                                <label>Package</label>
                                <input type="text" name="amount" class="form-control" value="{{ $pin->package->name.' (Rs.'.$pin->package->amount.')' }}" readonly>
                            </div>
                            <div class="form-group form-group-default">
                                <label>Transfer To</label>
                                <input type="text" name="amount" class="form-control" value="{{ $pin->user->detail->full_name.' ('.$pin->user->tracking_id.')' }}" readonly>
                            </div>
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Status</label>
                                <select name="status" class="full-width" data-init-plugin="select2">
                                    <option value="0" {{ $pin->status == 0 ? 'selected' : '' }}>Unused</option>
                                    <option value="2" {{ $pin->status == 2 ? 'selected' : '' }}>Block</option>
                                </select>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-primary btn-lg btn-rounded"> Update PIN </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop