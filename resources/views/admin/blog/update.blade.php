@extends('admin.template.layout')

@section('title', 'Blog')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Update Blog:active)
    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="post" role="form" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Title</label>
                                <input type="text" name="title" class="form-control" value="{{ $blog->title  }}" required>
                            </div>

                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Status</label>
                                <select name="status" class="full-width" data-init-plugin="select2">
                                    <option value="1" {{ $blog->status == 1 ? 'selected' : '' }}>Active</option>
                                    <option value="0" {{ $blog->status == 0 ? 'selected' : '' }}>In-Active</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Select Blog image</label>
                                <input type="file" name="image" id="image">
                                <p><br>1. Upload File Size Not More than 2 MB.</p>
                                <p>2. File Upload Before Getting Smaller Size From <a href="http://tinypng.com" target="_blank">Tinypng.com</a><br/></p>
                                <p class="text-danger">3. For Responsive Blog upload image size in 300*300 pixels</p>
                            </div>
                            @if($blog->image)
                                <div class="form-group">
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            <a href="{{ env('BLOG_IMAGE_URL') . $blog->image }}"
                                               target="_blank">
                                                <img src="{{ env('BLOG_IMAGE_URL') . $blog->image  }}" width="80px"></a>
                                            <a href="javascript:void(0)"
                                               onclick="deleteImage('{{ $blog->id }}')"
                                               title="Delete" class="btn btn-danger pull-right mt-4">
                                                <i class="fa fa-trash"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="form-group">
                                    <img src="{{ env('BLOG_IMAGE_URL').$blog->image }}" width="100%" alt="">
                                </div>
                            @endif
                            <div class="col-md-12 text-center">
                                <button class="btn btn-success btn-cons m-b-10"><i class="fa fa-cloud-upload"></i> <span class="bold">Upload</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-8 summernote-wrapper">
                            <label>Description</label>
                            <textarea class="summernote" name="description" cols="30" rows="10"
                                      placeholder="Enter Product Description"></textarea>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop


@section('import-javascript')
    <script src="/user-assets/plugins/jquery.filer/js/jquery.filer.min.js"></script>
    <script src="/plugins/summernote/summernote.min.js"></script>
@stop

@section('import-css')
    <link rel="stylesheet" href="/user-assets/plugins/jquery.filer/css/jquery.filer.css">
    <link href="/plugins/summernote/summernote.css" rel="stylesheet"/>
@stop

@section('page-javascript')
    <script>
        $('#image').filer({ limit: 1, maxSize: 2, extensions: ['jpg', 'jpeg', 'png'], changeInput: true, showThumbs: true });

        $('.summernote').summernote({
            height: 250,
            minHeight: null,
            maxHeight: null,
            focus: false,
            toolbar: [['style', ['bold', 'italic', 'underline', 'clear']], ['fontsize', ['fontsize']], ['para', ['ul', 'ol', 'paragraph']], ['color', ['color']]],
            onfocus: function (e) {
                $('body').addClass('overlay-disabled');
            },
            onblur: function (e) {
                $('body').removeClass('overlay-disabled');
            },

        });

        $('.inline-editor').summernote({
            airMode: true
        });
    </script>
    <script>
        function deleteImage(id){
            swal({
                title: "Are you sure?",
                text: 'Are you confirm to delete this Image',
                icon: "info",
                buttons: true,
                dangerMode: true
            }).then(function (isConfirm) {

                if (isConfirm) {
                    INGENIOUS.blockUI(true);
                    window.location = '{{ route("admin-blog-update",['id'=> $blog->id,'deleteImage' => 'Yes']) }}';
                }

            });
        }
    </script>
@stop