@extends('admin.template.layout')

@section('title', 'Blog')

@section('content')
    @breadcrumb(Dashboard:admin-dashboard,Blog:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control" value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            @refreshBtn()
                        </div>
                        <div class="col-md-2 pull-right">

                            <a href="{{ route('admin-blog-create') }}" class="btn btn-success btn-sm pull-right"> Create New </a>
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Created At</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($blogs as $index => $blog)
                            <tr>
                                <td>{{ $index+1 }}</td>
                                <td>{{ \Carbon\Carbon::parse($blog->created_at)->format('d M Y') }}</td>
                                <td>{{ $blog->title }}</td>
                                    <td>
                                        <button href="#" data-target="#blogDescription" data-toggle="modal"class="btn btn-info btn-xs"{!! !strip_tags($blog->description) ? 'disabled' : '' !!} >View Description</button>
                                        <div class="modal" id="blogDescription" role="dialog">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <p class="text-justify">{!! $blog->description !!}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                <td>
                                    @if ($blog->status == 1)
                                        <span class="label label-theme"> Active</span>
                                    @else
                                        <span class="label label-danger"> In Active</span>
                                    @endif
                                </td>
                                <td><a href="{{ env('BLOG_IMAGE_URL').$blog->image }}" target="_blank" class="btn-sm btn-primary"> View Image </a></td>
                                <td>
                                    <a href="{{ route('admin-blog-update', [ 'id' => $blog->id ]) }}" class="btn btn-info btn-xs"> Edit</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $blogs->appends(['search' => Request::get('search'), 'dateRange' => Request::get('dateRange')])->links() }}
                </div>
            </div>
        </div>
    </div>

@stop