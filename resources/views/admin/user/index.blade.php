@extends('admin.template.layout')

@section('title', 'Users')

@section('content')
    @breadcrumb(Dashboard:admin-dashboard,Users:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="input-group transparent">
                                        <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                                        <input type="text" placeholder="Search" name="search" class="form-control" value="{{ Request::get('search') }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                        <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                                    </div>
                                </div>
                                {{--<div class="col-md-4">--}}
                                    {{--<div class="input-group">--}}
                                        {{--<select name="package_id" class="full-width" data-init-plugin="select2">--}}
                                            {{--<option value="">Package Filter</option>--}}
                                            {{--@foreach($packages as $package)--}}
                                                {{--<option value="{{ $package->id }}" {{ Request::get('package_id') ==  $package->id ? 'selected' : '' }}>{{ $package->name }}</option>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            </div>
                            <div class="row mt-2">
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <select name="pan_card_status" class="full-width" data-init-plugin="select2" data-disable-search="true">
                                            <option value="">PAN Card Filter</option>
                                            @foreach(\App\Models\UserStatus::getKycStatuses() as $index => $pan_status)
                                                <option value="{{ $index }}" {{ Request::get('pan_card_status') ==  $index ? 'selected' : '' }}>PAN Card {{ $pan_status }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <select name="bank_status" class="full-width" data-init-plugin="select2" data-disable-search="true">
                                            <option value="">Bank KYC Filter</option>
                                            @foreach(\App\Models\UserStatus::getKycStatuses() as $index => $bank_status)
                                                <option value="{{ $index }}" {{ Request::get('bank_status') ==  $index ? 'selected' : '' }}>Bank {{ $bank_status }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <select name="user_status" class="full-width" data-init-plugin="select2" data-disable-search="true">
                                            <option value="">User Status Filter</option>
                                            <option value="active" {{ Request::get('user_status') ==  'active' ? 'selected' : '' }}>User Active</option>
                                            <option value="inactive" {{ Request::get('user_status') ==  'inactive' ? 'selected' : '' }}>User Inactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 text-right">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            <a href="{{ route('admin-user-view', ['download' => 'yes','status' => Request::get('status'),'search' => Request::get('search'), 'dateRange' => Request::get('dateRange'), 'package_id' =>Request::get('package_id'), 'user_status' => Request::get('user_status'), 'bank_status' => Request::get('bank_status'), 'pan_card_status' => Request::get('pan_card_status'), 'page' => Request::get('page')]) }}" class="btn btn-info btn-sm" title="Download">
                                <i class="fa fa-cloud-download"></i> <span class="bold">Download</span>
                            </a>
                            @refreshBtn()
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="table-responsive m-t-10">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Access</th>
                            <th>Date</th>
                            {{--<th>Package</th>--}}
                            <th>User</th>
                            <th>Sponsor</th>
                            <th>Direct Upline</th>
                            <th>PAN Card</th>
                            <th>Bank</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $index => $user)
                            <tr class="accordion-toggle">
                                <td>{{ \App\Library\Helper::tableIndex($users, $index) }}</td>
                                <td>
                                    <div class="btn-toolbar flex-wrap" role="toolbar">
                                        <div class="btn-group sm-m-t-10">
                                            <a href="{{ route('admin-user-account-access', ['id' => $user->id]) }}" target="_blank" class="btn btn-default" data-toggle="tooltip" title="View Account">
                                                <i class="fa fa-universal-access text-primary"></i>
                                            </a>
                                            <a href="{{ route('admin-business-binary-tree', ['tracking_id' => $user->tracking_id]) }}" target="_blank" data-toggle="tooltip" class="btn btn-default" title="View Tree">
                                                <i class="fa fa-sitemap text-danger"></i>
                                            </a>
                                            <a href="{{ route('admin-user-update', ['id' => $user->id]) }}"  class="btn btn-default" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil text-dark"></i></a>
                                            <a href="javascript:void(0)" data-toggle="collapse" data-parent="#UserDetails" data-target=".userDetails{{ $index+1 }}" class="btn btn-secondary" title="View Details">
                                                <i class="fa fa-eye text-dark"></i>
                                            </a>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <span class="text-primary"> Joining : {{ $user->created_at->format('M d, Y h:i A') }}</span><br>
                                    <span class="text-danger"> Activation : {{ $user->paid_at ? \Carbon\Carbon::parse($user->paid_at)->format('M d, Y h:i A') : 'N.A'}}</span>
                                </td>
                                {{--<td>--}}
                                    {{--@if($user->package)--}}
                                        {{--{{ $user->package->name }} <br> (Rs. {{ $user->joining_amount }})--}}
                                    {{--@else--}}
                                        {{--Not Available--}}
                                    {{--@endif--}}
                                {{--</td>--}}
                                <td class="text-primary">
                                    {{ $user->detail->full_name }} <br>
                                    ({{ $user->tracking_id }})
                                </td>
                                <td>
                                    {{ $user->sponsorBy ? $user->sponsorBy->detail->full_name : '' }} <br>
                                    {{ $user->sponsorBy ? '(' . $user->sponsorBy->tracking_id . ')' : '' }}
                                </td>
                                <td>
                                    {{ $user->parentBy ? $user->parentBy->detail->full_name : '' }} <br>
                                    {{ $user->parentBy ? '(' . $user->parentBy->tracking_id . ')' : '' }}
                                </td>
                                <td>
                                    @if($user->status->pancard == \App\Models\UserStatus::KYC_PENDING)
                                        <span class="badge badge-warning">PENDING</span>
                                    @elseif($user->status->pancard == \App\Models\UserStatus::KYC_INPROGRESS)
                                        <span class="badge badge-inverse">In Progress</span>
                                    @elseif($user->status->pancard == \App\Models\UserStatus::KYC_VERIFIED)
                                        <span class="badge badge-success">Verified </span>
                                    @else
                                        <span class="badge badge-danger">Rejected</span>
                                    @endif
                                </td>
                                <td>
                                    @if($user->status->bank_account == \App\Models\UserStatus::KYC_PENDING)
                                        <span class="badge badge-warning">PENDING</span>
                                    @elseif($user->status->bank_account == \App\Models\UserStatus::KYC_INPROGRESS)
                                        <span class="badge badge-inverse">In Progress</span>
                                    @elseif($user->status->bank_account == \App\Models\UserStatus::KYC_VERIFIED)
                                        <span class="badge badge-success">Verified </span>
                                    @else
                                        <span class="badge badge-danger">Rejected</span>
                                    @endif
                                </td>
                            </tr>
                            <tr class="user-row-details">
                                <td colspan="10" class="p-0">
                                    <div class="accordion-body collapse userDetails{{ $index+1 }}">
                                        <div class="p-2 table-responsive">
                                            <table class="table table-bordered ">
                                                <thead>
                                                <tr>
                                                    <th>Username</th>
                                                    <th>Password</th>
                                                    <th>eWallet Password</th>
                                                    <th>Leg</th>
                                                    <th>Mobile </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>{{ $user->username }}</td>
                                                    <td>{{ $user->password }}</td>
                                                    <td>{{ $user->wallet_password }}</td>
                                                    <td>{{ $user->leg }}</td>
                                                    <td>{{ $user->mobile }}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $users->appends(['search' => Request::get('search'), 'dateRange' => Request::get('dateRange'), 'package_id' => Request::get('package_id'), 'user_status' => Request::get('user_status'), 'bank_status' => Request::get('bank_status'), 'pan_card_status' => Request::get('pan_card_status') ])->links() }}
                </div>
            </div>
        </div>
    </div>
@stop