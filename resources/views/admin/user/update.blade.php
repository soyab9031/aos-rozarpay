@extends('admin.template.layout')

@section('title', 'Update User Details ' . $user->detail->full_name)

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Users:admin-user-view,Update:active)

    <div class="container-fluid container-fixed-lg">
        <form action="" method="post" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <h5>
                                Static & Not Changeable Values
                            </h5>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Pin: <span class="text-primary">{{ $user->pin ? $user->pin->number : 'N.A' }}</span>
                                </li>
                                <li class="list-group-item">
                                    Tracking Id: <span class="text-primary">{{ $user->tracking_id }}</span>
                                </li>
                                <li class="list-group-item">
                                    Package: <span class="text-primary">{{ $user->package ? $user->package->name : 'N.A' }}</span>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="card card-default">
                        <div class="card-header ">
                            <div class="card-title">Account Details
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group form-group-default">
                                <label>Username</label>
                                <input type="text" class="form-control" value="{{ $user->username }}" readonly>
                            </div>
                            <div class="form-group form-group-default">
                                <label> Password</label>
                                <input type="text" name="password" class="form-control" value="{{ $user->password }}">
                            </div>
                            <div class="form-group form-group-default">
                                <label>Wallet Password</label>
                                <input type="text" name="wallet_password" class="form-control" value="{{ $user->wallet_password }}">
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h5>Account Information</h5>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <a href="{{ route('admin-document-view', ['search' => $user->tracking_id]) }}" target="_blank">KYC DOCUMENTS <i class="fa fa-external-link-square"></i></a>
                                </li>
                                <li class="list-group-item">
                                    <a href="{{ route('admin-wallet-status', ['tracking_id' => $user->tracking_id]) }}" target="_blank">WALLET STATUS <i class="fa fa-external-link-square"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">Personal Details</div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group form-group-default form-group-default-select2">
                                        <label>Title</label>
                                        <select name="title" class="full-width" data-init-plugin="select2">
                                            <option value="Mr" {{ $user->detail->title == 'Mr' ? 'selected' : '' }}>Mr</option>
                                            <option value="Miss" {{ $user->detail->title == 'Miss' ? 'selected' : '' }}>Miss</option>
                                            <option value="Mrs" {{ $user->detail->title == 'Mrs' ? 'selected' : '' }}>Mrs</option>
                                            <option value="Dr." {{ $user->detail->title == 'Dr.' ? 'selected' : '' }}>Dr.</option>
                                            <option value="Sri" {{ $user->detail->title == 'Sri' ? 'selected' : '' }}>Sri</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group form-group-default">
                                        <label>First Name</label>
                                        <input type="text" name="first_name" class="form-control" value="{{ $user->detail->first_name }}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group form-group-default">
                                        <label>Last Name</label>
                                        <input type="text" name="last_name" class="form-control" value="{{ $user->detail->last_name }}" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <hr>
                                </div>
                                <div class="col-md-6 p-l-10">
                                    <label>Gender</label>
                                    <div class="radio radio-danger m-t-0">
                                        <input type="radio" value="1" name="gender" id="male" {{ $user->detail->gender == 1 ? 'checked' : null }}>
                                        <label for="male">Male</label>
                                        <input type="radio" value="2" name="gender" id="female" {{ $user->detail->gender == 2 ? 'checked' : null }}>
                                        <label for="female">Female</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label> Birth Date</label>
                                        <input type="text" name="birth_date" id="nomineeBirthDate" class="form-control" value="{{ $user->detail->birth_date->format('d-m-Y') }}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Email</label>
                                        <input type="text" name="email" class="form-control" value="{{ $user->email }}" autocomplete="off">
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Mobile</label>
                                        <input type="text" name="mobile" class="form-control" value="{{ $user->mobile }}" autocomplete="off">
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">Nominee Details</div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Nominee Name</label>
                                        <input type="text" name="nominee_name" class="form-control" value="{{ $user->detail->nominee_name }}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default form-group-default-select2">
                                        <label>Nominee Relations</label>
                                        <select name="nominee_relation" class="full-width" data-init-plugin="select2">
                                            <option value="" disabled selected> Select </option>
                                            @foreach($nominees as $nominee)
                                                <option value="{{ $nominee }}" {{ $nominee == $user->detail->nominee_relation ? 'selected' : '' }}>
                                                    {{ $nominee  }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Nominee Birth Date</label>
                                        @if($user->detail->nominee_birth_date)
                                            <input type="text" name="nominee_birth_date" id="nomineeBirthDate" class="form-control" value="{{ $user->detail->nominee_birth_date->format('d-m-Y') }}" readonly>
                                        @else
                                            <input type="text" name="nominee_birth_date" id="nomineeBirthDate" class="form-control" value="{{ old('nominee_birth_date') }}" readonly>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">Address Details</div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group form-group-default">
                                        <label>Address</label>
                                        <input type="text" name="address" class="form-control" value="{{ $user->user_default_address()->address }}" autocomplete="off">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Landmark</label>
                                        <input type="text" name="landmark" class="form-control" value="{{ $user->user_default_address()->landmark }}" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group form-group-default">
                                        <label>City</label>
                                        <input type="text" name="city" class="form-control" value="{{ $user->user_default_address()->city }}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-group-default">
                                        <label>District</label>
                                        <input type="text" name="district" class="form-control" value="{{ $user->user_default_address()->district }}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-group-default form-group-default-select2">
                                        <label>State</label>
                                        <select name="state_id" class="full-width" data-init-plugin="select2">
                                            @foreach ($states as $state)
                                                <option value="{{ $state->id }}" {{ $user->user_default_address()->state_id == $state->id ? 'selected' : null }}>{{ $state->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group form-group-default">
                                        <label>Pincode</label>
                                        <input type="text" name="pincode" class="form-control" value="{{ $user->user_default_address()->pincode }}" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">Bank Details</div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Bank Name</label>
                                        <input type="text" name="bank_name" class="form-control" value="{{ $user->bank->bank_name }}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Account Holder Name</label>
                                        <input type="text" name="bank_account_name" class="form-control" value="{{ $user->bank->account_name }}" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group form-group-default">
                                        <label>Account Number</label>
                                        <input type="text" name="bank_account_number" class="form-control" value="{{ $user->bank->account_number }}" autocomplete="off">
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>IFSC Code</label>
                                        <input type="text" name="bank_ifsc" class="form-control" value="{{ $user->bank->ifsc }}" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group form-group-default form-group-default-select2">
                                        <label>Account Type</label>
                                        <select name="bank_account_type" class="full-width" data-init-plugin="select2">
                                            <option value="1" {{ $user->bank->type == 1 ? 'selected' : '' }}>Saving</option>
                                            <option value="2" {{ $user->bank->type == 2 ? 'selected' : '' }}>Current</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-group-default">
                                        <label>Branch</label>
                                        <input type="text" name="bank_branch" class="form-control" value="{{ $user->bank->branch }}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-group-default">
                                        <label>PAN NUMBER</label>
                                        <input type="text" class="form-control" value="{{ $user->detail->pan_number }}" disabled autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 text-center mb-2">
                    <button type="submit" class="btn btn-danger btn-rounded">
                        Update Account
                    </button>
                </div>
            </div>

        </form>
    </div>
@stop

@section('page-javascript')
    <script>
        $('#birthDate, #nomineeBirthDate').datepicker({
            autoclose: true,
            format: "dd-mm-yyyy",
            startDate: "-99y",
            endDate: "-18y"
        });
    </script>
@stop
