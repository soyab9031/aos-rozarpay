@extends('admin.template.layout')

@section('title', 'Orders')

@section('content')
    @breadcrumb(Dashboard:admin-dashboard,Orders:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i
                                                class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control"
                                       value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                </div>
                                <input type="text" name="dateRange" class="form-control date-range"
                                       value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter"
                                       readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <select name="status" id="" class="full-width" data-init-plugin="select2">
                                <option value="" selected disabled="">Filter Status</option>
                                @foreach(\App\Models\Order::getStatus() as $index => $status)
                                    <option
                                            value="{{ $index }}" {{ Request::get('status') == $index ? 'selected' : null }}>{{ $status }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2">
                            <div class="col-md-12 text-right">
                                <button class="btn btn-danger btn-sm"> Search</button>
                                @refreshBtn()
                            </div>
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th width="10%"> Date</th>
                            <th>Order ID</th>
                            <th>User</th>
                            <th>Total BV</th>
                            <th>Total</th>
                            <th>Status</th>
                            <th>Details</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $index => $order)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($orders, $index) }}</td>
                                <td>
                                    Order At: {{ $order->created_at->format('M d, Y h:i A') }}
                                    @if($order->approved_at)
                                        <b class="d-block">
                                            Approved
                                            At: {{ \Carbon\Carbon::parse($order->approved_at)->format('M d, Y h:i A') }}
                                        </b>
                                    @endif
                                </td>
                                <td class="text-danger">
                                    {{ $order->customer_order_id }}
                                </td>
                                <td class="text-primary">

                                    {{ $order->user->detail->full_name }} <br>
                                    ({{ $order->user->tracking_id }})
                                </td>
                                <td class="text-danger">{{ $order->total_bv }} BV</td>
                                <td>{{ $order->total }}</td>
                                <td>
                                    @if($order->status == 2)
                                        <span class="label label-warning"> Placed </span>
                                    @elseif($order->status == 3)
                                        <span class="label label-success">Approved </span>
                                    @elseif($order->status == 5)
                                        <span class="label label-danger"> Rejected </span>
                                    @elseif($order->status == 6)
                                        <span class="label label-theme"> Cancel </span>
                                    @else
                                        <span
                                                class="label label-info"> {{ \App\Models\Order::getStatus($order->status) }} </span>
                                    @endif
                                </td>
                                <td>
                                    <a class="btn btn-xs btn-success"
                                       href="{{ route('admin-shopping-order-details', ['customer_order_id' => $order->customer_order_id]) }}">Details</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $orders->appends(['search' => Request::get('search'), 'dateRange' => Request::get('dateRange'), 'status' =>Request::get('status') ])->links() }}
                </div>
            </div>
        </div>
    </div>
@stop
