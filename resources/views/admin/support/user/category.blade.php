@extends('admin.template.layout')

@section('title')
    @if($category)
        Update User Support Categories
    @else
        User Support Categories
    @endif
@stop

@section('content')

    @if($category)
        @breadcrumb(Dashboard:admin-dashboard, Support:admin-user-support, Categories:admin-user-support-category, Update:active)
    @else
        @breadcrumb(Dashboard:admin-dashboard, Support:admin-user-support, Categories:active)
    @endif

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-5">
                <div class="card">
                    @if($category)
                        <div class="card-header">
                            <div class="card-title">Update Support Category</div>
                        </div>
                    @endif
                    <div class="card-body">
                        <form action="" method="post">
                            {{ csrf_field() }}
                            <div class="form-group form-group-default">
                                <label>Support Category</label>
                                <input type="text" class="form-control" name="name" value="{{ $category ? $category->name : old('name') }}" required autocomplete="off">
                            </div>
                            @if($category)
                                <div class="form-group form-group-default form-group-default-select2">
                                    <label>Status</label>
                                    <select name="status" class="full-width" data-init-plugin="select2">
                                        <option value="1" {{ $category->status == '1' ? 'selected' : null }}>Active</option>
                                        <option value="2" {{ $category->status == '2' ? 'selected' : null }}>Inactive</option>
                                    </select>
                                </div>
                            @endif
                            <div class="text-center">
                                @if($category)
                                    <button class="btn btn-primary btn-rounded"> Update </button>
                                @else
                                    <button class="btn btn-danger btn-rounded"> Create </button>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($categories as $index => $category)
                                    <tr>
                                        <td>{{ $index+1 }}</td>
                                        <td>{{ $category->name }}</td>
                                        <td>
                                            @if($category->status == 1)
                                                <span class="badge badge-success">Active</span>
                                            @else
                                                <span class="badge badge-danger">Inactive</span>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('admin-user-support-category', ['id' => $category->id]) }}" class="btn btn-xs btn-theme">Update</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop