@extends('admin.template.layout')

@section('title', 'Support ticket for ' . $support->category->name)

@section('content')

    @breadcrumb(Dashboard:admin-dashboard, User Support:admin-user-support, Chat:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-4">
                <section>
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="alert alert-danger">
                                    Ticket Number: <b>{{ $support->ticket_number }}</b>
                                </div>
                                <h5 class="mb-1">Ticket: {{ $support->category->name }}</h5>
                                <hr>
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        Name: <span
                                            class="pull-right">{{ \Str::limit($support->user->detail->full_name, 25) }}</span>
                                    </li>
                                    <li class="list-group-item">
                                        ID <span class="pull-right">{{ $support->user->tracking_id }}</span>
                                    </li>
                                    <li class="list-group-item">
                                        Mobile <span class="pull-right">{{ $support->user->mobile }}</span>
                                    </li>
                                    @if($support->status == \App\Models\Support::CLOSED)
                                        <li class="list-group-item text-danger">
                                            Status <span class="pull-right"> Ticket is Closed</span>
                                        </li>
                                    @endif
                                </ul>
                                @if($support->status == \App\Models\Support::OPEN)
                                    <hr>
                                    <a href="" class="btn btn-primary mb-1" title="Refresh">
                                        <i class="fa fa-refresh d-xl-none"></i>
                                        <span class="d-none d-lg-none d-xl-block">Refresh </span>
                                    </a>
                                    <a href="javascript:void(0)" class="btn btn-danger mb-1 closeThisTicket"
                                       title="close the ticket">
                                        <i class="fa fa-close d-xl-none"></i>
                                        <span class="d-none d-lg-none d-xl-block">Close the Ticket </span>
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-8">
                <section>
                    <div class="card chat-application">
                        <div class="card-content">
                            <div class="card-body" style="height: 100%;">
                                <section class="chat-app-window">
                                    <div class="mb-1 bg-complete text-dark text-bold-700" style="padding: 0.5rem">
                                        Support ticket for {{ $support->category->name }}
                                    </div>
                                    <div class="chats">
                                        @foreach($details as $detail)
                                            <div
                                                class="chat {{ $detail->type == \App\Models\Support::USER_TO_ADMIN ? 'chat-left' : null }}">
                                                <div class="chat-avatar">
                                                    <a class="thumbnail-wrapper d32 circular inline">
                                                        @if($detail->type == \App\Models\Support::ADMIN_TO_USER)
                                                            <img src="/user-assets/images/icons/support.svg"
                                                                 class="box-shadow-4">
                                                        @else
                                                            <img src="/user-assets/images/icons/user-tie.svg"
                                                                 class="box-shadow-4">
                                                        @endif
                                                    </a>
                                                </div>
                                                <div class="chat-body">
                                                    <div class="chat-content" style="padding: 0.5rem;">
                                                        @if($detail->image)
                                                            <a href="{{ $detail->image }}" target="_blank">
                                                                <img src="{{ $detail->image }}"
                                                                     style="width: 15rem; border-radius: 0.5rem;">
                                                            </a>
                                                        @endif
                                                        <span
                                                            class="font-small-1 {{ $detail->type == \App\Models\Support::ADMIN_TO_USER ? 'text-white-50' : 'text-black-50' }}">
                                                            {{ $detail->created_at->format('M d, Y H:i A') }}
                                                        </span>
                                                        <p class="text-left">{{ $detail->message }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        @if($support->status == \App\Models\Support::CLOSED)
                                            <div class="mb-1 bg-danger text-white text-bold-700"
                                                 style="padding: 0.5rem">
                                                Ticket has been closed
                                            </div>
                                        @endif
                                    </div>
                                </section>
                                <section class="chat-app-form bg-white">
                                    @if($support->status == \App\Models\Support::OPEN)
                                        <form class="chat-app-input d-flex" action="" method="post">
                                            {{ csrf_field() }}
                                            <fieldset class="form-group col-10 m-0 p-0">
                                                <input type="text" name="message" class="form-control"
                                                       placeholder="Type your message" autocomplete="off" required>
                                            </fieldset>
                                            <fieldset class="form-group has-icon-left col-2">
                                                <button type="submit" class="btn btn-danger">
                                                    <span class="d-none d-lg-none d-xl-block">Send Message </span>
                                                </button>
                                            </fieldset>
                                        </form>
                                    @endif
                                </section>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@stop

@section('import-css')
    <link rel="stylesheet" href="/user-assets/css/chat.css">
@stop

@section('page-javascript')
    <script>
        var height = 0;
        $('div.chat').each(function (i, value) {
            height += parseInt($(this).height());
        });

        height += '';

        $('section.chat-app-window').animate({scrollTop: height});

        $('.closeThisTicket').click(function () {

            swal({
                title: "Are you sure?",
                text: 'Are you confirm to close this Ticket?',
                icon: "info",
                buttons: true,
                dangerMode: true
            }).then(function (isConfirm) {

                if (isConfirm) {
                    window.location = '{{ route('admin-user-support-chat', ['id' => $support->id, 'close_ticket' => 'Yes']) }}';
                }

            });
        });

    </script>
@stop
