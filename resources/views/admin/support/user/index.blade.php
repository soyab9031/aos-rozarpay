@extends('admin.template.layout')

@section('title', 'User Support')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard, User Support:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control" value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-group-default-select2">
                                <select name="category_id" class="full-width" data-init-plugin="select2">
                                    <option value="" disabled selected>Select Category</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" {{ Request::get('category_id') == $category->id ? 'selected' : '' }}>
                                            {{ $category->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            @refreshBtn()
                        </div>
                        <div class="col-md-2 text-right">
                            <a href="{{ route('admin-user-support-category') }}" class="btn btn-primary btn-sm"> Category <i class="sl-star"></i> </a>
                        </div>
                    </div>
                </form>
                <div class="table table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Category</th>
                            <th>Admin</th>
                            <th>User</th>
                            <th>Message</th>
                            <th>Details</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($supports as $index => $support)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($supports, $index) }}</td>
                                <td>
                                    {{ $support->created_at->format('M d, Y') }}
                                    <span class="text-primary d-block">T: {{ $support->ticket_number }}</span>
                                </td>
                                <td>
                                    @if($support->status == \App\Models\Support::OPEN)
                                        <span class="label label-warning text-dark">Open</span>
                                    @else
                                        <span class="label label-danger">Closed</span>
                                    @endif
                                </td>
                                <td>{{ $support->category->name }}</td>
                                <td>{{ $support->admin ? $support->admin->name : '-' }}</td>
                                <td>
                                    {{ $support->user->detail->full_name }}
                                    <b class="d-block">{{ $support->user->tracking_id }}</b>
                                </td>
                                <td>{{ \Str::limit($support->message, 30) }}</td>
                                <td>
                                    <a href="{{ route('admin-user-support-chat', ['id' => $support->id]) }}" class="btn btn-sm btn-primary">
                                        Chat
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $supports->appends(['category_id' => Request::get('category_id'), 'search' => Request::get('search')])->links() }}
                </div>
            </div>
        </div>
    </div>

@stop
