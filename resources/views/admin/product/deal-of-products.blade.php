@extends('admin.template.layout')

@section('title', 'Create Deal Of The Day')

@section('content')
    @breadcrumb(Dashboard:admin-dashboard,Deal Of The Day:active)

    <div class="container-fluid container-fixed-lg">

        <div class="card">
            <div class="card-body">
                <form action="" method="post" role="form" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label>Deal Of The Day Product List</label>
                            <select name="product_price_ids[]" class="listbox" multiple="multiple">
                                @foreach ($product_prices as $product_price)
                                    <option value="{{ $product_price->id }}" {{ $product_price->deal == 1 ? 'selected' : '' }}>
                                        {{ $product_price->product->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-12 text-center">
                            <button class="btn btn-danger"> Update </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

@stop

@section('import-css')
    <link href="/plugins/bootstrap-duallistbox/bootstrap-duallistbox.css"  rel="stylesheet" type="text/css" />
@stop

@section('import-javascript')
    <script type="text/javascript" src="/plugins/bootstrap-duallistbox/jquery.bootstrap-duallistbox.js"></script>
@stop

@section('page-javascript')
    <script>
        $(function() {
            $('.listbox').bootstrapDualListbox({
                selectorMinimalHeight: 300
            });
        })
    </script>
@stop