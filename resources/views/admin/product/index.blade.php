@extends('admin.template.layout')

@section('title', 'Product List')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Product:admin-product-view,View:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control" value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            @refreshBtn()
                        </div>
                        <div class="col-md-2 pull-right">
                            <a href="{{ route('admin-product-create') }}" class="btn btn-success btn-sm pull-right"> Create New </a>
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th> Date </th>
                            <th> Admin Manager </th>
                            <th> Category </th>
                            <th> Name </th>
                            <th> Image </th>
                            <th> Points</th>
                            <th> Amount </th>
                            <th> Status </th>
                            <th> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($products as $index => $product)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($products, $index) }}</td>
                                <td>{{ $product->created_at->format('M d, Y') }} </td>
                                <td>{{ $product->admin->name }}</td>
                                <td>{{ $product->category->name }}</td>
                                <td>{{ $product->name }}</td>
                                <td>
                                    <a href="{{ $product->prices->pluck('primary_image')[0] }}" target="_blank" class="btn-xs btn btn-primary">
                                        View Image
                                    </a>
                                </td>
                                <td>{{ $product->prices->pluck('points')[0] }}</td>
                                <td>{{ $product->prices->pluck('price')[0] }}</td>
                                <td>
                                    @if ($product->status == 1 )
                                        <span class="label label-success">Active</span>
                                    @else
                                        <span class="label label-danger">Inactive</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('admin-product-update', ['id' => $product->id]) }}" class="btn btn-info btn-xs">
                                        Update
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $products->appends([
                    'search' => Request::get('search'), 'dateRange' => Request::get('dateRange')])->links() }}
                </div>
            </div>
        </div>
    </div>
@stop