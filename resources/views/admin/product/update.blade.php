@extends('admin.template.layout')

@section('title', 'Update Product: ' . $product->name)

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Product:admin-product-view,Update:active)

    <div class="container-fluid container-fixed-lg" id="updateProductPage">
        <form action="" method="post" role="form" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
            {{ csrf_field() }}
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Name</label>
                                <input type="text" name="name" class="form-control" value="{{ $product->name }}"
                                       required>
                            </div>
                            <div class="form-group form-group-default form-group-default-select2" v-if="main_parent_category_id">
                                <label>Main Parent Category</label>
                                <vue-select2 :options="main_categories" place-holder="Select Category"
                                             required="required" allow-search="0" @change-select2="getParentCategories"
                                             v-model="main_parent_category_id"></vue-select2>
                            </div>
                            <div class="form-group form-group-default form-group-default-select2"
                                 v-if="parent_categories.length > 0">
                                <label>Sub Category</label>
                                <vue-select2 :options="parent_categories" place-holder="Select Sub Category"
                                             allow-search="0" required="required" @change-select2="getProductCategories"
                                             v-model="parent_category_id"></vue-select2>
                            </div>
                            <div class="form-group form-group-default form-group-default-select2" v-if="categories.length > 0">
                                <label>Category</label>
                                <vue-select2 :options="categories" name="category_id" v-model="category_id"
                                             place-holder="Select Category" allow-search="0"
                                             required="required"></vue-select2>
                            </div>
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Status</label>
                                <select class="full-width" data-init-plugin="select2" name="status"
                                        data-disable-search="true" required>
                                    <option value="1" {{ $product->status == 1 ? 'selected' : null }}>Active</option>
                                    <option value="2" {{ $product->status == 2 ? 'selected' : null }}>Inactive</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-8 summernote-wrapper">
                            <label>Description</label>
                            <textarea class="summernote" name="description" cols="30" rows="10"
                                      placeholder="Enter Product Description">{{ $product->description }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    @foreach($product->prices as $index => $price)
                        <input type="hidden" name="prices[{{ $index }}][id]" value="{{ $price->id }}">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group form-group-default">
                                    <label>Product Unique Code</label>
                                    <input type="text" name="prices[{{ $index }}][code]" value="{{ $price->code }}"
                                           class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-group-default">
                                    <label>Product Barcode</label>
                                    <input type="text" name="prices[{{ $index }}][barcode]"
                                           value="{{ $price->barcode }}"
                                           class="form-control" onkeypress="INGENIOUS.numericInput(event)">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-group-default">
                                    <label>Points / PV / BV</label>
                                    <input type="text" name="prices[{{ $index }}][points]" value="{{ $price->points }}"
                                           class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group form-group-default">
                                    <label>Amount/MRP</label>
                                    <input type="text" name="prices[{{ $index }}][price]"
                                           value="{{ $price->price }}" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-group-default">
                                    <label>Distributor Price</label>
                                    <input type="text" name="prices[{{ $index }}][distributor_price]"
                                           value="{{ $price->distributor_price }}" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Product Image</label>
                                    <input type="file" name="prices[{{ $index }}][images]" id="image">
                                </div>
                                <div class="form-group">
                                    <ul class="list-group">
                                        @foreach($price->images as $image)
                                            <li class="list-group-item">
                                                <a href="{{ env('PRODUCT_IMAGE_URL') . $image }}"
                                                   target="_blank"><img
                                                        src="{{ env('PRODUCT_IMAGE_URL') . $image }}" width="80px"></a>
                                                <a href="javascript:void(0)"
                                                   @click="deleteImage('{{ $image }}', '{{ $price->id }}')"
                                                   title="Delete" class="btn btn-danger pull-right mt-4"><i
                                                        class="fa fa-trash"></i></a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-group-default form-group-default-select2">
                                    <label>Gst Rate</label>
                                    <select class="full-width" data-init-plugin="select2"
                                            name="prices[{{ $index }}][gst_rate]" data-disable-search="true"
                                            required>
                                        <option value="">Select</option>
                                        @foreach(\App\Models\ProductPrice::getGstPercentage() as $percentage)
                                            <option
                                                value="{{ $percentage }}" {{ $price->gst->percentage == $percentage ? 'selected' : null }}>
                                                {{ $percentage }}%
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-group-default">
                                    <label>HSN Code</label>
                                    <input type="text" name="prices[{{ $index }}][hsn_code]"
                                           value="{{ $price->gst->code }}" class="form-control" required>
                                </div>
                                <small><a href="https://cleartax.in/s/gst-hsn-lookup" class="pull-right"
                                          target="_blank">Don't Know the Code? Click Here</a></small>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="card">
                    <div class="card-body text-center">
                        <button class="btn btn-danger">Update</button>
                    </div>
                </div>
        </form>
    </div>
@stop

@section('import-javascript')
    <script src="/user-assets/plugins/jquery.filer/js/jquery.filer.min.js"></script>
    <script src="/plugins/summernote/summernote.min.js"></script>
@stop

@section('import-css')
    <link rel="stylesheet" href="/user-assets/plugins/jquery.filer/css/jquery.filer.css">
    <link href="/plugins/summernote/summernote.css" rel="stylesheet"/>
@stop

@section('page-javascript')
    <script>

        Vue.prototype.$http = axios;

        new Vue({
            el: '#updateProductPage',
            data: {
                main_parent_category_id: '',
                main_categories: {!! json_encode($categories) !!},
                parent_category_id: '',
                parent_categories: [],
                category_id: '{{ $product->category->id }}',
                categories: [],
                category_parents: {!! json_encode($category_parents) !!}, // On Load Setup
            },
            mounted: function () {

                setTimeout(() => {

                    this.main_parent_category_id = _.chain(this.category_parents).filter(category_parent => {
                        return category_parent.parent_id == null;
                    }).head().value().id;

                    this.parent_category_id = _.chain(this.category_parents).filter(category_parent => {
                        return category_parent.parent_id;
                    }).head().value().id;

                    this.getCategories(this.main_parent_category_id, 1);
                    this.getCategories(this.parent_category_id, 2);

                }, 500);

            },
            methods: {

                getParentCategories: function (main_parent_category_id) {

                    this.parent_category_id = '';
                    this.parent_categories = [];
                    this.category_id = '';
                    this.categories = [];
                    this.getCategories(main_parent_category_id, 1);
                },
                getProductCategories: function (parent_category_id) {

                    this.category_id = '';
                    this.categories = [];
                    this.getCategories(parent_category_id, 2);
                },
                getCategories: function (category_id, level) {

                    INGENIOUS.blockUI(true);

                    this.$http.get('{{ route("admin-api-category-get-children")}}', {
                        params: {
                            category_id: category_id,
                        }
                    }).then(response => {

                        INGENIOUS.blockUI(false);

                        if (response.data.status) {

                            if (level === 1) {
                                this.parent_categories = response.data.categories;
                            } else {
                                this.categories = response.data.categories;
                            }
                        } else {
                            swal('Error', response.data.message, 'error');
                        }

                    })
                },
                deleteImage: function ($image, product_price_id) {

                    swal({
                        title: "Are you sure?",
                        text: 'Are you confirm to Delete this Image ?',
                        icon: "info",
                        buttons: true,
                        dangerMode: true
                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            INGENIOUS.blockUI(true);
                            window.location = '{{ route('admin-product-update', ['id' => $product->id]) }}?deleteImage=' + $image + '&product_price_id=' + product_price_id;
                        }

                    });

                }
            }
        });

        $('#image').filer({
            limit: 3,
            maxSize: 2,
            extensions: ['jpg', 'jpeg', 'png'],
            changeInput: true,
            showThumbs: true
        });

        jQuery(document).ready(function () {

            $('.summernote').summernote({
                height: 250,
                minHeight: null,
                maxHeight: null,
                focus: false,
                toolbar: [['style', ['bold', 'italic', 'underline', 'clear']], ['fontsize', ['fontsize']], ['para', ['ul', 'ol', 'paragraph']], ['color', ['color']]],
                onfocus: function (e) {
                    $('body').addClass('overlay-disabled');
                },
                onblur: function (e) {
                    $('body').removeClass('overlay-disabled');
                },

            });

            $('.inline-editor').summernote({
                airMode: true
            });
        });
    </script>
@stop
