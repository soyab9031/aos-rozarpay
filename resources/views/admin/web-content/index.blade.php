@extends('admin.template.layout')

@section('title', 'Content Manager')

@section('content')
    @breadcrumb(Dashboard:admin-dashboard,Content Manager:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive m-t-10">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Created At</th>
                            <th>Admin</th>
                            <th>Title</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($web_contents as $index => $web_content)
                            <tr>
                                <td>{{ $index+1 }}</td>
                                <td>{{ \Carbon\Carbon::parse($web_content->created_at)->format('d M Y') }}</td>
                                <td>{{ $web_content->admin->name }}</td>
                                <td>{{ $web_content->title }}</td>
                                <td><a href="{{ env('WEB_CONTENT_IMAGE_URL').$web_content->image }}" target="_blank" class="btn-sm
                                 btn-primary"> View Image </a></td>
                                <td>
                                    <a href="{{ route('admin-web-content-update', [ 'id' => $web_content->id ]) }}" class="btn btn-info btn-xs"> Edit</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@stop