@extends('admin.template.layout')

@section('title', 'Update Content')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Content Manager:admin-web-content-view,Update Content:active)
    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="post" role="form" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-group-default">
                                <label>Title</label>
                                <input type="text" name="title" class="form-control" value="{{ $web_content->title }}" required>
                            </div>
                            <div class="summernote-wrapper">
                                <label>Description</label>
                                <textarea class="summernote" name="description" cols="30" rows="10"
                                          placeholder="Enter Product Description">{{ $web_content->description }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Select image</label>
                                <input type="file" name="image" id="image">
                                <p><br>1. Upload File Size Not More than 2 MB.</p>
                                <p>2. File Upload Before Getting Smaller Size From <a href="http://tinypng.com" target="_blank">Tinypng.com</a><br/></p>
                            </div>
                            <div class="col-md-12 text-center">
                                <button class="btn btn-success btn-cons m-b-10"><i class="fa fa-cloud-upload"></i> <span class="bold">Upload</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card card-default">
                                <div class="card-header  separator">
                                    <div class="card-title">Current Image</div>
                                </div>
                                <div class="card-body">
                                    <img src="{{ env('WEB_CONTENT_IMAGE_URL').$web_content->image }}" width="100%" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop


@section('import-javascript')
    <script src="/user-assets/plugins/jquery.filer/js/jquery.filer.min.js"></script>
    <script src="/plugins/summernote/summernote.min.js"></script>
@stop

@section('import-css')
    <link rel="stylesheet" href="/user-assets/plugins/jquery.filer/css/jquery.filer.css">
    <link href="/plugins/summernote/summernote.css" rel="stylesheet"/>
@stop

@section('page-javascript')
    <script>
        $('#image').filer({ limit: 1, maxSize: 2, extensions: ['jpg', 'jpeg', 'png'], changeInput: true, showThumbs: true });

        jQuery(document).ready(function () {

            $('.summernote').summernote({
                height: 250,
                minHeight: null,
                maxHeight: null,
                focus: false,
                toolbar: [['style', ['bold', 'italic', 'underline', 'clear']], ['fontsize', ['fontsize']], ['para', ['ul', 'ol', 'paragraph']], ['color', ['color']]],
                onfocus: function (e) {
                    $('body').addClass('overlay-disabled');
                },
                onblur: function (e) {
                    $('body').removeClass('overlay-disabled');
                },

            });

            $('.inline-editor').summernote({
                airMode: true
            });
        });
    </script>
@stop