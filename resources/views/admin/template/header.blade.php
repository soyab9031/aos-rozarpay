<div class="header">
    <a href="#" class="btn-link toggle-sidebar d-lg-none pg pg-menu" data-toggle="sidebar"></a>
    <div class="">
        <div class="brand inline">
            <img src="/admin-assets/images/theme/logo.png" alt="logo" data-src="/admin-assets/images/theme/logo.png"
                 width="78">
        </div>
        <a href="{{ config('project.url') }}" target="_blank"
           class="btn btn-link text-primary m-l-20 d-none d-lg-inline-block d-xl-inline-block">View Website</a>
        <a href="#" class="search-link d-none d-lg-inline-block d-xl-inline-block" data-toggle="search"><i
                    class="pg-search"></i>Type anywhere to <span class="bold">search</span></a>
    </div>
    <div class="d-flex align-items-center">
        <div class="pull-left p-r-10 fs-14 font-heading d-lg-inline-block d-none m-l-20">
            <span class="semi-bold hidden-xs hidden-sm text-danger">Last Login: {{ Session::get('admin')->last_logged_in_at ? \Carbon\Carbon::parse(Session::get('admin')->last_logged_in_at)->format('M d, Y h:i A') : 'N.A' }}</span>
            <span class="semi-bold"> | {{ Session::get('admin')->name }}</span>
        </div>
        <div class="dropdown pull-right d-lg-inline-block">
            <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                <span class="thumbnail-wrapper d32 circular inline">
                    <img src="/admin-assets/images/profiles/1.jpg" alt="" data-src="/admin-assets/images/profiles/1.jpg"
                         data-src-retina="/admin-assets/images/profiles/1.jpg" width="32" height="32">
                </span>
            </button>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                <a href="{{ route('admin-manager-profile') }}" class="dropdown-item"><i class="fa fa-user"></i> Profile</a>
                <a href="{{ route('admin-manager-view') }}" class="dropdown-item"><i class="pg-outdent"></i> Admin
                    Manager</a>
                <a href="{{ route('admin-logout') }}" class="clearfix bg-master-lighter dropdown-item">
                    <span class="pull-left">Logout</span>
                    <span class="pull-right"><i class="pg-power"></i></span>
                </a>
            </div>
        </div>

    </div>
</div>