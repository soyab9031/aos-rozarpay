<nav class="page-sidebar" data-pages="sidebar">

    <div class="sidebar-header">
        <img src="/admin-assets/images/theme/logo.png" alt="logo" class="brand" data-src="/admin-assets/images/theme/logo.png" width="150">
    </div>

    <div class="sidebar-menu">

        <ul class="menu-items">
            <li class="m-t-30">
                <a href="{{ route('admin-dashboard') }}" class="detailed">
                    <span class="title">Dashboard</span>
                    <span class="details">{{ env('APP_VERSION') }} Version</span>
                </a>
                <span class="icon-thumbnail"><i data-feather="shield"></i></span>
            </li>
            <li>
                <a href="javascript:void(0)"><span class="title">Users</span>
                    <span class=" arrow"></span></a>
                <span class="icon-thumbnail"><i data-feather="users"></i></span>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin-user-view') }}">View</a>
                        <span class="icon-thumbnail">V</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-user-blocked-view') }}">Blocked</a>
                        <span class="icon-thumbnail">B</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-user-payment-stop-view') }}">Payment Stop</a>
                        <span class="icon-thumbnail">PS</span>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{ route('admin-customer-view') }}"><span class="title">Customers</span></a>
                <span class="icon-thumbnail"><i data-feather="user"></i></span>
            </li>
            <li>
                <a href="javascript:void(0)"><span class="title">Business</span>
                    <span class=" arrow"></span></a>
                <span class="icon-thumbnail"><i data-feather="box"></i></span>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin-business-sponsors-view') }}">Sponsors</a>
                        <span class="icon-thumbnail">S</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-business-binary-tree') }}">Binary Tree</a>
                        <span class="icon-thumbnail">T</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-business-binary-status') }}">Binary Status</a>
                        <span class="icon-thumbnail">BS</span>
                    </li>
                </ul>
            </li>
            {{--<li class="">--}}
            {{--<a href="javascript:;"><span class="title">Packages</span>--}}
            {{--<span class=" arrow"></span></a>--}}
            {{--<span class="icon-thumbnail"><i data-feather="grid"></i></span>--}}
            {{--<ul class="sub-menu">--}}
            {{--<li>--}}
            {{--<a href="{{ route('admin-package-create') }}">Create</a>--}}
            {{--<span class="icon-thumbnail">C</span>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a href="{{ route('admin-package-view') }}">View</a>--}}
            {{--<span class="icon-thumbnail">V</span>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="">--}}
            {{--<a href="javascript:;"><span class="title">Pin Module</span>--}}
            {{--<span class=" arrow"></span></a>--}}
            {{--<span class="icon-thumbnail"><i data-feather="settings"></i></span>--}}
            {{--<ul class="sub-menu">--}}
            {{--<li>--}}
            {{--<a href="{{ route('admin-pin-view') }}">View</a>--}}
            {{--<span class="icon-thumbnail">V</span>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a href="{{ route('admin-pin-create') }}">Generate</a>--}}
            {{--<span class="icon-thumbnail">G</span>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a href="{{ route('admin-pin-request-view') }}">User's Request</a>--}}
            {{--<span class="icon-thumbnail">R</span>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            <li class="">
                <a href="javascript:;"><span class="title">Reports</span>
                    <span class=" arrow"></span></a>
                <span class="icon-thumbnail"><i data-feather="trending-up"></i></span>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin-report-sales') }}">Sales</a>
                        <span class="icon-thumbnail">S</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-report-top-earners') }}">Top Earners</a>
                        <span class="icon-thumbnail">TE</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-report-package-ranking') }}">Package Ranking</a>
                        <span class="icon-thumbnail">PR</span>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)" class="text-danger">
                    <span class="title">Inventory Master</span>
                    <span class="arrow"></span>
                </a>
                <span class="icon-thumbnail"><i class="text-danger fa fa-calendar-plus-o"></i></span>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin-store-manager-supplier-view') }}">Suppliers</a>
                        <span class="icon-thumbnail">S</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-store-manager-purchase-order-view') }}">Purchase Order</a>
                        <span class="icon-thumbnail">PO</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-store-manager-stock-purchase-view') }}">Stock Purchase</a>
                        <span class="icon-thumbnail">SP</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-store-manager-stock-list') }}">Company Stock</a>
                        <span class="icon-thumbnail">CS</span>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)" class="text-primary">
                    <span class="title">Outlet Master</span>
                    <span class="arrow"></span>
                </a>
                <span class="icon-thumbnail"><i class="text-primary" data-feather="triangle"></i></span>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin-store-manager-store-view') }}">Outlets</a>
                        <span class="icon-thumbnail">O</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-store-manager-store-register-request') }}">New Outlet Request</a>
                        <span class="icon-thumbnail">NOR</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-store-manager-document-view') }}">Kyc Request</a>
                        <span class="icon-thumbnail">KYC</span>
                    </li>
                    <li>
                        <a href="javascript:;"><span class="title">Wallet</span><span class="arrow"></span></a>
                        <span class="icon-thumbnail">W</span>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{ route('admin-store-manager-store-wallet-view') }}">View</a>
                                <span class="icon-thumbnail">WV</span>
                            </li>
                            <li>
                                <a href="{{ route('admin-store-manager-wallet-request-view') }}">Request</a>
                                <span class="icon-thumbnail">WR</span>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('admin-store-manager-store-request-view') }}">Stock Request</a>
                        <span class="icon-thumbnail">SR</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-store-manager-store-supply-view') }}">Supply</a>
                        <span class="icon-thumbnail">SL</span>
                    </li>
                    <li>
                        <a href="javascript:;"><span class="title">Reports</span><span class="arrow"></span></a>
                        <span class="icon-thumbnail">R</span>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{ route('admin-store-manager-report-product-ranking') }}">Product Ranking</a>
                                <span class="icon-thumbnail">PR</span>
                            </li>
                            <li>
                                <a href="{{ route('admin-store-manager-report-store-sales') }}">Outlet Sales</a>
                                <span class="icon-thumbnail">OS</span>
                            </li>
                            <li>
                                <a href="{{ route('admin-store-manager-report-supply-transfer') }}">Supply Transfer</a>
                                <span class="icon-thumbnail">ST</span>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)"><span class="title">Wallet</span>
                    <span class=" arrow"></span></a>
                <span class="icon-thumbnail"><i data-feather="credit-card"></i></span>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin-wallet-status') }}">Status</a>
                        <span class="icon-thumbnail">WS</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-wallet-report') }}">Total Report</a>
                        <span class="icon-thumbnail">WR</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-wallet-transaction') }}">Transaction</a>
                        <span class="icon-thumbnail">T</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-wallet-balance-report') }}">Balance Report</a>
                        <span class="icon-thumbnail">B</span>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)"><span class="title">Payout</span>
                    <span class=" arrow"></span></a>
                <span class="icon-thumbnail"><i data-feather="briefcase"></i></span>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin-payout-create') }}">Process</a>
                        <span class="icon-thumbnail">P</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-payout-view') }}">Report</a>
                        <span class="icon-thumbnail">R</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-payout-tds-report') }}">TDS Report</a>
                        <span class="icon-thumbnail">TR</span>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)"><span class="title">Orders</span>
                    <span class=" arrow"></span></a>
                <span class="icon-thumbnail"><i data-feather="grid"></i></span>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin-shopping-order-view') }}">View All</a>
                        <span class="icon-thumbnail">A</span>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)"><span class="title">Products</span>
                    <span class=" arrow"></span></a>
                <span class="icon-thumbnail"><i data-feather="grid"></i></span>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin-product-view') }}">View</a>
                        <span class="icon-thumbnail">V</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-new-arrival-list') }}">New Arrival List</a>
                        <span class="icon-thumbnail">NA</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-deal-of-products-list') }}">Deal Of Products List</a>
                        <span class="icon-thumbnail">NA</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-recommended-list') }}">Recommended List</a>
                        <span class="icon-thumbnail">NA</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-category-view') }}">Category</a>
                        <span class="icon-thumbnail">C</span>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)"><span class="title">Support</span>
                    <span class=" arrow"></span></a>
                <span class="icon-thumbnail"><i data-feather="life-buoy"></i></span>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin-website-support') }}">Website</a>
                        <span class="icon-thumbnail">W</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-website-grievance') }}">Grievance</a>
                        <span class="icon-thumbnail">G</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-user-support') }}">User</a>
                        <span class="icon-thumbnail">A</span>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{ route('admin-document-view') }}"><span class="title">Documents</span></a>
                <span class="icon-thumbnail"><i class="fa fa-id-card-o"></i></span>
            </li>
            <li>
                <a href="javascript:void(0)"><span class="title">Image Manager</span>
                    <span class=" arrow"></span></a>
                <span class="icon-thumbnail"><i data-feather="image"></i></span>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin-banner-view') }}">Primary Banner</a>
                        <span class="icon-thumbnail">PM</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-gallery-view') }}">Gallery</a>
                        <span class="icon-thumbnail">G</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-popup-view') }}">Popup</a>
                        <span class="icon-thumbnail">P</span>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)"><span class="title">Blogs</span>
                    <span class="arrow"></span></a>
                <span class="icon-thumbnail"><i data-feather="life-buoy"></i></span>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin-blog-view') }}">View All</a>
                        <span class="icon-thumbnail">V</span>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{ route('admin-sms-report') }}"><span class="title">SMS</span></a>
                <span class="icon-thumbnail"><i data-feather="message-circle"></i></span>
            </li>
            <li>
                <a href="javascript:void(0)"><span class="title">Utilities</span>
                    <span class="arrow"></span></a>
                <span class="icon-thumbnail"><i data-feather="server"></i></span>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin-web-content-view') }}">Website Content</a>
                        <span class="icon-thumbnail">WC</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-news') }}">News</a>
                        <span class="icon-thumbnail">N</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-setting-faq-view') }}">FAQs</a>
                        <span class="icon-thumbnail">F</span>
                    </li>
                    <li>
                        <a href="{{ route('admin-events-view') }}">Events</a>
                        <span class="icon-thumbnail">E</span>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)"><span class="title">Settings</span>
                    <span class="arrow"></span></a>
                <span class="icon-thumbnail"><i data-feather="settings"></i></span>
                <ul class="sub-menu">
                    @foreach(\App\Models\Setting::select('name')->get() as $setting)
                        <li>
                            <a href="{{ route('admin-system-settings', ['name' => $setting->name]) }}"><span class="title">{{ $setting->module_name }}</span></a>
                            <span class="icon-thumbnail">{{ substr($setting->module_name, 0, 1) }}</span>
                        </li>
                    @endforeach
                </ul>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
</nav>