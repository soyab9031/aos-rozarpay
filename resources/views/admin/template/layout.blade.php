<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>@yield('title') - {{ config('project.company') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="/admin-assets/images/theme/favicon.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/admin-assets/images/theme/favicon.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/admin-assets/images/theme/favicon.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/admin-assets/images/theme/favicon.png">
    <link rel="icon" type="image/x-icon" href="/admin-assets/images/theme/favicon.png" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <link href="/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="/admin-assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="/plugins/simple-line-icons/simple-line-icons.css" rel="stylesheet" type="text/css" />
    <link href="/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
    <link href="/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" media="screen">
    <link href="/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/admin-assets/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/switch.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="/admin-assets/css/light.css?v=1" rel="stylesheet" type="text/css" />
    @yield('import-css')
    @yield('page-css')
</head>
<body class="fixed-header menu-pin">
@if(env('APP_ENV') == 'local')
    <img style="position: fixed;z-index: 99999;width: 150px;margin: auto;top: -2px;left: 0;right: 0;opacity: 0.75;" src="/user-assets/images/ribbon.png" alt="TYMK Software Dev Mode">
@endif
@include('admin.template.left-sidebar')
<div class="page-container">
    @include('admin.template.header')
    <div class="page-content-wrapper ">
        <div class="content">
            @yield('content')
        </div>
        @include('admin.template.footer')
    </div>
</div>

<div class="overlay hide" data-pages="search">
    <div class="overlay-content has-results m-t-20">
        <div class="container-fluid">
            <img class="overlay-brand" src="/admin-assets/images/theme/logo.png" alt="logo" data-src="/admin-assets/images/theme/logo.png" width="100">
            <a href="#" class="close-icon-light overlay-close text-black fs-16">
                <i class="pg-close"></i>
            </a>
        </div>

        <div class="container-fluid">
            <input id="overlay-search" class="no-border overlay-search bg-transparent" placeholder="enter tracking id" autocomplete="off" spellcheck="false">
            <br>
            <div class="inline-block m-l-10">
                <p class="fs-13">Press enter to search</p>
            </div>
        </div>
    </div>

</div>

<script src="/plugins/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="/plugins/feather-icons/feather.min.js" type="text/javascript"></script>
<script src="/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="/plugins/modernizr.custom.js" type="text/javascript"></script>
<script src="/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="/plugins/popper/umd/popper.min.js" type="text/javascript"></script>
<script src="/admin-assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
<script src="/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
<script src="/plugins/jquery-actual/jquery.actual.min.js"></script>
<script src="/admin-assets/js/jquery.blockUI.js"></script>
<script src="/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="/plugins/select2/js/select2.full.min.js"></script>
<script type="text/javascript" src="/plugins/classie/classie.js"></script>
<script src="/plugins/moment/moment.js" type="text/javascript"></script>
<script src="/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="/user-assets/js/lodash.js"></script>
<script src="/user-assets/plugins/axios/axios.min.js"></script>
<script src="/user-assets/plugins/vue/{{ env('APP_ENV') == 'local' ? 'vue.js' : 'vue.min.js' }}"></script>
<script src="/user-assets/plugins/vue/vue-helper.js"></script>
<script src="/plugins/sweetalert/sweetalert.min.js"></script>
@yield('import-javascript')
@yield('page-javascript')
<script src="/admin-assets/js/pages.js"></script>
<script src="/admin-assets/js/scripts.js?v=oct2019" type="text/javascript"></script>
<script>
    @if(session('errors'))
        INGENIOUS.showNotification('{{ session('errors')->first() }}', 'danger');
    @elseif(session('error'))
        INGENIOUS.showNotification('{{ session('error') }}', 'danger');
    @elseif(session('success'))
        INGENIOUS.showNotification('{{ session('success') }}', 'success', 2500);
    @endif
</script>
</body>
</html>
