@extends('admin.template.layout')

@section('title', 'Popup List')

@section('content')
    @breadcrumb(Dashboard:admin-dashboard,Popup List:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-12 pull-right">
                            <a href="{{ route('admin-popup-create') }}" class="btn btn-success btn-sm pull-right"> Create New </a>
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Created At</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Image</th>
                            <th>Priority</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $cnt=1; @endphp
                        @foreach ($popups as $index => $popup)
                            <tr>
                                <td>{{ $index+1 }}</td>
                                <td>{{ \Carbon\Carbon::parse($popup->created_at)->format('d M Y') }}</td>
                                <td>{{ $popup->name }}</td>
                                <td>
                                    @if ($popup->status == 1)
                                        <span class="label label-theme"> Active</span>
                                    @else
                                        <span class="label label-danger"> In Active</span>
                                    @endif
                                </td>
                                <td><a href="{{ env('POPUP_IMAGE_PATH').$popup->image }}" target="_blank" class="btn-sm btn-primary"> View Image </a></td>
                                <td> <label class="badge badge-info">{{$cnt++}}</label> </td>
                                <td>
                                    <a href="{{ route('admin-popup-update', [ 'id' => $popup->id ]) }}" class="btn btn-info btn-xs"> Edit</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@stop