@extends('admin.template.layout')

@section('title', 'Create New Offer')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Offer:admin-store-manager-offer-view,Create:active)

    <div class="container-fluid container-fixed-lg" id="offerModule">
        <form action="" method="post" onsubmit="INGENIOUS.blockUI(true)">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group form-group-default">
                                <label>Name</label>
                                <input type="text" name="name" placeholder="Enter Offer Name" class="form-control" value="{{ old('name') }}" required>
                            </div>
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Offer Type</label>
                                <vue-select2 :options="offer_types" name="type" place-holder="Select Offer Type" allow-search="0" v-model="selected_offer_type"></vue-select2>
                            </div>
                            <hr>
                            <div class="form-group form-group-default">
                                <label>Start Date</label>
                                <vue-datepicker name="start_date" placeholder="Start Date"></vue-datepicker>
                            </div>
                            <div class="form-group form-group-default">
                                <label>End Date</label>
                                <vue-datepicker name="end_date" placeholder="End Date"></vue-datepicker>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card" v-if="selected_offer_type == 1">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Min. Billing Amount</label>
                                        <input type="text" name="min_billing_amount" onkeypress="INGENIOUS.numericInput(event)" placeholder="Enter Min. Billing Amount" class="form-control" value="0">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Max. Billing Amount</label>
                                        <input type="text" name="max_billing_amount" onkeypress="INGENIOUS.numericInput(event)" placeholder="Enter Max. Billing Amount" class="form-control" value="0">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row" v-for="(offer_item, index) of offer_items">
                                <div class="col-md-2">
                                    <div class="form-group form-group-default">
                                        <label>Group</label>
                                        <input type="text" class="form-control" :name="'offer_items['+index+'][group_id]'" :value="offer_item.group_id" readonly>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group form-group-default form-group-default-select2">
                                        <label>Free Item</label>
                                        <vue-select2 :options="search_items" :name="'offer_items['+index+'][product_price_id]'" v-model="offer_item.product_price_id" place-holder="Select Item" allow-search="0"></vue-select2>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-group-default">
                                        <label>Qty</label>
                                        <input type="text" :name="'offer_items['+index+'][qty]'" onkeypress="INGENIOUS.numericInput(event, false)" placeholder="Enter Qty" class="form-control" v-model="offer_item.qty">
                                    </div>
                                </div>
                                <div class="col-md-2 mt-2">
                                    <button type="button" @click="addOfferItem(offer_item.group_id)" class="btn btn-primary"><i class="fa fa-plus"></i></button>
                                    <button type="button" @click="removeOfferItem(offer_item)" class="btn btn-danger"><i class="fa fa-close"></i></button>
                                </div>
                            </div>
                            <div class="text-center mb-2">
                                <button type="button" @click="addOfferItem('NEW')" class="btn btn-info">Add New Group</button>
                            </div>
                            <div class="text-center">
                                <hr>
                                <button class="btn btn-danger">Create</button>
                            </div>
                        </div>
                    </div>
                    <div class="card" v-if="selected_offer_type == 2">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default form-group-default-select2">
                                        <label>Condition Product / Item</label>
                                        <vue-select2 :options="search_items" name="condition_items[0][product_price_id]"  place-holder="Select Item" allow-search="0"></vue-select2>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Min. Qty</label>
                                        <input type="text" name="condition_items[0][qty]" onkeypress="INGENIOUS.numericInput(event, false)" placeholder="Enter Min. Qty" class="form-control" value="{{ old('free_condition_item_min_qty') }}">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default form-group-default-select2">
                                        <label>Free Item</label>
                                        <vue-select2 :options="search_items" name="offer_items[0][product_price_id]" place-holder="Select Item" allow-search="0"></vue-select2>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Qty</label>
                                        <input type="text" name="offer_items[0][qty]" onkeypress="INGENIOUS.numericInput(event, false)" placeholder="Enter Qty" class="form-control" value="{{ old('free_item_qty') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-danger">Create</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop

@section('page-javascript')
    <script>

        Vue.prototype.$http = axios;

        new Vue({
            el:"#offerModule",
            data: {
                selected_offer_type: '',
                offer_items: [],
                offer_types: [
                    {value: 1, label: 'Free Products on Billing'},
                    {value: 2, label: 'Product to Product (Free Product on Selected Product)'},
                ],
                items: {!! collect($items)->toJson() !!},
            },
            mounted: function () {

                this.offer_items.push({
                    product_price_id: null, qty: 1, group_id: 1, id: this.getRandomNumber()
                });
            },
            methods: {
                getRandomNumber: function () {
                    return _.random(99999, true);
                },
                addOfferItem: function (group_id) {

                    let lastItem = _.chain(this.offer_items).sortBy('group_id').last().value();

                    if (group_id === 'NEW') {
                        this.offer_items.push({
                            product_price_id: null, qty: 1, group_id: (parseInt(lastItem.group_id)+1), id: this.getRandomNumber()
                        });
                    }
                    else {
                        this.offer_items.push({
                            product_price_id: null, qty: 1, group_id: group_id, id: this.getRandomNumber()
                        });

                        let offer_items = _.chain(this.offer_items).sortBy('group_id').value();
                        INGENIOUS.blockUI(true);
                        this.offer_items = [];
                        setTimeout(() => {
                            this.offer_items = offer_items;
                            INGENIOUS.blockUI(false);
                        }, 200);
                    }

                },
                removeOfferItem: function (item) {

                    if (this.offer_items.length === 1) {
                        swal('Warning', 'At least one offer should be available', 'warning');
                        return false;
                    }

                    this.offer_items = _.chain(this.offer_items).reject(offer_item => {
                        return offer_item.id === item.id;
                    }).value();
                }
            },
            computed: {

                search_items: function () {

                    return this.items.map(item => {
                        item.value = item.id;
                        item.label = item.product.name + ' (Code: ' + item.code + ')';
                        return item;
                    });

                }
            }
        });

    </script>
@stop
