@extends('admin.template.layout')

@section('title', 'Create Store')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Stores:admin-store-manager-store-view,Create:active)

    <div class="container-fluid container-fixed-lg" id="storeCreatePage">
        <form action="" method="post" role="form" onsubmit="INGENIOUS.blockUI(true)">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group form-group-default">
                                <label>Select Store Type</label>
                                <select name="type" class="form-control">
                                    <option value="">---Select---</option>
                                    @foreach(\App\Models\StoreManager\Store::getStoreTypes() as $storeType)
                                        <option value="{{ $storeType->id }}" {{ old('type') == $storeType->id ? 'selected' : null }}>{{ $storeType->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group form-group-default">
                                <label>Select Owner Type</label>
                                <select name="owner_type" class="form-control" v-model="owner_type">
                                    <option value="">---Select---</option>
                                    <option value="1">Member</option>
                                    <option value="2">Non-Member</option>
                                </select>
                            </div>
                            <hr v-if="owner_type == 1">
                            <div class="form-group form-group-default input-group"  v-if="owner_type == 1">
                                <div class="form-input-group">
                                    <label>Search User</label>
                                    <input type="text" class="form-control tracking_id_input" required autocomplete="off">
                                </div>
                                <div class="input-group-append">
                                    <button class="btn btn-danger" type="button" onclick="INGENIOUS.getUser(this, 'tracking_id_input')">Search</button>
                                </div>
                            </div>
                            <div class="form-group form-group-default" v-if="owner_type == 2">
                                <label>Store Panel Password <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="password" value="{{ old('password') }}" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Store Name <span class="text-danger">*</span></label>
                                        <input type="text" name="name" class="form-control" value="{{ old('name') }}" autocomplete="off">
                                    </div>
                                    <div class="form-group form-group-default">
                                        <label>Email</label>
                                        <input type="text" name="email" class="form-control" value="{{ old('email') }}" autocomplete="off">
                                    </div>
                                    <div class="form-group form-group-default">
                                        <label>Mobile <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="mobile" onkeypress="INGENIOUS.numericInput(event)" value="{{ old('mobile') }}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group form-group-default">
                                        <label>Store / WareHouse Address <span class="text-danger">*</span></label>
                                        <textarea class="form-control" name="address" cols="5" rows="5" placeholder="Enter Store Address" autocomplete="off">{{ old('address') }}</textarea>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group form-group-default">
                                                <label>City <span class="text-danger">*</span></label>
                                                <input type="text" name="city" class="form-control" value="{{ old('city') }}" autocomplete="off">
                                            </div>
                                            <div class="form-group form-group-default">
                                                <label>Pincode <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" name="pincode" value="{{ old('pincode') }}" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-group-default">
                                                <label>Select State</label>
                                                <select name="state_id" class="form-control" data-init-plugin="select2" required>
                                                    <option value=""> Select State</option>
                                                    @foreach($states as $index => $state)
                                                        <option value="{{ $state->id }}" {{ old('state_id') == $state->id ? 'selected' : null }}>{{ $state->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-primary btn-lg btn-rounded"> Submit </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop
@section('page-javascript')
    <script>
        new Vue({
            el: '#storeCreatePage',
            data: {
                owner_type: '',
            },
        });
    </script>
@stop

