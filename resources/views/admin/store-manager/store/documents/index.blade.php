@extends('admin.template.layout')

@section('title', 'Stores Documents')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Stores Documents:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control" value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="input-group">
                                <select name="status" class="full-width" data-init-plugin="select2" data-disable-search="true">
                                    <option value="">Select</option>
                                    <option value="1">Pending</option>
                                    <option value="2">Verified</option>
                                    <option value="3">Rejected</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <select name="type" class="full-width" data-init-plugin="select2" data-disable-search="true">
                                <option value="" selected disabled>Select Document Type</option>
                                <option value="1">Pancard</option>
                                <option value="2">AADHAR CARD</option>
                                <option value="3">CANCEL CHEQUE</option>
                                <option value="4">BANK PASSBOOK</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            <a href="{{ route('admin-store-manager-document-view', ['search' => Request::get('search'), 'dateRange' => Request::get('dateRange'), 'status' => Request::get('status'), 'type' => Request::get('type'), 'export' => 'yes']) }}" class="btn btn-info text-white btn-sm"> <i class="fa fa-download"></i> Download </a>
                            @refreshBtn()
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Store Name</th>
                            <th>Ref. Number</th>
                            <th>Document Type</th>
                            <th width="10%">Image</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($documents) == 0)
                            <tr>
                                <td colspan="10" class="text-center">No Stores Documents Available</td>
                            </tr>
                        @endif
                        @foreach($documents as $index => $document)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($documents, $index) }}</td>
                                <td class="text-primary">
                                    {{ $document->store->name }} <br>({{ $document->store->tracking_id }})<br>
                                    @if($document->store->type == \App\Models\StoreManager\Store::PREMIUM_TYPE)
                                        <span class="badge badge-success">Premium</span>
                                    @elseif($document->store->type == \App\Models\StoreManager\Store::PRO_TYPE)
                                        <span class="badge badge-info">Pro</span>
                                    @elseif($document->store->type == \App\Models\StoreManager\Store::STANDARD_TYPE)
                                        <span class="badge badge-info">Standard</span>
                                    @else
                                        <span class="badge badge-warning">Basic</span>
                                    @endif

                                </td>
                                <td>
                                    {{ $document->number ? $document->number : 'N.A' }}
                                </td>
                                <td>{{ $document->document_name }}</td>
                                <td>
                                    <a href="{{ env('DOCUMENT_IMAGE_URL') . $document->image }}" target="_blank">
                                        <img src="{{ env('DOCUMENT_IMAGE_URL') . $document->image }}" alt="" width="100%" title="Click to View Full Image">
                                    </a>

                                </td>
                                <td>
                                    @if ($document->status == 1 )
                                        <span class="badge badge-warning">Pending</span>
                                    @elseif ($document->status == 2)
                                        <span class="badge badge-success">Verified</span>
                                    @else
                                        <span class="badge badge-danger">Rejected</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('admin-store-manager-document-update', ['id' => $document->id]) }}" class="btn btn-danger btn-xs m-b-5">Update</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $documents->appends(['search' => Request::get('search'), 'dateRange' => Request::get('dateRange'), 'type' => Request::get('type'), 'status' => Request::get('status'), ])->links() }}
                </div>
            </div>
        </div>
    </div>
@stop