@extends('admin.template.layout')

@section('title', 'Document Update')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard, Document List:admin-store-manager-document-view,Update:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6 offset-md-3 offset-lg-3">
                <div class="card">
                    <div class="card-body">
                        <form action="" method="post" role="form" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
                            {{ csrf_field() }}
                            <input type="hidden" name="type" value="{{ $document->type }}">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    User:
                                    <span class="pull-right text-danger">
                                        ({{ $document->store->tracking_id }})
                                    </span>
                                </li>
                                <li class="list-group-item">
                                    Type:
                                    <span class="pull-right">
                                     <td>{{ $document->document_name }}</td>
                                    </span>
                                </li>
                                <li class="list-group-item">
                                    {{ ucwords(strtolower($document->document_name)) }} Number:
                                    <span class="pull-right">
                                        {{ $document->number }}
                                    </span>
                                </li>

                                <li class="list-group-item">
                                    Image:
                                    <span class="pull-right">
                                         <a href="{{ env('DOCUMENT_IMAGE_URL').$document->image }}" target="_blank" class="btn-xs btn btn-primary">
                                        View Image
                                    </a>
                                    </span>
                                </li>
                                @if($document->secondary_image)
                                    <li class="list-group-item">
                                        Back Image:
                                        <span class="pull-right">
                                         <a href="{{ env('DOCUMENT_IMAGE_URL').$document->secondary_image }}" target="_blank" class="btn-xs btn btn-primary">
                                        View Image
                                    </a>
                                    </span>
                                    </li>
                                @endif
                            </ul>

                            <div class="form-group form-group-default form-group-default-select2 m-t-10">
                                <label>Status</label>
                                <select name="status" class="full-width" data-init-plugin="select2">
                                    <option value="1" {{ $document->status == 1 ? 'selected' : '' }}>Pending</option>
                                    <option value="2"  {{ $document->status == 2 ? 'selected' : '' }}>Approved</option>
                                    <option value="3"  {{ $document->status == 3 ? 'selected' : '' }}>Rejected</option>
                                </select>
                            </div>

                            <div class="form-group ">
                                <textarea class="form-control" name="remarks" cols="5" rows="5" placeholder="Remarks" >{{ $document->remarks }}</textarea>
                            </div>
                            <div class="text-center m-t-10">
                                <button class="btn btn-theme btn-sm">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop