@extends('admin.template.layout')

@section('title', 'Supply Transfer Report')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Supply Transfer Report:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <div class="auto-overflow">
                    <canvas id="chart" width="400" height="200"></canvas>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row mb-2">
                        <div class="col-md-5">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <button class="btn btn-primary"> Search </button>
                            @refreshBtn()
                        </div>
                    </div>
                </form>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Date</th>
                            <th class="bg-danger-light text-white">Amount</th>
                            <th>Total Supplies</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($records) == 0)
                            <tr>
                                <td colspan="10" class="text-center">No Supply Report Available</td>
                            </tr>
                        @endif
                        @foreach ($records as $index => $record)
                            <tr>
                                <td>{{ ($index+1) }}</td>
                                <td>{{ $record->date->format('M d, Y') }}</td>
                                <td class="bg-danger-lighter">{{ number_format($record->amount) }}</td>
                                <td>{{ number_format($record->supply_count) }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')

    <script>
        var ctx = document.getElementById('chart');
        ctx.height = 90;
        new Chart(ctx, {
            type: 'line',
            data: {
                labels: [
                    @foreach(collect($records) as $record)
                        '{{ $record->date->format('M d') }}',
                    @endforeach
                ],
                datasets: [{
                    label: 'Supplies',
                    data: [{{ implode(collect($records)->pluck('amount')->toArray(), ', ') }}],
                    backgroundColor: 'rgb(90, 29, 167)', borderColor: 'rgb(93, 29, 167)', borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true, precision: 0
                        }
                    }]
                }
            }
        });
    </script>
@stop

@section('import-javascript')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js"></script>
@stop