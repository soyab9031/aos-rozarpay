@extends('admin.template.layout')

@section('title', 'Product Ranking')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Product Ranking:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row mb-2">
                        <div class="col-md-5">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <button class="btn btn-primary"> Search </button>
                            @refreshBtn()
                        </div>
                    </div>
                </form>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Item</th>
                            <th>Product Code</th>
                            <th class="bg-danger-light text-white">Item Sold</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($items) == 0)
                            <tr>
                                <td colspan="10" class="text-center">No Product Ranking Available</td>
                            </tr>
                        @endif
                        @foreach ($items as $index => $item)
                            <tr>
                                <td>{{ ($index+1) }}</td>
                                <td>{{ $item->product->name }}</td>
                                <td>{{ $item->code }}</td>
                                <td class="bg-danger-lighter">{{ number_format($item->sold_qty) }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="3" class="text-right">Total</td>
                            <td class="bg-danger-light text-white">{{ number_format(collect($items)->sum('sold_qty')) }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

@stop