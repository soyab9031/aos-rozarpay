@extends('admin.template.layout')

@section('title', 'Purchase Order - ' . $purchase_order->customer_order_id)

@section('content')

    @breadcrumb(Dashboard:admin-dashboard, Purchsae Orders:admin-store-manager-purchase-order-view, Details - {{ $purchase_order->customer_order_id }}:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <b>PO ID:</b> <span class="pull-right">{{ $purchase_order->customer_order_id }}</span>
                            </li>
                            <li class="list-group-item">
                                <b>Supplier:</b> <span class="pull-right">{{ $purchase_order->supplier->name }}</span>
                            </li>
                            <li class="list-group-item">
                                <b>Mobile:</b> <span class="pull-right">{{ $purchase_order->supplier->mobile }}</span>
                            </li>
                            <li class="list-group-item">
                                <b>Address:</b> <span class="pull-right">{{ $purchase_order->supplier->address }}, {{ $purchase_order->supplier->city }}, {{ $purchase_order->supplier->state->name }} - {{ $purchase_order->supplier->pincode }}</span>
                            </li>
                        </ul>
                        <ul class="list-group mt-3">
                            <li class="list-group-item">
                                <b>Amount:</b> <span class="pull-right">{{ number_format($purchase_order->amount, 2) }}</span>
                            </li>
                            <li class="list-group-item">
                                <b>GST:</b> <span class="pull-right">{{ number_format($purchase_order->tax_amount, 2) }}</span>
                            </li>
                            <li class="list-group-item">
                                <b>Total:</b> <span class="pull-right">{{ number_format($purchase_order->total, 2) }}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Item</th>
                                <th>Qty</th>
                                <th>Amount</th>
                                <th>Total Taxable Amount</th>
                                <th>GST</th>
                                <th class="text-right">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($purchase_order->details as $index => $detail)
                                <tr>
                                    <td>{{ $index+1 }}</td>
                                    <td>
                                        {{ $detail->product_name }}
                                        @if($detail->note)
                                            <br>
                                            <small>Note: {{ $detail->note }}</small>
                                        @endif
                                    </td>
                                    <td>{{ $detail->qty }}</td>
                                    <td>{{ $detail->amount }}</td>
                                    <td>{{ $detail->total_taxable_amount }}</td>
                                    <td>{{ round($detail->total_taxable_amount * $detail->gst_percentage / 100, 2) }} <br> ({{ $detail->gst_percentage }}%)</td>
                                    <td class="text-right"> {{ number_format($detail->total_amount, 2) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="mt-3">
                            <a href="{{ route('admin-store-manager-purchase-order-update', ['id' => $purchase_order->id ]) }}" class="btn btn-danger btn-xs"> Update <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop