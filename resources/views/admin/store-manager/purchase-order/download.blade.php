@php
    $f = new \App\Library\CurrencyInWord;
@endphp
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $filename }}</title>
    <style>
        @media print {
            @page  {
                margin: .5cm .3cm;
            }
        }
        table { page-break-inside:auto }
        tr    { page-break-inside:avoid; page-break-after:auto }
        thead { display:table-header-group }
        tfoot { display:table-footer-group }
        body {
            font-family: Verdana, Arial, Helvetica, sans-serif;
        }
        .text-center {
            text-align: center !important;
        }
        .text-right {
            text-align: right !important;
        }
        .datagrid table {
            border-collapse: collapse;
            text-align: left;
            width: 100%;
        }

        .datagrid {
            font: normal 12px/150% Verdana, Arial, Helvetica, sans-serif;
            background: #fff;
            overflow: hidden;
            border: 1px solid #000;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        .datagrid table td,
        .datagrid table th {
            padding: 3px 10px;
        }

        .datagrid table tbody tr.header td {
            filter: progid: DXImageTransform.Microsoft.gradient(startColorstr='#FFFAC3', endColorstr='#80141C');
            background-color: #000;
            color: #FFFFFF;
            font-size: 10px;
            font-weight: bold;
            padding: 7px;
        }
        .datagrid table tbody tr.table-title td
        {
            text-align: center;
            background: #6241A7;
            color: #fff;
            font-size: 12px !important;
            border-top: 2px solid #000;
            padding: 7px;
            font-weight: 600;
        }
        .datagrid table tbody tr.child-category-title td
        {
            text-align: center;
            font-size: 13px;
        }
        .datagrid table thead th:first-child {
            border: none;
        }

        .datagrid table tbody td {
            color: #000;
            border-left: 1px solid #000;
            font-size: 10px;
            font-weight: normal;
            padding: 8px;
            border-bottom: 1px solid;
        }
        .datagrid table tbody .alt td {
            background: #F7CDCD;
            color: #000;
        }
        .datagrid table tbody td:first-child {
            border-left: none;
        }

        .datagrid table tbody tr:last-child td {
            border-bottom: none;
        }
        .border-bottom {
            border-bottom: 1px solid #000 !important;
        }
    </style>
</head>
<body>
<table style="width: 100%">
    <tr>
        <td width="50%">
            <img src="{{ config('project.url') }}user-assets/images/company/logo.png" alt="{{ config('project.brand') }}" class="mb-3" width="100px">
            <div style="font-size: 16px;margin-top: 1rem" >{{ collect($company_profile)->where('title', 'company_name')->first()->value }}</div>
            <div class="address" style="font-size: 14px; color: #122b40;">
                <div style="font-size: 14px; color: #122b40;">Address: </div>
                {{ collect($company_profile)->where('title', 'address')->first()->value }}
                <br>{{ collect($company_profile)->where('title', 'city')->first()->value }},
                {{ collect($company_profile)->where('title', 'state')->first()->value }}
                - {{ collect($company_profile)->where('title', 'pincode')->first()->value }}
                <br>
            </div>
        </td>
        <td width="50%" style="text-align: right">
            <div style="font-size: 30px; font-weight: 600; text-transform: uppercase;"> PURCHASE ORDER</div>
            <table style="width: 100%;border-spacing: 0;border-collapse: collapse;border: 1px solid;margin: 10px 0px;">
                <tbody>
                <tr>
                    <td style="padding: 5px;border-right: 1px solid;border-bottom: 1px solid;">PO Number </td>
                    <td style="padding: 5px;border-bottom: 1px solid;">{{ $purchase_order->customer_order_id }}</td>
                </tr>
                <tr>
                    <td style="padding: 5px;border-right: 1px solid;border-bottom: 1px solid;">Date</td>
                    <td style="padding: 5px;border-bottom: 1px solid;">{{ \Carbon\Carbon::parse($purchase_order->created_at)->format('M d, Y') }}</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
<div class="datagrid" style="margin-top: 15px;">
    <table>
        <tbody>
        <tr>
            <td width="50%">
                <div style="font-size: 15px; margin-bottom: 5px; font-weight: 600;">TO</div>
                <div style="font-size: 15px;">Name : {{ $purchase_order->supplier->name }}</div>
                <div>GST IN: {{ $purchase_order->supplier->gst_number }}</div>
            </td>
            <td width="50%">
                <div class="address" style="font-size: 14px; color: #122b40;">
                    <div style="font-size: 14px; color: #122b40; font-weight: 800">Address: </div>
                    {{ $purchase_order->supplier->address }},<br>
                    {{ $purchase_order->supplier->city }} - {{ $purchase_order->supplier->pincode }} ({{ $purchase_order->supplier->state ? $purchase_order->supplier->state->name : 'N.A' }})<br>

                    Contact: {{ $purchase_order->supplier->mobile }}<br>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <table style="width: 100%;">
        <tbody>
        <tr class="table-title">
            <td>#</td>
            <td>Item</td>
            <td>Qty</td>
            <td>Amount</td>
            <td>Taxable Amount</td>
            <td>GST</td>
            <td>Total</td>
        </tr>
        @foreach($purchase_order->details as $index => $detail)
            <tr>
                <td>{{ $index+1 }}</td>
                <td>
                    {{ $detail->product_name }}
                    @if($detail->note)
                        <br>
                        <small>Note: {{ $detail->note }}</small>
                    @endif
                </td>
                <td>{{ $detail->qty }}</td>
                <td>{{ $detail->amount }}</td>
                <td>{{ $detail->total_taxable_amount }}</td>
                <td>{{ round($detail->total_tax_amount, 2) }} <br> ({{ $detail->gst_percentage }}%)</td>
                <td class="text-right"> {{ number_format($detail->total_amount, 2) }}</td>
            </tr>
        @endforeach
        <tr class="table-title" style="font-size: 15px;">
            <td colspan="2" class="text-right">Total</td>
            <td>{{ collect($purchase_order->details)->sum('qty') }}</td>
            <td></td>
            <td>{{ number_format(collect($purchase_order->details)->sum('total_taxable_amount') ,2) }}</td>
            <td>{{ number_format(collect($purchase_order->details)->sum('total_tax_amount') ,2) }}</td>
            <td>{{ number_format(collect($purchase_order->details)->sum('total_amount') ,2) }}</td>
        </tr>
        <tr>
            <td colspan="10">
                Amount Chargeable (in Words):
                <br> <b style="font-size: 15px;">{{ ucwords($f->display(collect($purchase_order->details)->sum('total_amount'))) }}</b>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div style="text-align: center; margin-top: 20px; font-size: 14px;">
    This is a Computer Generated Purchase Order
</div>
</body>
</html>