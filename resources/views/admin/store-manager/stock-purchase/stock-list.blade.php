@extends('admin.template.layout')

@section('title', 'Company Stock')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Company Stock Records:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-lg-3 mb-3">
                <div class="list-group">
                    <div class="list-group-item d-flex align-items-center">
                        <div class="width-40 height-40 d-flex align-items-center justify-content-center bg-danger-lighter text-white rounded ml-n1">
                            <i class="fa fa-database fa-lg text-danger"></i>
                        </div>
                        <div class="flex-fill pl-3 pr-3">
                            <div class="font-medium-3">{{ number_format($out_of_stock) }}</div>
                            <div class="font-small-2">Out of Stock</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 mb-3">
                <div class="list-group">
                    <div class="list-group-item d-flex align-items-center">
                        <div class="width-40 height-40 d-flex align-items-center justify-content-center bg-warning-light text-white rounded ml-n1">
                            <i class="fa fa-database fa-lg text-dark"></i>
                        </div>
                        <div class="flex-fill pl-3 pr-3">
                            <div class="font-medium-3">{{ number_format($near_to_empty) }}</div>
                            <div class="font-small-2">Near to Empty</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search Product Name or Product Code" name="search" class="form-control" value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            <a href="{{ route('admin-store-manager-stock-list', ['download' => 'yes','search' => Request::get('search'),]) }}" class="btn btn-info btn-sm" title="Download">
                                <i class="fa fa-cloud-download"></i> <span class="bold">Download</span>
                            </a>
                            @refreshBtn()
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Product</th>
                            <th class="bg-danger-light text-white">Stock</th>
                            <th class="bg-primary-light text-white">Minimum Qty</th>
                            <th>Distributor Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($records) == 0)
                            <tr>
                                <td colspan="10" class="text-center">No Stocks Available</td>
                            </tr>
                        @endif
                        @foreach ($records as $index => $record)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($records, $index) }}</td>
                                <td>
                                 <span>{{ $record->product_price->product->name }}
                                     @if($record->minimum_stock && $record->minimum_stock->qty >= $record->balance)
                                         <i class="fa fa-exclamation-triangle text-danger"></i>
                                     @endif
                                 </span>
                                    <br>
                                    <code>Code: {{ $record->product_price->code }}</code>
                                </td>
                                <td class="bg-danger-lighter text-dark">{{ number_format($record->balance) }}</td>
                                <td class="bg-primary-lighter text-dark">
                                    <span>{{ $record->minimum_stock ? $record->minimum_stock->qty : 0}}</span>
                                    <button type="button" class="btn btn-xs btn-primary pull-right" data-toggle="modal" data-keyboard="false" data-target="#updateMinimumQty{{ $record->product_price->id }}" title="Edit Minimum Qty of {{ $record->product_price->product->name }}"><i class="fa fa-edit"></i></button>

                                    <div class="modal fade" id="updateMinimumQty{{ $record->product_price->id }}" tabindex="-1" role="dialog" aria-labelledby="basicModalLabel3" aria-hidden="true">
                                        <form action="{{ route('admin-store-manager-stock-minimum-qty-update') }}" onsubmit="INGENIOUS.blockUI()">
                                            {{ csrf_field() }}
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title mt-0" id="basicModalLabel3">Update Minimum Quantity </h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group mt-2">
                                                            <label for="">Product</label>
                                                            <input type="text" value="{{ $record->product_price->product->name }}" readonly disabled class="form-control">
                                                        </div>
                                                        <div class="form-group mt-2">
                                                            <label for="">Current Minimum Qty</label>
                                                            <input type="number" name="qty"
                                                                   value="{{ $record->minimum_stock ? $record->minimum_stock->qty : 0 }}" class="form-control">
                                                        </div>
                                                        <input type="hidden" name="product_price_id" value="{{ $record->product_price->id }}">
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-danger transferPins">Update</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </td>
                                <td>{{ number_format($record->product_price->distributor_price) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $records->appends(['search' => Request::get('search')])->links() }}
                </div>
            </div>
        </div>
    </div>
@stop