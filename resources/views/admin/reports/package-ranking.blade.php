@extends('admin.template.layout')

@section('title', 'Package Ranking')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Package Ranking:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row mb-2">
                        <div class="col-md-5">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <button class="btn btn-primary"> Search </button>
                            @refreshBtn()
                        </div>
                    </div>
                </form>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Package</th>
                            <th class="bg-danger-light text-white">QTY</th>
                            <th class="bg-complete-darker text-white">Total PVs</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($packages) == 0)
                            <tr>
                                <td colspan="10" class="text-center">No Product Ranking Available</td>
                            </tr>
                        @endif
                        @foreach ($packages as $index => $package)
                            <tr>
                                <td>{{ ($index+1) }}</td>
                                <td>{{ $package->name }}</td>
                                <td class="bg-danger-lighter">{{ number_format($package->sold_qty) }}</td>
                                <td class="bg-complete-lighter">{{ number_format($package->pv * $package->sold_qty) }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="2" class="text-right">Total</td>
                            <td class="bg-danger-light text-white">{{ number_format(collect($packages)->sum('sold_qty')) }}</td>
                            <td class="bg-complete-darker text-white">{{ number_format(collect($packages)->sum(function ($package) { return $package->pv * $package->sold_qty; })) }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

@stop