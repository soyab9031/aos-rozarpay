@extends('admin.template.layout')

@section('title', 'Update Customer Details ')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Customers:admin-customer-view,Update:active)

    <div class="container-fluid container-fixed-lg">
        <form action="" method="post" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Name</label>
                                        <input type="text" name="name" class="form-control"
                                               value="{{ $customer->name }}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Mobile</label>
                                        <input type="text" name="mobile" class="form-control"
                                               value="{{ $customer->mobile }}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Email</label>
                                        <input type="text" name="email" class="form-control"
                                               value="{{ $customer->email }}" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label> Birth Date</label>
                                        <input type="text" name="birth_date" id="BirthDate" class="form-control"
                                               value="{{ $customer->birth_date }}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label> Set New Password</label>
                                        <input type="text" name="password" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-danger btn-rounded">
                                    Update Account
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop

@section('page-javascript')
    <script>
        $('#BirthDate').datepicker({
            autoclose: true,
            format: "dd-mm-yyyy",
            startDate: "-99y",
            endDate: "-18y"
        });
    </script>
@stop