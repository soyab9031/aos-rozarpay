@extends('admin.template.layout')

@section('title', 'Customer')

@section('content')
    @breadcrumb(Dashboard:admin-dashboard,Customer:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i
                                                class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control"
                                       value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i
                                                class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range"
                                       value="{{ Request::get('dateRange') }}"
                                       placeholder="Date range for Filter" readonly>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button class="btn btn-danger btn-sm"> Search</button>
                            @refreshBtn()
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="table-responsive m-t-10">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Access</th>
                            <th>Date</th>
                            <th>Name</th>
                            {{--<th>Sponsor</th>--}}
                            <th>Mobile</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($customers as $index => $customer)
                            <tr class="accordion-toggle">
                                <td>{{ \App\Library\Helper::tableIndex($customers, $index) }}</td>
                                <td>
                                    <div class="btn-toolbar flex-wrap" role="toolbar">
                                        <div class="btn-group sm-m-t-10">
                                            <a href="{{ route('admin-customer-account-access', ['id' => $customer->id]) }}"
                                               target="_blank" class="btn btn-default" data-toggle="tooltip"
                                               title="View Account">
                                                <i class="fa fa-universal-access text-primary"></i>
                                            </a>
                                            {{--<a href="{{ route('admin-customer-update', ['id' => $customer->id]) }}"  class="btn btn-default" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil text-dark"></i></a>--}}
                                            <a href="javascript:void(0)" data-toggle="collapse" data-parent="#SellerDetails" data-target=".customerDetails{{ $index+1 }}" class="btn btn-secondary" title="View Details">
                                                <i class="fa fa-eye text-dark"></i>
                                            </a>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    {{ $customer->created_at->format('M d, Y h:i A') }}
                                </td>
                                <td class="text-primary">
                                    {{ $customer->name }} <br>
                                    ({{ $customer->tracking_id }})
                                </td>
                                {{--<td class="text-primary">--}}
                                    {{--{{ $customer->user->detail->full_name }} <br>--}}
                                    {{--({{ $customer->user->tracking_id }})--}}
                                {{--</td>--}}
                                <td>{{ $customer->mobile }}</td>
                                <td>{{ $customer->email }}</td>
                            </tr>
                            <tr class="customer-row-details">
                                <td colspan="10" class="p-0">
                                    <div class="accordion-body collapse customerDetails{{ $index+1 }}">
                                        <div class="p-2 table-responsive">
                                            <table class="table table-bordered ">
                                                <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Password</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>{{ $customer->mobile }}</td>
                                                    <td>{{ $customer->password }}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $customers->appends(['search' => Request::get('search'), 'dateRange' => Request::get('dateRange') ])->links() }}
                </div>
            </div>
        </div>
    </div>
@stop