@extends('admin.template.layout')

@section('title', 'Wallet Status')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard, Wallet Status:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-6 offset-md-3 offset-lg-3">
                            <div class="input-group">
                                <input type="text" placeholder="Search Tracking Id" name="tracking_id" class="form-control" value="{{ Request::get('tracking_id') }}">
                                <div class="input-group-prepend">
                                    <button class="btn btn-danger"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <p class="small text-center text-danger m-t-10">
                                Search Users through their Tracking ID & Get Complete Wallet Information
                            </p>
                        </div>
                    </div>
                    <ul class="list-group m-t-10">
                        <li class="list-group-item bg-danger-darker text-white">
                            User: <span class="pull-right">{{ $user->detail->full_name }}</span>
                        </li>
                    </ul>
                </form>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <div class="table-responsive m-t-10">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th colspan="4">
                                <span class="text-complete"><i class="fa fa-square"></i> Credit &nbsp; </span>
                                <span class="text-danger"><i class="fa fa-square"></i> Debit</span>
                            </th>
                            <th colspan="2" class="text-center bg-success-darker text-white">Tax / Charges</th>
                            <th colspan="4"></th>
                        </tr>
                        <tr>
                            <th>#</th>
                            <th>Date</th>
                            <th>Type</th>
                            <th>Total</th>
                            <th class="bg-success-lighter">TDS</th>
                            <th class="bg-success-lighter">Admin</th>
                            <th>Amount</th>
                            <th width="20%">Remark</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($wallets->count() == 0)
                            <tr>
                                <td colspan="10" class="text-center">
                                    Right Now, <span class="text-danger">{{ $user->detail->full_name }}</span> has no any wallet information.<br>
                                    After Core Calculations, Income will be displayed here...!!
                                </td>
                            </tr>
                        @endif
                        @foreach($wallets as $index => $wallet)
                            <tr class="{{ $wallet->type == 1 ? 'bg-complete-lighter' : 'bg-danger-lighter' }}">
                                <td>{{ \App\Library\Helper::tableIndex($wallets, $index) }}</td>
                                <td>{{ $wallet->created_at->format('M d, Y') }}</td>
                                <td><span class="label label-primary">{{ $wallet->income_type_name }}</span></td>
                                <td class="text-dark">{{ $wallet->total }}</td>
                                <td>{{ $wallet->tds }}</td>
                                <td>{{ $wallet->admin_charge }}</td>
                                <td class="text-dark">{{ $wallet->amount }}</td>
                                <td>{{ $wallet->remarks }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $wallets->appends(['tracking_id' => Request::get('tracking_id')])->links() }}
                </div>
            </div>
        </div>
    </div>
@stop
