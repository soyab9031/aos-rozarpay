@extends('website.template.layout')
@section('page-title','Vision')
@section('page-content')
    <div class="page-content">
        <div class="holder mt-0">
            <div class="container">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('website-home')  }}">Home</a></li>
                    <li><span>Vision</span></li>
                </ul>
            </div>
        </div>
        <div class="holder mt-0">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 aside" id="centerColumn">
                        <div class="post-prw-big vision-image">
                            <img src="/website-assets/images/home/vision.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-6 aside aside--right" id="sideColumn">
                        <div class="title-wrap text-center">
                            <h2 class="h1-style btn-decor">Our Vision</h2>
                        </div>
                        <p class="post-teaser">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop