@extends('website.template.layout')
@section('page-title', 'Customer Registration')
@section('page-content')
    <div class="page-content">
        <div class="holder mt-0">
            <div class="container">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('website-home') }}">Home</a></li>
                    <li><a href="{{ route('customer-login') }}">Login</a></li>
                    <li><span>Registration</span></li>
                </ul>
            </div>
        </div>
        <div class="holder mt-0">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 offset-md-3 offset-lg-3 aside">
                        <form class="form-horizontal" action="" method="post" onsubmit="INGENIOUS.blockUI(true)">
                            {{ csrf_field() }}
                            @if(session('error'))
                                <div class="alert alert-danger">{{ session('error') }}</div>
                            @elseif(session('errors'))
                                <div class="alert alert-danger">{{ session('errors')->first() }}</div>
                            @endif
                            <div class="row">
                                <div class="col-md-6">
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <label>Name</label>
                                        <input type="text" class="form-control round" name="name"
                                               placeholder="Enter Your Name" value="{{ old('name') }}" required>
                                    </fieldset>
                                </div>
                                <div class="col-md-6">
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <label>Mobile No</label>
                                        <input type="text" class="form-control round" name="mobile"
                                               onkeypress="INGENIOUS.numericInput(event)"
                                               placeholder="Enter Your Mobile Number" value="{{ old('mobile') }}" required>
                                        <div class="form-control-position"><i class="ft-phone"></i></div>
                                    </fieldset>
                                </div>
                                <div class="col-md-6">
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <label>Email ID</label>
                                        <input type="email" class="form-control round" name="email"
                                               placeholder="Enter Your Email ID" value="{{ old('email') }}" required>
                                        <div class="form-control-position"><i class="ft-mail"></i></div>
                                    </fieldset>
                                </div>
                                <div class="col-md-6">
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <label>Birth Date</label>
                                        <input type="text" name="birth_date" id="BirthDate"
                                               class="form-control round"
                                               placeholder="Enter Date Of Birth" value="{{ old('birth_date') }}" readonly>
                                        <div class="form-control-position"><i class="la la-birthday-cake"></i></div>
                                    </fieldset>
                                </div>
                                <div class="col-md-6">
                                    <label>Password</label>
                                    <div class="input-group">
                                        <input type="password" class="form-control passwordInput form-control-bg" name="password"
                                               placeholder="Enter Password">
                                        <div class="input-group-append">
                                            <button class="btn btn-bg-gradient-x-website-red" type="button"
                                                    onclick="INGENIOUS.showPassword(this)">
                                                <i class="fa fa-eye-slash"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Confirm Password</label>
                                    <div class="input-group">
                                        <input type="password" id="password" class="form-control passwordInput form-control-bg"
                                               name="password_confirmation" placeholder="Enter Confirm Password">
                                        <div class="input-group-append">
                                            <button class="btn btn-bg-gradient-x-website-red" type="button"
                                                    onclick="INGENIOUS.showPassword(this)">
                                                <i class="fa fa-eye-slash"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="form-group text-center">
                                <button type="submit"
                                        class="btn round btn-block btn-glow btn-bg-gradient-x-orange-rad col-12 mr-1 mb-1">
                                    Create Account
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('page-javascript')
    <script>

        @if(Request::get('referral'))
        $(function () {
            getUser('{{ Request::get('referral') }}');
        });
        @endif

        @if(old('tracking_id'))
        $(function () {
            getUser('{{ old('tracking_id') }}');
        });
        @endif

        function searchUser(element) {
            getUser($(element).val());
        }

        function getUser(tracking_id) {

            let sponsorParent = $('#sponsorParent');

            $.get('{{ route("user-register-get-user") }}', {'tracking_id': tracking_id}, function (response) {

                sponsorParent.html('');

                if (!response.status) {
                    swal('Oops', response.message, 'warning');
                    return false;
                }

                sponsorParent.html('<div class="bg-primary text-white font-weight-bold rounded p-1 cart">' + response.user.name + '</div>');

            });
        }
    </script>
    <script>
        $('#BirthDate').datepicker({
            autoclose: true,
            format: "dd-mm-yyyy",
            startDate: "-99y",
            endDate: "-18y"
        });
    </script>
@stop