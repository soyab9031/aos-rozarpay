@extends('website.template.layout')
@section('page-title','Home')
@section('page-content')
    <div class="holder fullwidth full-nopad mt-0">
        <div class="container">
            <div class="bnslider-wrapper">
                @if(count($banners) > 0)
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            @foreach($banners as $banner)
                                <div class="swiper-slide">
                                    <img class="img-fluid rounded" src="{{ env('BANNER_IMAGE_URL'). $banner->image }}" alt="{{ $banner->image }}" width="100%">
                                </div>
                            @endforeach
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-pagination"></div>
                    </div>
                @else
                    <div class="bnslider bnslider--lg keep-scale" id="bnslider-001" data-slick='{"arrows": true, "dots": true}' data-autoplay="false" data-speed="5000" data-start-width="1920" data-start-height="845" data-start-mwidth="480" data-start-mheight="578">

                        <div class="bnslider-slide bnslide-sport-2">
                            <div class="bnslider-image-mobile" style="background-image: url('/website-assets/images/new_slider/1.png');"></div>
                            <div class="bnslider-image" style="background-image: url('/website-assets/images/new_slider/1.png');"></div>
                        </div>
                        <div class="bnslider-slide bnslide-sport-1">
                            <div class="bnslider-image-mobile" style="background-image: url('/website-assets/images/new_slider/2.png');"></div>
                            <div class="bnslider-image" style="background-image: url('/website-assets/images/new_slider/2.png');"></div>
                        </div>
                        <div class="bnslider-slide bnslide-sport-3">
                            <div class="bnslider-image-mobile" style="background-image: url('/website-assets/images/new_slider/3.png');"></div>
                            <div class="bnslider-image" style="background-image: url('/website-assets/images/new_slider/3.png');"></div>
                        </div>
                        <div class="bnslider-slide bnslide-sport-3">
                            <div class="bnslider-image-mobile" style="background-image: url('/website-assets/images/new_slider/4.png');"></div>
                            <div class="bnslider-image" style="background-image: url('/website-assets/images/new_slider/4.png');"></div>
                        </div>
                        <div class="bnslider-slide bnslide-sport-3">
                            <div class="bnslider-image-mobile" style="background-image: url('/website-assets/images/new_slider/6.png');"></div>
                            <div class="bnslider-image" style="background-image: url('/website-assets/images/new_slider/6.png');"></div>
                        </div>
                    </div>
                    <div class="bnslider-loader">
                        <div class="loader-wrap">
                            <div class="dots">
                                <div class="dot one"></div>
                                <div class="dot two"></div>
                                <div class="dot three"></div>
                            </div>
                        </div>
                    </div>
                    <div class="bnslider-arrows container">
                        <div></div>
                    </div>
                    <div class="bnslider-dots container"></div>

                @endif
            </div>
        </div>
    </div>
    {{--<div class="holder">--}}
    {{--<div class="container">--}}
    {{--<div class="row vert-margin-middle mobile-sm-pad">--}}
    {{--@if(count($categories) >  0)--}}
    {{--@foreach($categories as $category)--}}
    {{--<div class="col-6 col-md-3">--}}
    {{--<a href="#" class="collection-box">--}}
    {{--<div class="collection-box-image">--}}
    {{--<img src="{{ env('CATEGORY_IMAGE_URL').$category->image  }}" data-src="{{ env('CATEGORY_IMAGE_URL').$category->image  }}" alt="">--}}
    {{--</div>--}}
    {{--<div class="collection-box-text">--}}
    {{--<h4 class="collection-box-title">{{ $category->name  }}</h4>--}}
    {{--</div>--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--@endforeach--}}
    {{--@else--}}
    {{--<div class="col-6 col-md-3">--}}
    {{--<a href="#" class="collection-box bg-color">--}}
    {{--<div class="collection-box-image"><img src="/website-assets/images/static_products/1.png" data-src="/website-assets/images/static_products/1.png" class="lazyload" alt=""></div>--}}
    {{--<div class="collection-box-text">--}}
    {{--<h4 class="collection-box-title">Kitchen</h4>--}}
    {{--</div>--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--<div class="col-6 col-md-3">--}}
    {{--<a href="#" class="collection-box bg-color">--}}
    {{--<div class="collection-box-image"><img src="/website-assets/images/static_products/1.png" data-src="/website-assets/images/static_products/1.png" class="lazyload" alt=""></div>--}}
    {{--<div class="collection-box-text">--}}
    {{--<h4 class="collection-box-title">SPORT</h4>--}}
    {{--</div>--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--<div class="col-6 col-md-3">--}}
    {{--<a href="#" class="collection-box bg-color">--}}
    {{--<div class="collection-box-image"><img src="/website-assets/images/static_products/1.png" data-src="/website-assets/images/static_products/1.png" class="lazyload" alt=""></div>--}}
    {{--<div class="collection-box-text">--}}
    {{--<h4 class="collection-box-title">AYURVEDA</h4>--}}
    {{--</div>--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--<div class="col-6 col-md-3">--}}
    {{--<a href="#" class="collection-box bg-color">--}}
    {{--<div class="collection-box-image"><img src="/website-assets/images/static_products/1.png" data-src="/website-assets/images/static_products/1.png" class="lazyload" alt=""></div>--}}
    {{--<div class="collection-box-text">--}}
    {{--<h4 class="collection-box-title">BABY CARE</h4>--}}
    {{--</div>--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    <div class="holder fullboxed bgcolor mt-0 py-2 py-sm-3">
        <div class="container">
            <div class="row bnr-grid">
                @if(count($categories) > 0)
                    @foreach($categories as $category)
                        <div class="col-md">
                            <a href="{{ route('website-products-view',['slug' => $category->slug])  }}" class="bnr-wrap">
                                <div class="bnr bnr1 bnr--style-1 bnr--right bnr--middle bnr-hover-scale" data-fontratio="5.55">
                                    <img src="{{ env('CATEGORY_IMAGE_URL').$category->image  }}"
                                         data-src="{{ env('CATEGORY_IMAGE_URL').$category->image  }}" alt="Banner" class="lazyload">
                                    <span class="bnr-caption">
                                <span class="bnr-text-wrap">
                                    {{--<span class="bnr-text1"></span>--}}
                                    <span class="bnr-text2">{{ $category->name  }}</span>
                                    <span class="btn-decor bnr-btn">shop now<span class="btn-line"></span>
                                    </span>
                                </span>
                            </span>
                                </div>
                            </a>
                        </div>
                    @endforeach
                @endif

            </div>
        </div>
    </div>
    <div class="holder">
        <div class="container">
            <div class="title-with-right">
                <h2 class="h1-style">Customer Favorites</h2>
                <div class="prd-carousel-tabs js-filters-prd d-block d-md-flex" data-grid="tabCarousel-01">
                    <a href="{{ route('website-products-list',['type' =>  'customer_favorite', 'ids' => $customer_favorites->pluck('product_price_id')->toarray()]) }}"
                       class="btn-decor">shop all</a>
                </div>
            </div>
            @if(count($customer_favorites) > 0)
                <div class="prd-grid prd-text-center prd-carousel js-prd-carousel js-product-isotope-sm slick-arrows-squared data-to-show-3 data-to-show-md-3 data-to-show-sm-3 data-to-show-xs-2" data-slick='{"slidesToShow": 4, "slidesToScroll": 1, "responsive": [{"breakpoint": 992,"settings": {"slidesToShow": 3, "slidesToScroll": 1}},{"breakpoint": 768,"settings": {"slidesToShow": 3, "slidesToScroll": 1}},{"breakpoint": 480,"settings": {"slidesToShow": 2, "slidesToScroll": 1}}]}'>
                    @foreach($customer_favorites as $customer_favorite)
                        <div class="prd prd-has-loader prd-has-countdown">
                            <div class="prd-inside">
                                <div class="prd-img-area">
                                    <a href="{{ route('website-products-details',['slug'=> $customer_favorite->product_price->product->slug,'product_code'=> $customer_favorite->product_price->code])  }}" class="prd-img">
                                        <img src="{{ $customer_favorite->product_price->primary_image  }}" data-srcset="{{ $customer_favorite->product_price->primary_image  }}"
                                             alt="{{ $customer_favorite->product_price->product->name }}" class="js-prd-img"></a>
                                </div>
                                <div class="prd-info">
                                    <div class="prd-tag prd-hidemobile"><a href="#">{{ $customer_favorite->product_price->product->name  }}</a></div>
                                    <h2 class="prd-title"><a href="javascript:void(0);">{{  $customer_favorite->product_price->product->category->name }}</a></h2>
                                    <div class="prd-price">
                                        <div class="price-new">₹ {{ $customer_favorite->product_price->price  }}</div>
                                    </div>
                                    <div id="divReload">
                                        <a href="javascript:void(0);" onclick="addInWishList({{$customer_favorite->product_price->id}})">
                                            <i id="wishlist{{ $customer_favorite->product_price->id }}" class="{{ (!Session::get('user')) ? 'icon-heart ' : (\App\Models\UserWishList::where('user_id', Session::get('user')->id)->where('product_price_id',$customer_favorite->product_price->id)->exists() ? 'fa fa-heart text-danger '.$customer_favorite->product_price->id : 'icon-heart '.$customer_favorite->product_price->id) }}"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="prd-grid three-in-row prd-text-center prd-carousel js-prd-carousel slick-arrows-squared data-to-show-4 data-to-show-sm-3 data-to-show-xs-2" data-slick='{"slidesToShow": 4, "responsive": [{"breakpoint": 768,"settings": {"slidesToShow": 3}},{"breakpoint": 480,"settings": {"slidesToShow": 2}}]}'>
                    <div class="prd prd-has-loader">
                        <div class="prd-inside">
                            <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                    <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                            </div>
                            <div class="prd-info">
                                <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                <div class="prd-price">
                                    <div class="price-new">₹ 100.00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="prd prd-has-loader">
                        <div class="prd-inside">
                            <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                    <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                            </div>
                            <div class="prd-info">
                                <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                <div class="prd-price">
                                    <div class="price-new">₹ 100.00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="prd prd-has-loader">
                        <div class="prd-inside">
                            <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                    <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                            </div>
                            <div class="prd-info">
                                <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                <div class="prd-price">
                                    <div class="price-new">₹ 100.00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="prd prd-has-loader">
                        <div class="prd-inside">
                            <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                    <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                            </div>
                            <div class="prd-info">
                                <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                <div class="prd-price">
                                    <div class="price-new">₹ 100.00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="prd prd-has-loader">
                        <div class="prd-inside">
                            <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                    <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                            </div>
                            <div class="prd-info">
                                <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                <div class="prd-price">
                                    <div class="price-new">₹ 100.00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="prd prd-has-loader">
                        <div class="prd-inside">
                            <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                    <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                            </div>
                            <div class="prd-info">
                                <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                <div class="prd-price">
                                    <div class="price-new">₹ 100.00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="prd prd-has-loader">
                        <div class="prd-inside">
                            <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                    <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                            </div>
                            <div class="prd-info">
                                <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                <div class="prd-price">
                                    <div class="price-new">₹ 100.00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="prd prd-has-loader">
                        <div class="prd-inside">
                            <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                    <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                            </div>
                            <div class="prd-info">
                                <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                <div class="prd-price">
                                    <div class="price-new">₹ 100.00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <div class="holder">
        <div class="container">
            <div class="title-with-right">
                <h2 class="h1-style">Deals Of The Day</h2>
                <div class="prd-carousel-tabs js-filters-prd d-block d-md-flex" data-grid="tabCarousel-01">
                    <a href="{{ route('website-products-list',['type' =>  'deal']) }}" class="btn-decor">shop all</a>
                </div>
            </div>
            @if(count($deal_products) > 0)
                <div class="prd-grid prd-text-center prd-carousel js-prd-carousel js-product-isotope-sm slick-arrows-squared data-to-show-3 data-to-show-md-3 data-to-show-sm-3 data-to-show-xs-2" data-slick='{"slidesToShow": 4, "slidesToScroll": 1, "responsive": [{"breakpoint": 992,"settings": {"slidesToShow": 3, "slidesToScroll": 1}},{"breakpoint": 768,"settings": {"slidesToShow": 3, "slidesToScroll": 1}},{"breakpoint": 480,"settings": {"slidesToShow": 2, "slidesToScroll": 1}}]}'>
                    @foreach($deal_products as $deal_product)
                        <div class="prd prd-has-loader prd-has-countdown">
                            <div class="prd-inside">
                                <div class="prd-img-area">
                                    <a href="{{ route('website-products-details',['slug'=> $deal_product->product->slug,'product_code'=> $deal_product->code])  }}" class="prd-img">
                                        <img src="{{ $deal_product->primary_image  }}" data-srcset="{{ $deal_product->primary_image  }}"
                                             alt="{{ $deal_product->product->name }}" class="js-prd-img"></a>
                                </div>
                                <div class="prd-info">
                                    <div class="prd-tag prd-hidemobile"><a href="#">{{ $deal_product->product->name  }}</a></div>
                                    <h2 class="prd-title"><a href="javascript:void(0);">{{  $deal_product->product->category->name }}</a></h2>
                                    <div class="prd-price">
                                        <div class="price-new">₹ {{ $deal_product->price  }}</div>
                                    </div>
                                    <div id="divReload">
                                        <a href="javascript:void(0);" onclick="addInWishList({{$deal_product->id}})">
                                            <i id="wishlist{{ $deal_product->id }}" class="{{ (!Session::get('user')) ? 'icon-heart ' : (\App\Models\UserWishList::where('user_id', Session::get('user')->id)->where('product_price_id',$deal_product->id)->exists() ? 'fa fa-heart text-danger '.$deal_product->id : 'icon-heart '.$deal_product->id) }}"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="prd-grid three-in-row prd-text-center prd-carousel js-prd-carousel slick-arrows-squared data-to-show-4 data-to-show-sm-3 data-to-show-xs-2" data-slick='{"slidesToShow": 4, "responsive": [{"breakpoint": 768,"settings": {"slidesToShow": 3}},{"breakpoint": 480,"settings": {"slidesToShow": 2}}]}'>
                    <div class="prd prd-has-loader">
                        <div class="prd-inside">
                            <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                    <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                            </div>
                            <div class="prd-info">
                                <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                <div class="prd-price">
                                    <div class="price-new">₹ 100.00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="prd prd-has-loader">
                        <div class="prd-inside">
                            <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                    <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                            </div>
                            <div class="prd-info">
                                <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                <div class="prd-price">
                                    <div class="price-new">₹ 100.00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="prd prd-has-loader">
                        <div class="prd-inside">
                            <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                    <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                            </div>
                            <div class="prd-info">
                                <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                <div class="prd-price">
                                    <div class="price-new">₹ 100.00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="prd prd-has-loader">
                        <div class="prd-inside">
                            <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                    <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                            </div>
                            <div class="prd-info">
                                <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                <div class="prd-price">
                                    <div class="price-new">₹ 100.00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="prd prd-has-loader">
                        <div class="prd-inside">
                            <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                    <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                            </div>
                            <div class="prd-info">
                                <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                <div class="prd-price">
                                    <div class="price-new">₹ 100.00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="prd prd-has-loader">
                        <div class="prd-inside">
                            <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                    <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                            </div>
                            <div class="prd-info">
                                <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                <div class="prd-price">
                                    <div class="price-new">₹ 100.00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="prd prd-has-loader">
                        <div class="prd-inside">
                            <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                    <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                            </div>
                            <div class="prd-info">
                                <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                <div class="prd-price">
                                    <div class="price-new">₹ 100.00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="prd prd-has-loader">
                        <div class="prd-inside">
                            <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                    <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                            </div>
                            <div class="prd-info">
                                <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                <div class="prd-price">
                                    <div class="price-new">₹ 100.00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            {{--<div class="more-link-wrapper text-center">--}}
            {{--<a href="{{ route('website-products-list',['type' =>  'deal']) }}" class="btn-decor">shop all</a>--}}
            {{--</div>--}}
            {{--<div class="more-link-wrapper text-center"><a href="javascript:void(0);" class="btn-decor">shop all</a></div>--}}
        </div>
    </div>
    <div class="holder fullwidth full-nopad">
        <div class="container">
            <div class="holder">
                <div class="container">
                    <div class="title-with-right">
                        <h2 class="h1-style">Recommended Products</h2>
                        <div class="prd-carousel-tabs js-filters-prd d-block d-md-flex" data-grid="tabCarousel-01">
                            <a href="{{ route('website-products-list',['type' =>  'recommended']) }}" class="btn-decor">shop all</a>
                        </div>
                    </div>
                    @if(count($recommended_products) >  0)
                        <div class="prd-grid three-in-row prd-text-center prd-carousel js-prd-carousel slick-arrows-squared data-to-show-4 data-to-show-sm-3 data-to-show-xs-2" data-slick='{"slidesToShow": 4, "responsive": [{"breakpoint": 768,"settings": {"slidesToShow": 3}},{"breakpoint": 480,"settings": {"slidesToShow": 2}}]}'>
                            @foreach($recommended_products as $recommended_product)
                                <div class="prd prd-has-loader">
                                    <div class="prd-inside">
                                        <div class="prd-img-area">
                                            <a href="{{ route('website-products-details',['slug'=> $recommended_product->product->slug,'product_code'=> $recommended_product->code])  }}" class="prd-img">
                                                <img src="{{ $recommended_product->primary_image }}" data-srcset="{{ $recommended_product->primary_image }}"
                                                     alt="{{ $recommended_product->product->name }}" class="js-prd-img"></a>
                                        </div>
                                        <div class="prd-info">
                                            <div class="prd-tag prd-hidemobile"><a href="{{ route('website-products-details',['slug'=> $recommended_product->product->slug,'product_code'=> $recommended_product->code])  }}">{{ $recommended_product->product->name  }}</a></div>
                                            <h2 class="prd-title"><a href="javascript:void(0);">{{ $recommended_product->product->category->name  }}</a></h2>
                                            <div class="prd-price">
                                                <div class="price-new"> ₹ {{ $recommended_product->price  }}</div>
                                            </div>
                                            <div id="divReload">
                                                <a href="javascript:void(0);" onclick="addInWishList({{$recommended_product->id}})">
                                                    <i id="wishlist{{ $recommended_product->id }}" class="{{ (!Session::get('user')) ? 'icon-heart ' : (\App\Models\UserWishList::where('user_id', Session::get('user')->id)->where('product_price_id',$recommended_product->id)->exists() ? 'fa fa-heart text-danger '.$recommended_product->id : 'icon-heart '.$recommended_product->id) }}"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <div class="prd-grid three-in-row prd-text-center prd-carousel js-prd-carousel slick-arrows-squared data-to-show-4 data-to-show-sm-3 data-to-show-xs-2" data-slick='{"slidesToShow": 4, "responsive": [{"breakpoint": 768,"settings": {"slidesToShow": 3}},{"breakpoint": 480,"settings": {"slidesToShow": 2}}]}'>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 160.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 160.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 160.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 160.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 160.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 160.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 160.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 160.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    {{--<div class="more-link-wrapper text-center">--}}
                    {{--<a href="{{ route('website-products-list',['type' =>  'recommended']) }}" class="btn-decor">shop all</a>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>
    <div class="holder fullwidth full-nopad">
        <div class="container"><a href="#" class="bnr-wrap">
                <div class="bnr bnr34 bnr--style-6 bnr--left bnr--middle bnr-hover-scale" data-fontratio="11.4">
                    <img src="/website-assets/images/new_slider/banner.png" data-src="/website-assets/images/new_slider/banner.png" class="lazyload" alt="">
                    <span class="bnr-caption container">
                        {{--<span class="bnr-text-wrap">--}}
                        {{--<span class="bnr-text1">summer collections</span> <span class="bnr-text2">65% OFF</span>--}}
                        {{--<span class="btn-decor btn-decor--sm bnr-btn">shop now<span class="btn-line"></span></span></span>--}}
                    </span>
                </div>
            </a>
        </div>
    </div>
    <div class="holder fullwidth full-nopad">
        <div class="container">
            <div class="holder">
                <div class="container">
                    <div class="title-with-right">
                        <h2 class="h1-style">New Arrival Products</h2>
                        <div class="prd-carousel-tabs js-filters-prd d-block d-md-flex" data-grid="tabCarousel-01">
                            <a href="{{ route('website-products-list',['type' =>  'new_arrival']) }}" class="btn-decor">shop all</a>
                        </div>
                    </div>
                    @if(count($new_arrival_products) > 0)
                        <div class="prd-grid three-in-row prd-text-center prd-carousel js-prd-carousel slick-arrows-squared data-to-show-4 data-to-show-sm-3 data-to-show-xs-2" data-slick='{"slidesToShow": 4, "responsive": [{"breakpoint": 768,"settings": {"slidesToShow": 3}},{"breakpoint": 480,"settings": {"slidesToShow": 2}}]}'>
                            @foreach($new_arrival_products as $new_arrival_product)
                                <div class="prd prd-has-loader">
                                    <div class="prd-inside">
                                        <div class="prd-img-area">
                                            <a href="{{ route('website-products-details',['slug'=> $new_arrival_product->product->slug,'product_code'=> $new_arrival_product->code])  }}" class="prd-img">
                                                <img src="{{ $new_arrival_product->primary_image }}" data-srcset="{{ $new_arrival_product->primary_image }}"
                                                     alt="{{ $new_arrival_product->product->name }}" class="js-prd-img"></a>
                                        </div>
                                        <div class="prd-info">
                                            <div class="prd-tag prd-hidemobile"><a href="{{ route('website-products-details',['slug'=> $new_arrival_product->product->slug,'product_code'=> $new_arrival_product->code])  }}">{{ $new_arrival_product->product->name  }}</a></div>
                                            <h2 class="prd-title"><a href="javascript:void(0);">{{ $new_arrival_product->product->category->name  }}</a></h2>
                                            <div class="prd-price">
                                                <div class="price-new"> ₹ {{ $new_arrival_product->price  }}</div>
                                            </div>
                                            <div id="divReload">
                                                <a href="javascript:void(0);" onclick="addInWishList({{$new_arrival_product->id}})">
                                                    <i id="wishlist{{ $new_arrival_product->id }}" class="{{ (!Session::get('user')) ? 'icon-heart ' : (\App\Models\UserWishList::where('user_id', Session::get('user')->id)->where('product_price_id',$new_arrival_product->id)->exists() ? 'fa fa-heart text-danger '.$new_arrival_product->id : 'icon-heart '.$new_arrival_product->id) }}"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <div class="prd-grid three-in-row prd-text-center prd-carousel js-prd-carousel slick-arrows-squared data-to-show-4 data-to-show-sm-3 data-to-show-xs-2" data-slick='{"slidesToShow": 4, "responsive": [{"breakpoint": 768,"settings": {"slidesToShow": 3}},{"breakpoint": 480,"settings": {"slidesToShow": 2}}]}'>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 150.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 150.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 150.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 150.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 150.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 150.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 150.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prd prd-has-loader">
                                <div class="prd-inside">
                                    <div class="prd-img-area"><a href="javascript:void(0);" class="prd-img">
                                            <img src="/website-assets/images/static_products/1.png" data-srcset="/website-assets/images/static_products/1.png" alt="Product" class="js-prd-img lazyload"></a>
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-tag prd-hidemobile"><a href="javascript:void(0);">Ayurveda</a></div>
                                        <h2 class="prd-title"><a href="javascript:void(0);">Shatavari Granules</a></h2>
                                        <div class="prd-price">
                                            <div class="price-new">₹ 150.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    {{--<div class="more-link-wrapper text-center">--}}
                    {{--<a href="{{ route('website-products-list',['type' =>  'new_arrival']) }}" class="btn-decor">shop all</a>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>
    <div class="holder">
        <div class="container">
            <div class="title-with-arrows title-with-arrows--center">
                <h2 class="h1-style">Our Blogs</h2>
                <div class="carousel-arrows"></div>
            </div>
            <div class="post-prws post-prws-carousel" data-slick='{"slidesToShow": 4, "responsive": [{"breakpoint": 992,"settings": {"slidesToShow": 3}},{"breakpoint": 768,"settings": {"slidesToShow": 2}},{"breakpoint": 480,"settings": {"slidesToShow": 1}}]}'>
                @if(count($blog) > 0)
                    @foreach($blog as $item)
                        <div class="post-prw">
                            <a href="javascript:void(0);" class="post-img">
                                <img src="{{ env('BLOG_IMAGE_URL') . $item->image  }}" data-src="{{ env('BLOG_IMAGE_URL') . $item->image  }}" class="lazyload" alt="">
                            </a>
                            <h4 class="post-title"><a href="#">{{ $item->title  }}</a></h4>
                            <p class="post-teaser">{!! $item->description  !!}</p>
                            <div class="post-bot">
                                <div class="post-date">{{ $item->created_at->format('d M')  }}</div><a href="javascript:void(0);" class="post-link">read more</a>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="post-prw"><a href="javascript:void(0);" class="post-img"><img src="/website-assets/images/blog.jpeg" data-src="/website-assets/images/blog.jpeg" class="lazyload" alt=""></a>
                        <h4 class="post-title"><a href="#">Results for the month</a></h4>
                        <p class="post-teaser">This is your store’s blog. You can use it to talk about new product...</p>
                        <div class="post-bot">
                            <div class="post-date">09 dec</div><a href="javascript:void(0);" class="post-link">read more</a>
                        </div>
                    </div>
                    <div class="post-prw"><a href="javascript:void(0);" class="post-img"><img src="/website-assets/images/blog.jpeg" data-src="/website-assets/images/blog.jpeg" class="lazyload" alt=""></a>
                        <h4 class="post-title"><a href="#">How to choose a protein</a></h4>
                        <p class="post-teaser">This is your store’s blog. You can use it to talk about new product...</p>
                        <div class="post-bot">
                            <div class="post-date">10 dec</div><a href="javascript:void(0);" class="post-link">read more</a>
                        </div>
                    </div>
                    <div class="post-prw"><a href="javascript:void(0);" class="post-img"><img src="/website-assets/images/blog.jpeg" data-src="/website-assets/images/blog.jpeg" class="lazyload" alt=""></a>
                        <h4 class="post-title"><a href="#">Training schedule</a></h4>
                        <p class="post-teaser">This is your store’s blog. You can use it to talk about new product...</p>
                        <div class="post-bot">
                            <div class="post-date">11 dec</div><a href="javascript:void(0);" class="post-link">read more</a>
                        </div>
                    </div>
                    <div class="post-prw"><a href="javascript:void(0);" class="post-img"><img src="/website-assets/images/blog.jpeg" data-src="/website-assets/images/blog.jpeg" class="lazyload" alt=""></a>
                        <h4 class="post-title"><a href="#">The right diet</a></h4>
                        <p class="post-teaser">This is your store’s blog. You can use it to talk about new product...</p>
                        <div class="post-bot">
                            <div class="post-date">12 dec</div><a href="javascript:void(0);" class="post-link">read more</a>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    @if($popup)
        <div id="popUp" class="modal fade in" data-toggle="modal" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-md" >
                <div class="modal-content">
                    <div class="modal-body" style="width: 100%;">
                        <img src="{{ env('POPUP_IMAGE_URL').$popup->image }}" alt="" style="width: 100%">
                    </div>
                    <div class="modal-footer mt-3">
                        <button type="button" class="btn btn-outline-success btn-sm" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    @endif
@stop
@section('page-javascript')
    <script>
        $(window).ready(function modal(){
            $('#popUp').modal();
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        function addInWishList(product_price_id) {
            var user = "{{ \Session::get('user')  }}";
            if(!user){
                toastr.warning("please login for add product in wish list","Warning..!!");
            }else{
                $.ajax({
                    url:"{{ route('website-user-add-remove-wish-list')  }}",
                    type:"POST",
                    dataType:"json",
                    data:{ 'product_price_id': product_price_id },
                    success:function(response) {
                        if (response.status === true) {
                            if (response.type === 'add') {
                                $("#wishlist" + product_price_id).removeClass('icon-heart ' + product_price_id).addClass('fa fa-heart text-danger ' + product_price_id);
                                toastr.success(response.message, "Success..!!");
                            }
                            else {
                                $("#wishlist"+product_price_id).removeClass('fa fa-heart text-danger ' + product_price_id).addClass('icon-heart ' + product_price_id);
                                toastr.error(response.message, "Remove..!!");
                            }
                        }
                    }
                });
            }
        }
    </script>
@stop


