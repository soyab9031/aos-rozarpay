@extends('website.template.layout')
@section('page-title','Product')
@section('page-content')
    <!-- breadcrumb start -->
    <div class="holder mt-0">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="{{ route('website-home')  }}">Home</a></li>
                <li><span>Shop</span></li>
                {{--<li><a href="{{route('website-products-view', ['slug' => $parent_category->slug])}}"><span>{{ $parent_category->name }}</span></a></li>--}}
                {{--<li><span>{{ Request::get('sub') }}</span></li>--}}
            </ul>
        </div>
    </div>
    <!-- breadcrumb End -->

    <div class="holder mt-0">
        <div class="container">
            @if($product_prices)
                @if($parent_category->image)
                    <div class="col-12">
                        <img class="img-fluid" width="1200" src="{{ env('CATEGORY_IMAGE_URL') .$parent_category->image }}">
                    </div>
                @endif
            @endif
            <div class="page-title text-center d-none d-lg-block mt-5">
                <div class="title">
                    <h1>{{ $parent_category->name  }}</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 aside aside--left fixed-col js-filter-col">
                    <div class="fixed-col_container">
                        <div class="filter-close">CLOSE</div>
                        <div class="sidebar-block sidebar-block--mobile d-block d-lg-none">
                            <div class="d-flex align-items-center">
                                <div class="selected-label">(6) FILTER</div>
                                <div class="selected-count ml-auto">SELECTED <span><b>25 items</b></span></div>
                            </div>
                        </div>
                        <div class="sidebar-block filter-group-block collapsed open">
                            <div class="sidebar-block_title">
                                <span>{{ $parent_category->name }}</span>
                                <div class="toggle-arrow"></div>
                            </div>
                            <div class="sidebar-block_content">
                                <ul class="category-list">
                                    @foreach($categories_lists as $child_category)
                                        <li>
                                            <a href="{{route('website-products-view', ['slug' => $parent_category->slug,
                                            'sub' => $child_category->slug])}}">
                                                {{$child_category->name}}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Left column -->
                <!-- Center column -->
                <div class="col-lg aside" id="productListPage">
                    <div class="holder">
                        <div class="container">
                            <div class="row">
                                @if(count($product_prices) > 0)
                                    <div class="col-md-4" v-for="item of items">
                                        <div class="prd prd-has-loader prd-has-countdown bg-grey">
                                            <div class="prd-inside">
                                                <div class="prd-img-area">
                                                    <a :href="'/product-details/' + item.product.slug + '/' + item.code" class="prd-img">
                                                        <img :src="item.primary_image" :alt="item.product.name" class="js-prd-img lazyload">
                                                    </a>
                                                    <div class="countdown-box">
                                                        <div class="countdown js-countdown" data-countdown="2019/12/31"></div>
                                                    </div>
                                                    <div class="gdw-loader"></div>
                                                </div>
                                                <div class="prd-info text-center">
                                                    <h2 class="prd-title">
                                                        <a :href="'/product-details/' + item.product.slug + '/' + item.code">
                                                            <h3 class="website-font mb-0">
                                                                @{{ item.product.name | str_limit(30) }}
                                                            </h3>
                                                        </a>
                                                    </h2>
                                                    <div class="">
                                                        <div class="price-new"> @{{ item.price | currency }}</div>
                                                        {{--<div class="price-old">$ 70.00</div>--}}
                                                    </div>
                                                    <div>
                                                        <a href="javascript:void(0)"
                                                           class="btn mt-1"
                                                           @click="addToCart(item.id, 1)"
                                                           v-if="item.quantity.current == 0">
                                                            <i class="icon icon-handbag"></i> Add To Cart</a>
                                                        <div class="prd-links">
                                                            <a href="javascript:void(0)" @click="addInWishList(item.id)">
                                                                <i v-if="item.wishlist_exists" class="fa fa-heart text-danger"></i>
                                                                <i v-if="!item.wishlist_exists" class="icon-heart"></i>
                                                            </a>
                                                        </div>
                                                        <div v-if="item.quantity.current > 0">
                                                            <vue-counter-button v-model="item.quantity" min="0"
                                                                                max="100" :price-id="item.id"
                                                                                @change-counter-btn="changeCounterBtn($event)">
                                                            </vue-counter-button>
                                                            <a href="{{ route('website-cart') }}" class="btn mt-1">
                                                                <i class="icon icon-handbag"></i>  View CART
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="col-md-12 text-center">
                                        <div class="logo mb-4">
                                            <a href="{{ route('website-home') }}">
                                                <img src="/user-assets/images/company/logo.png" width="100" alt="" class="img-fluid">
                                            </a>
                                        </div>
                                        <h4 class="mb-3">
                                            Products Not Available Right Now...!!
                                        </h4>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Center column -->
            </div>
        </div>
    </div>
@stop
@section('page-javascript')
    <script>
        Vue.prototype.$http = axios;
        new Vue({
            el: '#productListPage',
            data: {
                isActive: false,
                items: {!! json_encode($items) !!},
                accountLogin: {!! $accountLogin !!},
            },
            methods: {
                addInWishList: function (product_price_id) {

                    if (!this.accountLogin) {
                        toastr.warning("please login for add product in wish list", "Warning..!!");
                        return false;
                    }

                    INGENIOUS.blockUI(true);

                    this.$http.get('{{ route('website-user-add-remove-wish-list') }}', {
                        params: {
                            product_price_id: product_price_id,
                        }
                    }).then(response => {

                        INGENIOUS.blockUI(false);

                        if (response.data.status) {

                            _.map(this.items, item => {

                                if (parseInt(item.id) === parseInt(product_price_id)) {
                                    item.wishlist_exists = !item.wishlist_exists;
                                }

                            });

                            toastr.success(response.data.message, "Success..!!");

                        }

                    });
                },
                changeCounterBtn: function (event) {

                    if (event.action === 'increment') {
                        this.addToCart(event.product_price_id, event.current);
                    } else if (event.action === 'decrement') {
                        this.removeFromCart(event.product_price_id, event.current);
                    }
                },
                addToCart: function (item_id, qty) {

                    INGENIOUS.blockUI(true);

                    this.$http.get('{{route('website-cart-add-item')}}', {
                        params: {
                            product_price_id: item_id,
                            qty: qty
                        }
                    }).then(response => {

                        INGENIOUS.blockUI(false);

                    if (!response.data.status) {
                        swal("Oops", response.data.message, "error");
                    }

                    _.map(this.items, item => {
                        if (parseInt(item.id) === parseInt(item_id)) {
                        item.quantity.current = qty;
                    }
                });

                });
                },
                removeFromCart: function (product_price_id, quantity) {

                    INGENIOUS.blockUI(true);

                    this.$http.get('{{ route('website-cart-remove-item') }}', {
                        params: {
                            product_price_id: product_price_id,
                            qty: quantity,
                        }
                    }).then(response => {

                        INGENIOUS.blockUI(false);

                    if (!response.data.status) {
                        swal("Oops", response.data.message, "error");
                    }

                    _.map(this.items, item => {
                        if (parseInt(item.id) === parseInt(product_price_id)) {
                        item.quantity.current = quantity;
                    }
                });
                });
                }
            }
        })
    </script>
@stop