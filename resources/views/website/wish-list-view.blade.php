@extends('website.template.layout')
@section('page-title','WishList')
@section('page-content')
    <div class="holder mt-0">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="{{ route('website-home')  }}">Home</a></li>
                <li><span>Wishlist</span></li>
            </ul>
        </div>
    </div>
    <div class="holder mt-0">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-12">
                    <div class="table-responsive wishlist_table">
                        <table class="table wishListTable">
                            <thead>
                            <tr>
                                <th class="product-thumbnail">Image</th>
                                <th class="product-name">Product</th>
                                <th class="product-price">Price</th>
                                <th class="product-add-to-cart"></th>
                                <th class="product-remove">Remove</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($userWishList as $item)
                                <tr id="wishList{{ $item->id  }}">
                                    <td class="product-thumbnail"><a href="javascript:void(0);"><img src="{{ $item->product_price->primary_image  }}" alt="product1"></a></td>
                                    <td class="product-name" data-title="Product"><a href="#">{{ $item->product_price->product->name  }}</a></td>
                                    <td>
                                        <span class="price"><i class="fas fa-rupee-sign"></i>{{ $item->product_price->distributor_price  }}</span>
                                        <del><i class="fas fa-rupee-sign"></i> {{ $item->product_price->price  }}</del>
                                    </td>
                                    <td class="product-price" data-title="Price"><i class="fa fa-rupee-sign"></i> </td>
                                    <td class="product-add-to-cart"><a href="{{ route('website-products-details',['slug' => $item->product_price->product->slug,'product_code' => $item->product_price->code]) }}" class="btn btn-fill-out"><i class="icon-basket-loaded"></i> Add to Cart</a></td>
                                    <td class="product-remove" data-title="Remove">
                                        <a href="javascript:void(0);" onclick="removeWishList({{ $item->id  }})"><i class="ti-close"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('page-javascript')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        function removeWishList(wishListId)
        {
            $.ajax({
                url:"{{ route('website-user-add-remove-wish-list')  }}",
                type:"POST",
                dataType:"JSON",
                data:{
                    'wishListId' : wishListId
                },
                success:function(response){
                    toastr.success('Remove wishList','Success..!!');
                    $('#wishList'+wishListId).remove();
                }
            });
        }
    </script>
@stop