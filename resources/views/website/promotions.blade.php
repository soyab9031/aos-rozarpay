@extends('website.template.layout')
@section('page-title','Promotions')
@section('page-content')
    <div class="holder mt-0">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="{{ route('website-home')  }}">Home</a></li>
                <li><span>Promotions</span></li>
            </ul>
        </div>
    </div>
    <div class="holder mt-0">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @if($promotion == '')
                        <div class="about">
                            <div class="about-image">
                                <img src="{{ env("WEB_CONTENT_IMAGE_URL").$promotion->image }}" alt="" class="center">
                            </div>
                            <div class="company-content">
                                <p class="text-justify">{!! $promotion->description !!}</p>
                            </div>
                        </div>
                    @else
                        <h2 class="h1-style text-center">Coming Soon..!</h2>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop