@php
    $header_categories = \App\Models\Category::active()->whereType(1)->whereNull('parent_id')->with(['children' => function ($q){ $q->active(); }])->get();
    $header_services = \App\Models\Category::active()->whereType(2)->whereNull('parent_id')->with(['children' => function ($q){ $q->active(); }])->get();
@endphp
<header class="hdr global_width hdr-style-6 hdr_sticky hdr-mobile-style2">
    <!-- Mobile Menu -->
    <div class="mobilemenu js-push-mbmenu">
        <div class="mobilemenu-content">
            <div class="mobilemenu-close mobilemenu-toggle">CLOSE</div>
            <div class="mobilemenu-scroll">
                <div class="mobilemenu-search"></div>
                <div class="nav-wrapper show-menu">
                    <div class="nav-toggle"><span class="nav-back"><i class="icon-arrow-left"></i></span> <span class="nav-title"></span></div>
                    <ul class="nav nav-level-1">
                        @if(count($header_categories) == 0)
                            <li><a href="javascript:void(0);" title="">Shop</a><span class="arrow"></span></li>
                        @else
                            <li><a href="javascript:void(0);" title="">Shop</a><span class="arrow"></span>
                                <ul class="nav-level-2">
                                    @foreach($header_categories as $header_category)
                                        <li><a href="{{route('website-products-view', ['slug' => $header_category->slug])}}" title="">{{  $header_category->name  }}</a><span class="arrow"></span>
                                            @if(count($header_category->children) > 0)
                                                <ul class="nav-level-3">
                                                    @foreach($header_category->children as $child)
                                                        <li><a href="{{route('website-products-view', ['slug' => $header_category->slug,'sub' => $child->slug])}}" title="">{{ $child->name  }}</a></li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        @endif
                        @if(count($header_services) == 0)
                            <li><a href="javascript:void(0);" title="">Services</a><span class="arrow"></span></li>
                        @else
                            <li><a href="javascript:void(0);" title="">Services</a><span class="arrow"></span>
                                <ul class="nav-level-2">
                                    @foreach($header_services as $header_service)
                                        <li><a href="{{route('website-products-view', ['slug' => $header_service->slug])}}" title="">{{  $header_service->name  }}</a><span class="arrow"></span>
                                            @if(count($header_service->children) > 0)
                                                <ul class="nav-level-3">
                                                    @foreach($header_service->children as $child)
                                                        <li><a href="{{route('website-products-view', ['slug' => $header_service->slug,'sub' => $child->slug])}}" title="">{{ $child->name  }}</a></li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        @endif
                        <li><a href="javascript:void(0);" title="">Promotions</a><span class="arrow"></span></li>
                        <li><a href="{{ route('website-company-about')  }}">About Aos LifeStyle</a><span class="arrow"></span></li>
                        <li><a href="{{ route('website-business-opportunity')  }}" title="">Start A Business</a><span class="arrow"></span>
                            <ul class="nav-level-2">
                                <li><a href="{{ route('website-business-opportunity')  }}" title="">Aos Business Opportunity</a><span class="arrow"></span></li>
                                <li><a href="{{ route('user-login')  }}" title="">Become A Direct Retailer</a><span class="arrow"></span></li>
                                <li><a href="{{ route('customer-login') }}" title="">Become A Customer</a><span class="arrow"></span></li>
                            </ul>
                        </li>
                        <li><a href="{{ route('website-company-outlet-price') }}">Outlet</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- /Mobile Menu -->
    <div class="hdr-mobile show-mobile">
        <div class="hdr-content">
            <div class="container">
                <!-- Menu Toggle -->
                <div class="menu-toggle"><a href="#" class="mobilemenu-toggle"><i class="icon icon-menu"></i></a></div>
                <!-- /Menu Toggle -->
                <div class="logo-holder"><a href="{{ route('website-home')  }}" class="logo"><img src="/user-assets/images/company/logo.png" srcset="/user-assets/images/company/logo.png" alt=""></a></div>
                <div class="hdr-mobile-right">
                    <div class="hdr-topline-right links-holder"></div>
                    <div class="minicart-holder"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="hdr-desktop hide-mobile">
        <div class="hdr-topline hdr-topline--dark">
            <div class="container">
                <div class="row">
                    <div class="col-auto hdr-topline-left">
                        <div class="custom-text">Welcome You to Aos LifeStyle!</div>
                    </div>
                    <div class="col hdr-topline-center"></div>
                    <div class="col-auto hdr-topline-right links-holder">
                        <div class="dropdn dropdn_language">
                            <div class="dropdn dropdn_caret ml-1">
                                <div class="custom-text">
                                    <a href="https://api.whatsapp.com/send?phone=+9163708681900&text=hello" target="_blank"><i class="fa fa-whatsapp my-float"></i>
                                        <b>+91 6370868190</b></a>
                                </div>
                            </div>
                        </div>
                        <div class="dropdn dropdn_search hide-mobile hide-desktop  @@classes">
                            <a href="#" class="dropdn-link"><i class="icon icon-search2"></i><span>Search</span></a>
                            <div class="dropdn-content">
                                <div class="container">
                                    {{--mobile--}}
                                    <button type="button" class="search-button" onclick="productSearch()">
                                        <i class="icon-search2"></i></button>
                                    <input type="text" id="search-data" class="search-input typeahead" placeholder="search keyword"
                                           autocomplete="off">
                                </div>
                            </div>
                        </div>
                        @if(Session::has('user'))
                            <div class="dropdn_account show-mobile @@classes"><a href="{{ route('user-dashboard')  }}" class="dropdn-link">
                                    <i class="icon icon-person"></i><span>My Account</span></a>
                            </div>
                        @elseif(Session::has('customer'))
                            <div class="dropdn_account show-mobile @@classes"><a href="{{ route('customer-dashboard')  }}" class="dropdn-link">
                                    <i class="icon icon-person"></i><span>My Account</span></a>
                            </div>
                        @else
                            <div class="dropdn_account show-mobile @@classes"><a href="{{ route('website-login-info')  }}" class="dropdn-link">
                                    <i class="icon icon-person"></i><br/><span>User Sign Up</span></a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="hdr-content hide-mobile">
            <div class="container">
                <div class="row">
                    <div class="col logo-holder"><a href="{{  route('website-home') }}" class="logo"><img src="/user-assets/images/company/logo.png" srcset="/user-assets/images/company/logo.png" alt=""></a></div>
                    <div class="col search-holder">
                        <!-- Header Search website-->
                        <button type="button" class="search-button" onclick="productSearchDesktop()">
                            <i class="icon-search2"></i></button>
                        <input type="text" id="search-data-desktop" class="search-input typeahead" placeholder="search keyword"
                               autocomplete="off">
                        <!-- /Header Search -->
                    </div>
                    <div class="col-auto custom-col">
                        @if(Session::has('user'))
                            <a href="{{ route('user-dashboard')  }}" class="dropdn-link">
                                <i class="icon icon-person"></i><span>My Account</span>
                            </a>
                        @elseif(Session::has('customer'))
                            <a href="{{ route('customer-dashboard')  }}" class="dropdn-link">
                                <i class="icon icon-person"></i><span>My Account</span>
                            </a>
                        @else
                            <a href="{{ route('website-login-info')  }}" class="dropdn-link">
                                <i class="icon icon-person"></i><br/><span>Login/ Sign Up</span>
                            </a>
                        @endif

                    </div>
                    <div class="col-auto minicart-holder">
                        <div class="minicart">
                            <a href="{{ route('website-cart') }}" class="cart-btn">
                                <i class="icon icon-handbag" style="font-size: 22px" ></i>
                                <span class="cart-counter">{{ Session::get('cart') ? count(Session::get('cart')) : 0 }}</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="nav-holder">
            <div class="hdr-nav">
                <div class="container">
                    <!--mmenu-->
                    <ul class="mmenu mmenu-js mmenu-list">
                        @if(count($header_categories) == 0)
                            <li class="mmenu-item--simple"><a href="javascript:void(0);" title="">Shop</a></li>
                        @else
                            <li class="mmenu-item--simple"><a href="#" title="">Shop</a>
                                <div class="mmenu-submenu">
                                    <ul class="submenu-list">
                                        @foreach($header_categories as $header_category)
                                            <li><a href="{{route('website-products-view', ['slug' => $header_category->slug])}}" title="">{{ $header_category->name  }}</a>
                                                @if(count($header_category->children) > 0)
                                                    <ul>
                                                        @foreach($header_category->children as $child)
                                                            <li><a href="{{route('website-products-view', ['slug' => $header_category->slug,'sub' => $child->slug])}}" title="">{{ $child->name  }}</a></li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </li>
                        @endif
                        @if(count($header_services) == 0)
                            <li class="mmenu-item--simple"><a href="javascript:void(0);" title="">Services</a></li>
                        @else
                            <li class="mmenu-item--simple"><a href="#" title="">Services</a>
                                <div class="mmenu-submenu">
                                    <ul class="submenu-list">
                                        @foreach($header_services as $header_service)
                                            <li><a href="{{route('website-products-view', ['slug' => $header_service->slug])}}" title="">{{ $header_service->name  }}</a>
                                                @if(count($header_service->children) > 0)
                                                    <ul>
                                                        @foreach($header_service->children as $child)
                                                            <li><a href="{{route('website-products-view', ['slug' => $header_service->slug,'sub' => $child->slug])}}" title="">{{ $child->name  }}</a></li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </li>
                        @endif
                        <li class="mmenu-item--simple"><a href="{{ route('website-promotions')  }}" title="">Promotions</a></li>
                        <li class="mmenu-item--mega"><a href="{{ route('website-company-about')  }}">About Aos LifeStyle</a></li>
                        <li class="mmenu-item--simple"><a href="javascript:void(0);" title="">Start A Business</a>
                            <div class="mmenu-submenu">
                                <ul class="submenu-list">
                                    <li><a href="{{ route('website-business-opportunity')  }}">Aos Business Opportunity</a></li>
                                    <li><a href="{{ route('user-login')  }}">Become A Direct Retailer</a></li>
                                    <li><a href="{{ route('customer-login')  }}">Become A Customer</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="mmenu-item--mega"><a href="{{ route('website-company-outlet-price') }}">Outlet</a></li>
                    </ul>
                    <!--/mmenu-->
                </div>
            </div>
        </div>
    </div>
    <div class="sticky-holder compensate-for-scrollbar">
        <div class="container">
            <div class="row"><a href="#" class="mobilemenu-toggle show-mobile"><i class="icon icon-menu"></i></a>
                <div class="col-auto logo-holder-s"><a href="{{ route('website-home')  }}" class="logo"><img src="/user-assets/images/company/logo.png" srcset="/user-assets/images/company/logo.png" alt=""></a></div>
                <!--navigation-->
                <div class="prev-menu-scroll icon-angle-left prev-menu-js"></div>
                <div class="nav-holder-s"></div>
                <div class="next-menu-scroll icon-angle-right next-menu-js"></div>
                <!--//navigation-->
                <div class="col-auto minicart-holder-s"></div>
            </div>
        </div>
    </div>
</header>