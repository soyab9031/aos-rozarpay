@extends('website.template.layout')
@section('page-title','Login')
@section('page-content')
    <div class="page-content">
        <div class="holder mt-0">
            <div class="container">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('website-home') }}">Home</a></li>
                    <li><span>Login</span></li>
                </ul>
            </div>
        </div>
        <div class="holder mt-0">
            <div class="container">
                <div class="row justify-content-around">
                    <div class="col-sm-6 col-md-4">
                        <div id="loginForm">
                            <h2 class="text-center">SIGN IN</h2>
                            <div class="form-wrapper">
                                @if(session('error'))
                                    <div class="alert alert-danger">{{ session('error') }}</div>
                                @elseif(session('errors'))
                                    <div class="alert alert-danger">{{ session('errors')->first() }}</div>
                                @elseif (session('success'))
                                    <div class="alert alert-success"> {{ session('success') }}</div>
                                @else
                                @endif
                                <p>If you have an account with us, please log in.</p>
                                <form action="" method="post" novalidate>
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="user_credential" placeholder="Enter Credential">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control passwordInput" name="password"
                                               placeholder="Password" id="password">
                                        <a toggle="#password" class="field-icon toggle-password"
                                           onclick="INGENIOUS.showPassword(this)">
                                            <i class="fa fa-eye-slash"></i>
                                        </a>
                                    </div>
                                    <p class="text-uppercase">
                                        <a href="{{ route('user-forget-password') }}" class="js-toggle-forms pull-right">Forgot Your Password?</a>
                                    </p>
                                    <button type="submit" class="btn ">Sign in</button>
                                </form>
                            </div>
                        </div>
                        <div id="recoverPasswordForm" class="d-none">
                            <h2 class="text-center">RESET YOUR PASSWORD</h2>
                            <div class="form-wrapper">
                                <p>Enter the username and the Mobile Number to get new password</p>
                                <form action="{{ route('user-forget-password') }}" method="post" novalidate>
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="user_credential"
                                               placeholder="Enter Your Credential" value="{{ old('user_credential') }}" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="mobile" placeholder="Enter Your Mobile Number" required>
                                    </div>
                                    <div class="btn-toolbar">
                                        <a href="{{ route('user-login') }}" class="btn btn--alt js-toggle-forms">Cancel</a>
                                        <button class="btn ml-1" type="submit">Submit</button></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-divider"></div>
                    <div class="col-sm-6 col-md-4 mt-3 mt-sm-0">
                        <h2 class="text-center">REGISTER</h2>
                        <div class="form-wrapper">
                            <p>By creating an account with our store,
                                you will be able to move through the checkout process faster, store multiple shipping addresses,
                                view and track your orders in your account and more.</p>
                            <a href="{{ route('user-register') }}" class="btn">create an account</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop