@extends('website.template.layout')
@section('page-title', 'Member / User Details for Registration')
@section('page-content')
    <div class="page-content">
        <div class="holder mt-0">
            <div class="container">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('website-home') }}">Home</a></li>
                    <li><a href="{{ route('user-login') }}">Login</a></li>
                    <li><span>Registration</span></li>
                </ul>
            </div>
        </div>
        <div class="holder mt-0">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 aside aside--left">
                        <div class="list-group">
                            <a class="list-group-item">01. Member Details</a>
                            <a class="list-group-item" disabled>02. Overview</a>
                            <a class="list-group-item" disabled>03. Registration Completed</a>
                        </div>
                    </div>
                    <div class="col-md-9 aside">
                        <form action="" method="post" onsubmit="INGENIOUS.blockUI(true)">
                            {{ csrf_field() }}
                            <h2>Member Details</h2>
                            @if(session('error'))
                                <div class="alert alert-danger">{{ session('error') }}</div>
                            @elseif(session('errors'))
                                <div class="alert alert-danger">{{ session('errors')->first() }}</div>
                            @endif
                            <div class="row">
                                <div class="col-md-6">
                                    <fieldset class="form-group ">
                                        @if(isset($sponsor_upline))
                                            <input type="text" name="tracking_id" class="form-control square"
                                                   value="{{ $sponsor_upline ? $sponsor_upline->tracking_id : old('tracking_id')}}">
                                        @else
                                            <input type="text" class="form-control square" onchange="searchUser(this)"
                                                   name="tracking_id" placeholder="Enter Sponsor Upline Tracking ID"
                                                   value="{{ $sponsor_upline ? $sponsor_upline->tracking_id : old('tracking_id')}}" autocomplete="off">
                                            <span class="text-danger font-small-2">*If Sponsor upline ID is not available, kindly enter '100000'</span>
                                        @endif

                                    </fieldset>
                                </div>
                                <div class="col-md-6">
                                    <span class="text-center" id="sponsorParent"></span>
                                </div>
                            </div>
                            <div class="row" id="legs">
                                <div class="col-12 text-center">
                                    <div class="radio radio-danger mt-1" {{ isset($selected_leg) ? "style=pointer-events:none;" : ''}}>
                                        <input type="radio" value="L" name="leg"
                                               id="leftLeg" {{ old('leg') == 'L' || $selected_leg == 'L' ? 'checked' : 'checked' }}>
                                        <label for="leftLeg">Left Team</label>
                                        <input type="radio" value="R" name="leg"
                                               id="rightLeg" {{ old('leg') == 'R' || $selected_leg == 'R' ? 'checked' : null }}>
                                        <label for="rightLeg">Right Team</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <fieldset class="form-group">
                                        <label>Title</label>
                                        <select name="title" class="form-control form-control-bg square">
                                            <option value="Mr" {{ old('title') == 'Mr' ? 'selected' : null }}>Mr
                                            </option>
                                            <option value="Miss" {{ old('title') == 'Miss' ? 'selected' : null }}>Miss
                                            </option>
                                            <option value="Mrs" {{ old('title') == 'Mrs' ? 'selected' : null }}>Mrs
                                            </option>
                                            <option value="Mrs" {{ old('title') == 'Ms' ? 'selected' : null }}>Ms
                                            </option>
                                            <option value="Dr" {{ old('title') == 'Dr' ? 'selected' : null }}>Dr
                                            </option>
                                            <option value="Sri" {{ old('title') == 'Sri' ? 'selected' : null }}>Sri
                                            </option>
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="col-md-5">
                                    <fieldset class="form-group">
                                        <label>First Name</label>
                                        <input type="text" class="form-control form-control-bg square" name="first_name"
                                               placeholder="First Name" value="{{ old('first_name') }}"
                                               autocomplete="off">
                                    </fieldset>
                                </div>
                                <div class="col-md-5">
                                    <fieldset class="form-group">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control form-control-bg square" name="last_name"
                                               placeholder="Last Name" value="{{ old('last_name') }}"
                                               autocomplete="off">
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control form-control-bg square" name="email" placeholder="Email"
                                               value="{{ old('email') }}" autocomplete="off" required>
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <label>Mobile Number</label>
                                        <input type="text" class="form-control form-control-bg square"
                                               onkeypress="INGENIOUS.numericInput(event)" name="mobile"
                                               value="{{ old('mobile') }}" placeholder="10 Digit Mobile Number"
                                               autocomplete="new-mobile" required>
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <label>Date of Birth</label>
                                    <div class="input-group">
                                        <input type="text" name="birth_date" id="BirthDate" class="form-control form-control-bg"
                                               value="{{ old('birth_date') }}" placeholder="Date of Birth" readonly>
                                        <div class="input-group-append">
										<span class="input-group-text btn-bg-gradient-x-website-red">
											<span class="la la-calendar-o"></span>
										</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="font-medium-4 text-center mb-1">
                                        Account Details
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <fieldset class="form-group">
                                        <label>Aadhaar Card Number</label>
                                        <input type="text" class="form-control square form-control-bg"
                                               onkeypress="INGENIOUS.numericInput(event)" name="aadhaar_number"
                                               value="{{ old('aadhaar_number') }}" placeholder="Please Enter Aadhaar Card Number"
                                               autocomplete="off" required>
                                    </fieldset>
                                </div>
                                <div class="col-md-6">
                                    <fieldset class="form-group">
                                        <label>Username</label>
                                        <input type="text" class="form-control square form-control-bg" name="username"
                                               value="{{ old('username') }}" placeholder="Enter Username">
                                    </fieldset>
                                </div>
                                <div class="col-md-6">
                                    <label>Password</label>
                                    <div class="input-group">
                                        <input type="password" class="form-control passwordInput form-control-bg" name="password"
                                               placeholder="Enter Password">
                                        <div class="input-group-append">
                                            <button class="btn btn-bg-gradient-x-website-red" type="button"
                                                    onclick="INGENIOUS.showPassword(this)">
                                                <i class="fa fa-eye-slash"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Confirm Password</label>
                                    <div class="input-group">
                                        <input type="password" id="password" class="form-control passwordInput form-control-bg"
                                               name="password_confirmation" placeholder="Enter Confirm Password">
                                        <div class="input-group-append">
                                            <button class="btn btn-bg-gradient-x-website-red" type="button"
                                                    onclick="INGENIOUS.showPassword(this)">
                                                <i class="fa fa-eye-slash"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mt-1">
                                    <div class="checkbox check-danger">
                                        <input type="checkbox" id="terms" name="terms" required>
                                        <label for="terms">
                                            I agree to the Terms and Conditions and Privacy Policy
                                            For {{ config('project.company') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="text-right">
                                <button type="submit" class="btn btn-sm ">Continue</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('page-javascript')
    <script>

        @if(Request::get('referral'))
        $(function () {
            @php
            $referral_link = explode('leg',base64_decode(strtr(Request::get('referral'), '-_,', '+/=')));
            $tracking_id = $referral_link[0];
            @endphp
            getUser('{{ $tracking_id }}');
        });
        $('#legs').addClass('d-none');
        @endif

        @if(old('tracking_id'))
        $(function () {
            getUser('{{ old('tracking_id') }}');
        });
        @endif

        function searchUser(element) {
            getUser($(element).val());
        }

        function getUser(tracking_id) {

            let sponsorParent = $('#sponsorParent');

            $.get('{{ route("user-register-get-user") }}', {'tracking_id': tracking_id}, function (response) {

                sponsorParent.html('');

                if (!response.status) {
                    swal('Oops', response.message, 'warning');
                    return false;
                }

                sponsorParent.html('<div class="bg-primary text-white font-weight-bold rounded p-1 cart">' + response.user.name + '</div>');

            });
        }
    </script>
    <script>
        $('#BirthDate').datepicker({
            autoclose: true,
            format: "dd-mm-yyyy",
            startDate: "-99y",
            endDate: "-18y"
        });
    </script>
@stop