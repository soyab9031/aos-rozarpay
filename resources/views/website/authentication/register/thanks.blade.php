@extends('website.template.layout')
@section('page-title', 'Registration Completed at')
@section('page-content')
    <div class="page-content">
        <div class="holder mt-0">
            <div class="container">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('website-home') }}">Home</a></li>
                    <li><a href="{{ route('user-login') }}">Login</a></li>
                    <li><span>Registration</span></li>
                </ul>
            </div>
        </div>
        <div class="holder mt-0">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 aside aside--left">
                        <div class="list-group">
                            <a class="list-group-item text-success">01. Member Details</a>
                            <a class="list-group-item text-success" disabled>02. Overview</a>
                            <a class="list-group-item text-success" disabled>03. Registration Completed</a>
                        </div>
                    </div>
                    <div class="col-md-9 aside">
                        <div class="row">
                            <div class="col-md-8 offset-lg-2 offset-md-2">
                                <div class="card border-grey border-lighten-3 px-1 py-1 m-0 bg-grey">
                                    <div class="card-header bg-grey border-0 p-1">
                                        <h2 class="text-center white">Welcome to {{ config('project.brand') }} family</h2>
                                        @if(session('success'))
                                            <div class="alert alert-success">{{ session('success') }}</div>
                                        @endif
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    Name: <span class="pull-right font-weight-bold">{{ $user->detail->full_name }}</span>
                                                </li>
                                                <li class="list-group-item bg-primary text-white">
                                                    Tracking ID: <span class="pull-right font-weight-bold">{{ $user->tracking_id }}</span>
                                                </li>
                                                <li class="list-group-item">
                                                    Username: <span class="pull-right font-weight-bold">{{ $user->username }}</span>
                                                </li>
                                                <li class="list-group-item">
                                                    Password: <span class="pull-right font-weight-bold">{{ $user->password }}</span>
                                                </li>
                                            </ul>
                                            <div class="text-center mt-2">
                                                <a href="{{ route('user-login') }}" class="btn btn-glow btn-bg-gradient-x-website-red mr-1 mb-1 continue">
                                                    Visit Your Account <i class="la la-arrow-circle-o-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
