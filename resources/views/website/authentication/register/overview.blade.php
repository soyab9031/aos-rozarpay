@extends('website.template.layout')
@section('page-title', 'Overview')
@section('page-content')
    <div class="page-content">
        <div class="holder mt-0">
            <div class="container">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('website-home') }}">Home</a></li>
                    <li><a href="{{ route('user-login') }}">Login</a></li>
                    <li><span>Registration</span></li>
                </ul>
            </div>
        </div>
        <div class="holder mt-0">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 aside aside--left">
                        <div class="list-group">
                            <a class="list-group-item text-success">01. Member Details</a>
                            <a class="list-group-item text-success" disabled>02. Overview</a>
                            <a class="list-group-item" disabled>03. Registration Completed</a>
                        </div>
                    </div>
                    <div class="col-md-9 aside">
                        <form action="" method="post" id="overview_form">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-8 offset-lg-2 offset-md-2">
                                    <div class="card border-grey bg-grey border-lighten-3 px-1 py-1 m-0">
                                        <div class="card-header bg-grey border-0 p-1">
                                            <h4 class="card-title font-medium-4 text-center text-danger mt-1">
                                                Registration Overview
                                            </h4>
                                            <div class="row align-items-center justify-content-center">
                                                <div class="col-md-12">
                                                    <ul class="list-group ">
                                                        <li class="list-group-item bg-danger text-white">
                                                            Sponsor Upline: <span class="text-white font-weight-bold pull-right">
                                                {{ $session_detail->sponsor_user->detail->full_name }}
                                                                ({{ $session_detail->sponsor_user->tracking_id }})</span>
                                                        </li>
                                                        <li class="list-group-item bg-danger text-white">
                                                            Selected Leg: <span class="pull-right">{{ $session_detail->selected_leg == 'L' ? 'Left' : 'Right' }}
                                                                Leg</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="font-medium-4 text-center mt-1">
                                                User / Member Details
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    Name:
                                                    <span class="font-weight-bold white pull-right">
                                        {{ $user_detail->title }} {{ $user_detail->first_name }} {{ $user_detail->last_name }}
                                    </span>
                                                </li>
                                                <li class="list-group-item">
                                                    Email: <span class="font-weight-bold white pull-right"> {{ $user_detail->email }}</span>
                                                </li>
                                                <li class="list-group-item">
                                                    Mobile: <span class="font-weight-bold white pull-right"> {{ $user_detail->mobile }}</span>
                                                </li>
                                                <li class="list-group-item">
                                                    Birth date: <span
                                                            class="font-weight-bold white pull-right"> {{ $user_detail->birth_date }}</span>
                                                </li>
                                            </ul>
                                            <hr>
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    Username: <span
                                                            class="text-danger text-bold-600 pull-right"> {{ $user_detail->username }}</span>
                                                </li>
                                                <li class="list-group-item">
                                                    Password: <span
                                                            class="text-danger text-bold-600 pull-right"> {{ $user_detail->password }}</span>
                                                </li>
                                            </ul>
                                            <div class="row mt-1">
                                                <div class="col-6 col-md-6">
                                                    <button type="button" class="btn btn-info"
                                                            onclick="history.go(-1);">
                                                        <i class="fa fa-arrow-circle-o-left"></i> Back
                                                    </button>
                                                </div>
                                                <div class="text-right col-6 col-md-6">
                                                    <button type="button"
                                                            class="btn continue">
                                                        Register  <i class="fa fa-arrow-circle-o-right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')
    <script type="text/javascript">

        $('.continue').click(function (e) {

            var form = $('#overview_form');
            e.preventDefault();

            $('.back-btn').remove();
            $(this).attr('disabled', true);

            swal('Processing', 'Do not Close or Refresh this page..!!', 'info');
            form.submit();
        });

    </script>
@stop