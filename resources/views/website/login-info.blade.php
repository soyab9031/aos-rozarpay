@extends('website.template.layout')
@section('page-title','Business Opportunity')
@section('page-content')
    <div class="holder mt-0">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="{{ route('website-home')  }}">Home</a></li>
                <li><span>Business Opportunity</span></li>
            </ul>
        </div>
    </div>
    <div class="holder mt-0">
        <div class="container">
            <h2 class="h1-style text-center">Business Opportunity</h2>
            <div class="row vert-margin">
                <div class="col-sm-6">
                    <div class="block-it text-center">
                        <div class="block-it-icon"><i class="icon-person"></i></div>
                        <h3 class="text-uppercase">Direct Retailer</h3>
                        <p>AOS LifeStyle Provides A World Of Opportunities With A New Promise And Focuses On Enriching Lives
                            By Transforming Lifestyle.</p>
                        <a class="btn mt-1" href="{{ route('user-login') }}" type="button">Login</a>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="block-it text-center">
                        <div class="block-it-icon"><i class="icon-person"></i></div>
                        <h3 class="text-uppercase">Customer</h3>
                        <p>AOS LifeStyle Provides A World Of Opportunities With A New Promise And Focuses On Enriching Lives By
                            Transforming Lifestyle.</p>
                        <a class="btn mt-1" href="{{ route('customer-login') }}" type="button">Login</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop