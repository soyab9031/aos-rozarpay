@extends('website.template.layout')
@section('page-title','About')
@section('page-content')
    <div class="page-content">
        <div class="holder mt-0">
            <div class="container">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('website-home')  }}">Home</a></li>
                    <li><span>About Us</span></li>
                </ul>
            </div>
        </div>
        <div class="holder mt-0">
            <div class="container">
                <h2 class="h1-style text-center">Our Company</h2>
                <div class="row justify-content-md-center">
                    <div class="col-md-12">
                        <div class="about">
                            <div class="about-image">
                                <img src="{{ env('WEB_CONTENT_IMAGE_URL').$content->about->image }}" alt="" class="center">
                            </div>
                            <div class="about-content">
                                <p class="text-justify">
                                    {!! $content->about->description !!}
                                </p>
                                {{--<button class="btn mt-1" type="button">Read More</button>--}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="vision-title">
                            <h2 class="h1-style text-left">Our Vision</h2>
                        </div>
                        <div class="vision-content">
                            <p class="text-justify">
                                {!! $content->vision->description !!}
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="vision-image">
                            <img src="{{ env('WEB_CONTENT_IMAGE_URL').$content->vision->image }}" alt="">
                        </div>
                    </div>
                    <div class="col-md-6 mt-2">
                        <div class="mission-image">
                            <img src="{{ env('WEB_CONTENT_IMAGE_URL').$content->mission->image }}" alt="">
                        </div>
                    </div>
                    <div class="col-md-6 mt-2">
                        <div class="mission-title">
                            <h2 class="h1-style text-left">Our Mission</h2>
                        </div>
                        <div class="mission-content">
                            <p class="text-justify">
                                {!! $content->mission->description !!}
                            </p>
                        </div>
                    </div>
                </div>
                {{--<div class="timeLine-center">--}}
                {{--<div class="timeLine-item">--}}
                {{--<div class="timeLine-item-image text-center">--}}
                {{--<img src="/website-assets/images/aos-about.png" alt="" class="center">--}}
                {{--</div>--}}
                {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</p>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
@stop