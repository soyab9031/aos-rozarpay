@extends('website.template.layout')
@section('page-title','Customer')
@section('page-content')
    <div class="holder mt-0">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="{{ route('website-home')  }}">Home</a></li>
                <li><span>Customer</span></li>
            </ul>
        </div>
    </div>
    <div class="holder mt-0">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mt-5 mb-5">
                    <h2 class="h1-style text-center">Coming Soon..!</h2>
                </div>
            </div>
        </div>
    </div>
@stop