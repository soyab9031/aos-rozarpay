<div class="modal fade modal--checkout" id="updateAddress" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" >
    <div class="modal-dialog">

        <div class="modal-header">
            <div class="modal-header-title">
                <i class="icon icon-check-box"></i>
                <span>Update Your Address</span>
            </div>
            <a href="{{ url()->current() }}" class="close">
                <span aria-hidden="true">&times;</span>
            </a>
        </div>
        <div class="modal-content">
            <div class="modal-body">
                @if(session('address_errors'))
                    <div class="alert alert-danger">
                        <ul>
                            @foreach(session('address_errors') as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{ route('website-checkout-address-update') }}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" value="{{ Route::getCurrentRoute()->getName() }}" name="page">
                    <input type="hidden" value="{{ $selected_address->id }}" name="id">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Full Name</label>
                                <input type="text" name="name" class="form-control"
                                       value="{{ $selected_address->name ? : Session::get('orderUser')->name }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Contact</label>
                                <input type="text" name="mobile" class="form-control"
                                       onkeypress="INGENIOUS.numericInput(event)"
                                       value="{{ $selected_address->mobile ? : Session::get('orderUser')->mobile }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" name="email" class="form-control"
                                       value="{{ $selected_address->email ? : Session::get('orderUser')->email }}" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group mt-1">
                                <label>Address</label>
                                <textarea name="address" placeholder="Enter Your Address" class="form-control" cols="30"
                                          rows="4" required>{{ $selected_address->address }}</textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Landmark</label>
                                <input type="text" name="landmark" class="form-control"
                                       value="{{ $selected_address->landmark }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Address Type</label>
                                <select name="type" class="form-control">
                                    <option value="1" {{ $selected_address->type == 1 ? 'selected' : null }}>Home
                                    </option>
                                    <option value="2" {{ $selected_address->type == 2 ? 'selected' : null }}>Office
                                    </option>
                                    <option value="3" {{ $selected_address->type == 3 ? 'selected' : null }}>Other
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>City</label>
                                <input type="text" name="city" class="form-control"
                                       value="{{ $selected_address->city }}" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>State</label>
                                <select name="state_id" class="form-control" required>
                                    <option value="">Select</option>
                                    @foreach(\App\Models\State::active()->orderBy('name', 'asc')->get() as $state)
                                        <option value="{{ $state->id }}" {{ $selected_address->state_id == $state->id ? 'selected' : null }}>{{ $state->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Pincode</label>
                                <input type="text" name="pincode" class="form-control"
                                       onkeypress="INGENIOUS.numericInput(event)"
                                       value="{{ $selected_address->pincode }}" required>
                            </div>
                        </div>
                        <div class="col-md-12 text-center mt-1">
                            <button class="btn btn-danger">Save</button>
                            <a href="{{ url()->current() }}" class="btn btn-primary text-white">Close</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    textarea.form-control , select.form-control{
        height: auto !important;
    }
</style>