@extends('website.template.layout')
@section('page-title','Business Opportunity')
@section('page-content')
    <div class="holder mt-0">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="{{ route('website-home')  }}">Home</a></li>
                <li><span>Business Opportunity</span></li>
            </ul>
        </div>
    </div>
    <div class="holder mt-0">
        <div class="container">
            <h2 class="h1-style text-center">Business Opportunity</h2>
            <div class="row justify-content-md-center">
                <div class="col-md-12">
                    <div class="about">
                        <div class="about-image">
                            <img src="{{ env("WEB_CONTENT_IMAGE_URL").$business->image }}" alt="" class="center">
                        </div>
                        <div class="company-content">
                            <p class="text-justify">{!! $business->description !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop