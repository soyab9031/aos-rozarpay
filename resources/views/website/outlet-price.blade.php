@extends('website.template.layout')
@section('page-title','Outlets')
@section('page-content')
    <div class="page-content">
        <div class="holder mt-0">
            <div class="container">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('website-home')  }}">Home</a></li>
                    <li><span>Outlets</span></li>
                </ul>
            </div>
        </div>
        <div class="holder mt-0">
            <div class="container">
                <div class="row">
                    @foreach($outlets as $outlet)
                        <div class="col-md-3">
                            <div class="pricingTable">
                                <div class="pricingTable-header">
                                        <span class="heading">
                                            {{ $outlet->name }}
                                        </span>
                                </div>
                                <div class="pricing-plans">
                                        <span class="price-value">
                                            <i class="fa fa-inr"></i>
                                            <span>{{ number_format($outlet->amount) }}</span>
                                        </span>
                                    {{--<span class="subtitle">text</span>--}}
                                </div>
                                <div class="pricingContent">
                                    <ul>
                                        <li><b>Outlet area</b> {{ $outlet->area }} Sqft</li>
                                        <li><b>Branding </b> Rs. {{ number_format($outlet->branding_investment) }}</li>
                                        <li><b>Sales Profit</b> 70%</li>
                                        <li><b>Maintenance Profit</b> 80%</li>
                                        <li><b>Distribution Profit</b> {{ $outlet->id == 4 ? 'NIL':  '10%'}}</li>
                                    </ul>
                                </div><!-- /  CONTENT BOX-->
                                <div class="pricingTable-sign-up"><!-- BUTTON BOX-->
                                    <a href="{{ route('store-register-request') }}"
                                       class="btn btn-block btn-default">buy now</a>
                                </div><!-- BUTTON BOX-->
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@stop