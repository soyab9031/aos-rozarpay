@extends('customer.template.layout')

@section('title', 'My Profile')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">My Profile</h3>
        </div>
    </div>
    <div class="content-body">

        <form role="form" method="post" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Personal Information</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">

                                <ul class="list-group">
                                    <li class="list-group-item">
                                        Tracking ID: <span class="pull-right">{{ $customer->tracking_id }}</span>
                                    </li>
                                    <li class="list-group-item">
                                        Name: <span class="pull-right">{{ $customer->name }}</span>
                                    </li>
                                    <li class="list-group-item">
                                        Mobile: <span class="pull-right">{{ $customer->mobile }}</span>
                                    </li>
                                    <li class="list-group-item">
                                        Email: <span class="pull-right">{{ $customer->email ? $customer->email : 'N.A' }}</span>
                                    </li>
                                </ul>
                                <div class="form-group mt-1">
                                    <label>Date Of Birth</label>
                                    @if($customer->birth_date)
                                        <input type="text" name="birth_date" class="form-control birth-date" value="{{ Carbon\Carbon::parse($customer->birth_date)->format('d-M-Y') }}" readonly>
                                    @else
                                        <input type="text" name="birth_date" class="form-control birth-date" readonly>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Address Information</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label>Address</label>
                                            <textarea name="address" class="form-control" cols="30" rows="6">{{ $customer->address->address }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>Landmark</label>
                                            <input type="text" class="form-control square" placeholder="Enter Landmark" name="landmark" value="{{ $customer->address->landmark}}">
                                        </div>
                                        <div class="form-group">
                                            <label>City</label>
                                            <input type="text" placeholder="Enter City" class="form-control square" name="city" value="{{ $customer->address->city}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>District</label>
                                            <input type="text" class="form-control square" placeholder="District" name="district" value="{{ $customer->address->district}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>State</label>
                                            <select class="form-control" name="state_id">
                                                @foreach ($states as $state)
                                                    <option value="{{ $state->id }}" {{ $customer->address->state_id == $state->id ? 'selected' : ''}} >{{ $state->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Pincode</label>
                                            <input type="text" class="form-control square" placeholder="Pincode" name="pincode" value="{{ $customer->address->pincode}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-danger">Update</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop
@section('page-javascript')
    <script>
        $('.birth-date').datepicker({
            autoclose: true,
            format: "dd-mm-yyyy",
            startDate: "-50y",
            endDate: "-18y"
        });
    </script>
@stop