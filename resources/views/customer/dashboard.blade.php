@extends('customer.template.layout')

@section('title', 'Dashboard')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Dashboard</h3>
        </div>
    </div>
    <div class="content-body">
        <div class="row match-height">
            <div class="col-xl-3 col-lg-6 col-md-12">
                <a href="{{ route('website-home') }}">
                    <div class="card bg-blue">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="align-self-top">
                                        <i class="la la-shopping-cart white font-large-4"></i>
                                    </div>
                                    <div class="media-body text-right align-self-bottom mt-3">
                                        <span class="d-block mb-1 font-medium-1 white mr-1">Go to </span>
                                        <h1 class="white mb-0 ml-15">Shopping</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-12">
                <a href="{{ route('customer-profile') }}">
                    <div class="card bg-primary">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="align-self-top">
                                        <i class="la la-user white font-large-4"></i>
                                    </div>
                                    <div class="media-body text-right align-self-bottom mt-3">
                                        <span class="d-block mb-1 font-medium-1 white">My</span>
                                        <h1 class="white mb-0">Account</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-12">
                <a href="{{ route('customer-shopping-order-view') }}">
                    <div class="card bg-dark">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="align-self-top">
                                        <i class="la la-list white font-large-4"></i>
                                    </div>
                                    <div class="media-body text-right align-self-bottom mt-3">
                                        <span class="d-block mb-1 font-medium-1 white">My Orders</span>
                                        <h1 class="white mb-0">{{ number_format($order_count) }}</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
@stop