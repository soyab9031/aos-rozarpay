<nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="collapse navbar-collapse show" id="navbar-mobile">
                <ul class="nav navbar-nav mr-auto float-left">
                    <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                    <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
                </ul>
                @php
                    $customer = \App\Models\Customer::whereId(Session::get('customer')->id)->first();
                @endphp
                <ul class="nav navbar-nav float-right">
                    <li class="dropdown dropdown-notification nav-item">
                        <a class="nav-link nav-link-label" href="{{ route('website-cart') }}" target="_blank">
                            <i class="ficon ft-shopping-cart bell-shake" id="notification-navbar-link"></i>
                            <span class="badge badge-pill badge-sm badge-success badge-up badge-glow">
                                {{ Session::get('cart') ? count(Session::get('cart')) : 0 }}
                            </span></a>
                    </li>
                    <li class="dropdown dropdown-user nav-item">
                        <a class="dropdown-toggle nav-link dropdown-user-link text-center" href="#" data-toggle="dropdown">
                            <span class="avatar avatar-online">
                                <img src="/user-assets/images/icons/user-tie.svg" class="profile-image" height="100px">
                                {{--<img src="{{ $customer->detail->image ? env('USER_PROFILE_IMAGE_URL').$customer->detail->image : '/user-assets/images/icons/user-tie.svg' }}" class="profile-image" height="100px">--}}
                            </span>
                            <small style="display: block">{{ Session::get('customer')->name }}</small>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <div class="arrow_box_right">
                                <a class="dropdown-item" href="#">
                                   <span class="text-bold-700 text-capitalize">
                                       {{ \Str::limit(Session::get('customer')->name, 15) }}
                                   </span>
                                </a>
                                <a class="dropdown-item header-tracking-id" href="javascript:void(0)">
                                    TID: {{ Session::get('customer')->tracking_id }}
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('customer-logout') }}"><i class="ft-power"></i> Logout</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>