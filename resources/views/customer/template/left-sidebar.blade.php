<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow " data-scroll-to-active="true" data-img="/user-assets/images/backgrounds/01.jpg">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto">
                <a class="navbar-brand" href="{{ route('customer-dashboard') }}">
                    <img class="brand-logo" alt="{{ config('project.brand') }}" src="/user-assets/images/company/logo.png"/>
                </a>
            </li>
            <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>
        </ul>
    </div>
    <div class="navigation-background"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="nav-item">
                <a href="{{ route('customer-dashboard') }}">
                    <i class="ft-home"></i><span class="menu-title">Dashboard</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('customer-shopping-order-view') }}">
                    <i class="ft-list"></i><span class="menu-title">Orders</span>
                </a>
            </li>
        </ul>
    </div>
</div>