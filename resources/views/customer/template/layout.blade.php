<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="Tymk Softwares">
    <title>@yield('title') - {{ config('project.company') }}</title>
    <link rel="apple-touch-icon" href="/user-assets/images/company/favicon.png">
    <link rel="shortcut icon" type="image/x-icon" href="/user-assets/images/company/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/fonts/feather/style.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/fonts/line-awesome/css/line-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/fonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/colors.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/components.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/vertical-menu.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/plugins/toastr/toastr.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/style.css?version=2021">
    <link href="/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
    {{-- DateRange filter  css--}}
    <link href="/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" media="screen">
    @yield('import-css')
    @yield('page-css')
    <style>
        .avatar img {
            width: 35px;
            max-width: 35px;
            height: 35px;
            border-radius: 50%;
            border: 1px solid;
            padding: 3px;
            background: #fff;
        }
    </style>
</head>
<body class="vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-website-red" data-col="2-columns" oncontextmenu="return false">
@if(env('APP_ENV') != 'production')
    <img style="position: fixed;z-index: 99999;width: 150px;margin: auto;top: -2px;left: 0;right: 0;opacity: 0.75;" src="/user-assets/images/ribbon.png" alt="TYMK Software Dev Mode">
@endif
@include('customer.template.header')
@include('customer.template.left-sidebar')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        @yield('content')
    </div>
</div>
@include('customer.template.footer')
<script src="/user-assets/js/vendors.min.js" type="text/javascript"></script>
<script src="/user-assets/js/app-menu.js" type="text/javascript"></script>
<script src="/user-assets/js/app.js?v=202010" type="text/javascript"></script>
<script src="/user-assets/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<script src="/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/user-assets/js/lodash.js"></script>
<script src="/user-assets/plugins/axios/axios.min.js"></script>
<script src="/user-assets/plugins/vue/{{ env('APP_ENV') == 'local' ? 'vue.js' : 'vue.min.js' }}"></script>
<script src="/user-assets/plugins/vue/vue-helper.js"></script>
<script src="/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
{{-- DateRange filter  js--}}
<script src="/plugins/moment/moment.js" type="text/javascript"></script>
<script src="/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
@yield('import-javascript')
@yield('page-javascript')
<script>
    @if(session('errors'))
    toastr.error('{{ session('errors')->first() }}', "Error");
    @elseif(session('error'))
    toastr.error('{{ session('error') }}', "Oops..!!");
    @elseif(session('success'))
    toastr.success('{{ session('success') }}', "Hurray..!!");
    @endif
</script>
</body>
</html>