@php
    $f = new \App\Library\CurrencyInWord;
@endphp
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $filename }}</title>
    <style>
        @media print {
            @page  {
                margin: .5cm .3cm;
            }
        }
        table { page-break-inside:auto }
        tr    { page-break-inside:avoid; page-break-after:auto;}
        thead { display:table-header-group }
        tfoot { display:table-footer-group }
        body {
            font-family: Verdana, Arial, Helvetica, sans-serif;
        }
        .text-center {
            text-align: center !important;
        }
        .text-right {
            text-align: right !important;
        }
        .datagrid table {
            border-collapse: collapse;
            text-align: left;
            width: 100%;
        }

        .datagrid {
            font: normal 12px/150% Verdana, Arial, Helvetica, sans-serif;
            background: #fff;
            overflow: hidden;
            border: 0px solid #000;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        .datagrid table td,
        .datagrid table th {
            padding: 3px 10px;
        }

        .datagrid table tbody tr.header td {
            filter: progid: DXImageTransform.Microsoft.gradient(startColorstr='#FFFAC3', endColorstr='#80141C');
            background-color: #000;
            color: #FFFFFF;
            font-size: 10px;
            font-weight: bold;
            padding: 7px;
        }
        .datagrid table tbody tr.table-title td
        {
            text-align: center;
            /*background: rgba(255, 60, 33, 0.9);*/
            /*color: #fff;*/
            font-size: 12px !important;
            border-top: 2px solid #000;
            padding: 7px;
            font-weight: 600;
        }
        .datagrid table tbody tr.child-category-title td
        {
            font-size: 13px;
        }
        .datagrid table thead th:first-child {
            border: none;
        }

        .datagrid table tbody td {
            color: #000;
            border: 1px solid #000;
            font-size: 10px;
            font-weight: normal;
            padding: 8px;
            border-bottom: 1px solid #000;
        }
        .datagrid table tbody .alt td {
            background: #F7CDCD;
            color: #000;
        }
        .border-bottom {
            border-bottom: 1px solid #000 !important;
        }
        .ml-40{
            margin-left: 40px !important;
        }
        .mt-1{
            margin-top: 1rem;
        }
        .text-bold-700{
            font-weight: 700;
        }
        .text-bold-600{
            font-weight: 600 !important;
        }
        .text-uppercase{
            text-transform: uppercase !important;
        }
        .vertical-top{
            vertical-align: top !important;
        }
        .mt-100{
            margin-top: 100px !important;
        }
        .text-left{
            text-align: left !important;
        }
        .no-border{
            border: none !important;
        }
        .font-15{
            font-size: 15px !important;
        }
        .tbl-border {
            border: 1px solid black;
            border-collapse: collapse;
        }
        .border-td{
            border-right: none !important;
            border-top: none !important;
            border-bottom: none !important;
            border-left: #000 1px solid !important;
        }
    </style>
</head>
<body >

<div class="border">
    <table style="width: 100%">
        <tr>
            <td width="40%" style="text-align: left">
                <img src="{{ config('project.url') }}/user-assets/images/company/logo.png" width="50%" alt="{{ config('project.brand') }}" class="ml-40 mb-3 pull-right" >
                <div class="address" style="font-size: 12px; color: #122b40;">
                    <ul class="px-0 list-unstyled text-bold-700" style="list-style-type:none !important;">
                        <li>{{ collect($setting->value)->where('title','company_name')->first()->value }}</li>
                        <li class="mt-1">24/3rt, 10-3-546,First floor,<br/>
                            Karan Residency, Besides Union Bank of India,<br/>
                            Vijaya Nagar colony Hyderabad -500057</li>
                        <li>{{ collect($setting->value)->where('title','city')->first()->value }} - {{ collect($setting->value)->where('title','pincode')->first()->value }},</li>
                        <li>{{ collect($setting->value)->where('title','state')->first()->value }}, {{ collect($setting->value)->where('title','country')->first()->value }}</li>
                        <li>TOLL Free No.: 1800 2744 700</li>
                        <li>EMAIL ID: support@mayleecarennutricare.com</li>
                        <li>GST: {{ collect($setting->value)->where('title','gst_no')->first()->value }}</li>
                        <li>CIN NO: U52200TG2021PTC151572</li>
                    </ul>
                </div>
            </td>
            <td width="30%" class="vertical-top">
                <div class="text-uppercase text-bold-600 text-center" style="font-size: 20px;">TAX INVOICE</div>
                <div class="text-uppercase text-bold-600 mt-100 text-center" style="font-size: 14px;">original</div>
                <div class="text-bold-600 mt-1" style="font-size: 14px;">
                    <span>Invoice :</span> {{ $order->customer_order_id }}
                </div>
                <div class="text-bold-600" style="font-size: 14px;">Operator   : {{ $order->customer_order_id }}</div>
                <div class="text-bold-600" style="font-size: 14px;">Order Date : {{ $order->created_at->format('M d, Y') }}</div>
                <div class="text-bold-600" style="font-size: 14px;">Paid Date  : {{ $order->created_at->format('M d, Y') }}</div>
                <div class="text-bold-600" style="font-size: 14px;">Print Date : {{ $order->created_at->format('M d, Y') }}</div>
            </td>
            <td width="30%" class="text-center">
                <div class="text-bold-600 mt-100 ml-40" style="font-size: 14px;">{{ $order->customer_order_id }}</div>
                <div class="text-bold-600" style="font-size: 14px;margin-left: 30px">Warehouse  : MNC</div>
                <div class="text-bold-600" style="font-size: 14px;margin-left: 70px">Order Channel : Member</div>
            </td>
        </tr>
    </table>

    <div class="datagrid" style="margin-top: 15px;">
        <table class="tbl-border">
            <tbody >
            <tr>
                <td colspan="2">
                    <span class="font-15">Invoice No. {{ $order->customer_order_id }}</span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <span class="font-15">Invoice Date : {{ $order->created_at->format('M d, Y h:i A') }}</span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <span class="font-15">Order No.: {{ $order->customer_order_id }} </span>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="text-center">
                    <span class="text-bold-700 font-15">Details of Receiver ( Bill TO)</span>
                </td>
            </tr>
            <tr>
                <td class="border-td font-15 text-bold-600" width="50%">Purchased by: Brand Partner 50% Discount.</td>
                <td class="border-td font-15 text-bold-600" width="50%"> Ship To  : </td>
            </tr>
            <tr>
                <td width="50%" class=" font-15 text-bold-600 border-td">ID : {{ $order->user->tracking_id }}</td>
                <td class="border-td"></td>
            </tr>
            <tr>
                <td width="50%" class=" font-15 text-bold-600 border-td">GSTIN : </td>
                <td width="50%" class=" font-15 text-bold-600 border-td">Name : <span>{{ $order->shipping_address->name }}</span></td>
            </tr>
            <tr>
                <td width="50%" class="border-td font-15 text-bold-600">Name : {{ $order->user->detail->full_name }}</td>
                <td width="50%" class="border-td font-15 text-bold-600"></td>
            </tr>
            <tr>
                <td width="50%" class="border-td font-15 text-bold-600">Address : {{ $order->user->address->address }}</td>
                <td width="50%" class="border-td font-15 text-bold-600">Address : {{ $order->shipping_address->address ? $order->shipping_address->address : '' }}</td>
            </tr>
            <tr>
                <td width="50%" class="border-td font-15 text-bold-600">Mobile No : {{ $order->user->mobile }}</td>
                <td width="50%" class="border-td font-15 text-bold-600"></td>
            </tr>
            <tr>
                <td width="50%" class="border-td font-15 text-bold-600">Sponsor ID : {{ isset($order->user->sponsorBy->tracking_id) }}</td>
                <td width="50%" class="border-td font-15 text-bold-600">Mobile No : {{ $order->shipping_address->mobile ? $order->shipping_address->mobile : '' }}</td>
            </tr>
            <tr>
                <td width="50%" class="border-td font-15 text-bold-600">Sponsor Name : {{ $order->user->sponsorBy ? $order->user->sponsorBy->detail->full_name : '' }}</td>
                <td class="border-td"></td>
            </tr>
            </tbody>
        </table>

        <table style="width: 100%;">
            <tbody>
            @php
                $sum = 0;
                $total = 0;
            @endphp
            <tr class="text-center">
                <td colspan="8"></td>
                @if($order->user->address->state->id == $company_state_id)
                    <td colspan="2">CGST</td>
                    <td colspan="2">SGST</td>
                @else
                    <td colspan="2">IGST</td>
                @endif
                <td colspan="6"></td>
            </tr>
            <tr class="table-title">
                <td width="3%" rowpan="8">S. No.</td>
                <td width="15%" rowpan="8">Product Name</td>
                <td width="5%" rowpan="8">HSN Code</td>
                <td width="5%" rowpan="8">Qty</td>
                <td width="8%" rowpan="8">Rate</td>
                <td width="8%" rowpan="8">Total</td>
                <td width="8%" rowpan="8">Discount</td>
                <td width="8%" rowpan="8">Taxable Value</td>
                @if($order->user->address->state->id == $company_state_id)
                    <td width="3%" rowpan="8">%</td>
                    <td width="3%" rowpan="8">Amt</td>
                    <td width="3%" rowpan="8">%</td>
                    <td width="3%" rowpan="8">Amt</td>
                @else
                    <td width="3%" rowpan="8">%</td>
                    <td width="3%" rowpan="8">Amt</td>
                @endif
                <td width="10%">Total</td>
                <td width="5%" rowpan="8">BV</td>
            </tr>

            @foreach($order->details as $index => $detail)
                <tr class="text-center">
                    <td>{{ $index+1 }}</td>
                    <td>{{ $detail->product_price->product->name }}</td>
                    <td>{{ $detail->gst->code }}</td>
                    <td>{{ $detail->qty }}</td>
                    <td>
                        {{ number_format($detail->actual_amount, 2) }}
                    </td>
                    <td>
                        {{ number_format($detail->actual_amount * $detail->qty, 2) }}
                    </td>
                    <td>0</td>
                    <td>{{ number_format($detail->total_taxable_amount, 2) }}</td>
                    @if($order->user->address->state->id == $company_state_id)
                        <td>{{ $detail->gst->percentage/2 }}%</td>
                        <td>{{ number_format(($detail->total_tax_amount)/2, 2) }} </td>

                        <td>{{ $detail->gst->percentage/2 }}%</td>
                        <td>{{ number_format(($detail->total_tax_amount)/2, 2) }} </td>
                    @else
                        <td>{{ $detail->gst->percentage }}%</td>
                        <td>{{ number_format(($detail->total_tax_amount), 2) }} </td>
                    @endif
                    <td>{{ number_format(($detail->total_amount), 2) }}</td>
                    <td>{{ $detail->total_bv }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <table style="width:100%;border-right: 1px solid #000;">
            <tbody>
            <tr>
                <td class="33%" colspan="3">
                    <div style="font-size: 15px;">
                        Before Discount Amount :
                        <span>{{ number_format(collect($order->details)->sum('total_taxable_amount'), 2) }}</span>
                    </div>
                </td>
                <td class="33%">
                    <div style="font-size: 15px;">
                        Discount :
                        <span>{{ number_format(collect($order->details)->sum('discount'), 2) }}</span>
                    </div>
                </td>
                <td class="34%">
                    <div style="font-size: 15px;">
                        Taxable Amount :
                        <span>{{ number_format(collect($order->details)->sum('total_taxable_amount'), 2) }}</span>
                    </div>
                </td>
            </tr>
            <tr>
                @if($order->user->address->state->id == $company_state_id)
                    <td class="33%">
                        <div style="font-size: 15px;">
                            CGST Amount :
                            <span>{{  number_format(collect($order->details)->sum('total_tax_amount')/2, 2)  }}</span>
                        </div>
                    </td>
                    <td class="33%" colspan="2">
                        <div style="font-size: 15px;">
                            SGST Amount :
                            <span>{{  number_format(collect($order->details)->sum('total_tax_amount')/2, 2)  }}</span>
                        </div>
                    </td>
                @else

                    <td class="33%" colspan="3">
                        <div style="font-size: 15px;">
                            <div style="font-size: 15px;"> IGST Amount :
                                <span>{{  number_format(collect($order->details)->sum('total_tax_amount'), 2)  }}</span></div>
                        </div>
                    </td>
                @endif
                <td class="40%" >
                    <div style="font-size: 15px;">
                        <span></span></div>
                </td>
                <td class="34%">
                    <div style="font-size: 15px;">
                        Total Tax(+) :
                        <span>{{  number_format(collect($order->details)->sum('total_tax_amount'), 2)  }}</span>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="33%" colspan="3">
                    <div style="font-size: 15px;">
                        <div style="font-size: 15px;">Total BV: <span>{{  number_format(collect($order->details)->sum('total_bv'))  }}</span></div>
                    </div>
                </td>
                <td class="33%" >
                    <div style="font-size: 15px;">Total Qty : {{ collect($order->details)->sum('qty') }}<span></span></div>
                </td>
                <td class="34%" >
                    <div style="font-size: 15px;">Round Off(+/-) : <span>0</span></div>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="70%">
                    <div style="font-size: 15px;">
                        Rs. (In Words) :
                        <b style="font-size: 15px;">
                            @if($order->wallet > 0)
                                {{ ucwords($f->display( collect($order->details)->sum('qty') * 1 )) }}
                            @else
                                {{ ucwords($f->display(collect($order->details)->sum('total_amount'))) }}
                            @endif
                        </b>
                    </div>
                </td>
                <td class="30%">
                    <div style="font-size: 15px;">
                        Net Amount :
                        @if($order->wallet > 0)
                            <span style="font-weight: 800;font-size: 16px">{{ number_format( collect($order->details)->sum('qty') * 1 )}}</span>
                        @else
                            <span style="font-weight: 800;font-size: 16px">{{ number_format(collect($order->details)->sum('total_amount') )}}</span>
                        @endif
                    </div>
                </td>
            </tr>
            </tbody>
        </table>

        <table style="margin: 1rem 0 2rem 0; border: 1px solid #000;">
            <tbody>
            <tr class="50%">
                <td colspan="2">
                    <div class="text-uppercase text-center" style="font-size: 15px;"><span>Terms & Condition Of Invoice </span></div>
                </td>
                <td class="50%">
                    <div class="text-center" style="font-size: 15px;"><span> Certified that the particulars given above are true and correct</span></div>
                </td>
            </tr>
            <tr class="60%">
                <td rowspan="2" colspan="2">
                    <p class="pull-left" style="font-size: 10px;"><span><b>We declare that this invoice shows the actual price of the goods.</b></span></p>
                </td>
                <td style="border-bottom:1px solid #fff !important">
                    <div class="text-center" style="font-size: 15px;">
                    <span>For {{ collect($setting->value)->where('title','company_name')->first()->value }}<br>
                        @if($order->store)
                            {{ $order->store->name }} <br>({{ $order->store->tracking_id }})
                        @endif
                    </span></div>
                    <br>
                    <br>
                    <br>
                </td>
            </tr>
            <tr class="40%" >
                <td >
                    <br>
                    <br>
                    <br>
                    <div class="text-center" style="font-size: 15px;"><span>Authorized Signatory.</span></div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="pull-left" style="font-size: 15px;"><span>Receiver Signature:  </span></div>
                </td>
                <td>
                    <div class="text-center"  style=" text-align: center; font-weight: 600; font-size: 15px;">THANK YOU FOR SHOPPING WITH US !</div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <div style="text-align: center; margin-top: 10px; font-size: 14px;">
        This is a Computer Generated Order Invoice
    </div>
    <div style="text-align: center; margin-top: 10px; font-size: 14px; border-top: 1px solid">
        **END OF INVOICE**
    </div>
</div>
</body>
</html>