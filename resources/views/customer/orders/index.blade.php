@extends('customer.template.layout')

@section('title')
    My Orders
@stop

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">My Orders</h3>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Order Date</th>
                                        <th>Order Id</th>
                                        <th>Total Amount</th>
                                        <th>Delivery Type</th>
                                        <th>Status</th>
                                        <th>Details</th>
                                        {{--<th>Invoice</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orders as $index => $order)
                                        <tr>
                                            <td>{{ \App\Library\Helper::tableIndex($orders, $index) }}</td>
                                            <td>{{ $order->created_at->format('M d, Y h:i A') }}</td>
                                            <td class="text-primary"><i>{{ $order->customer_order_id }}</i></td>
                                            <td>{{ $order->total }}</td>
                                            <td>
                                                @if($order->delivery_type == 1)
                                                    <span class="text-white badge badge-primary"> Self PickUp </span>
                                                @else
                                                    <span class="badge badge-warning"> Courier </span>
                                                @endif
                                            </td>
                                            <td>
                                                @if($order->status == 2)
                                                    <span class="text-dark badge badge-warning"> Placed </span>
                                                @elseif(in_array($order->status, [\App\Models\Order::APPROVED]))
                                                    <span class="badge badge-success text-nowrap"> Approved </span>
                                                @elseif($order->status == 5)
                                                    <span class="badge badge-danger text-nowrap"> Rejected </span>
                                                @else
                                                    <span class="badge badge-secondary"> Pending </span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('customer-shopping-order-details', ['customer_order_id' => $order->customer_order_id]) }}"
                                                   class="badge badge-primary">Details</a>
                                            </td>
                                            {{--<td>--}}
                                                {{--@if($order->approved_at)--}}
                                                    {{--<a href="{{ route('customer-shopping-order-details', ['customer_order_id' => $order->customer_order_id, 'download' => 'receipt']) }}"--}}
                                                       {{--class="badge badge-danger">Download</a>--}}
                                                {{--@endif--}}
                                            {{--</td>--}}
                                        </tr>
                                    @endforeach
                                </table>
                                {{ $orders->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

