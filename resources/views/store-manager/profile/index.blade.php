@extends('store-manager.template.layout')

@section('title', 'Store Profile')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Store Profile</h3>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <ul class="list-group">
                                <li class="list-group-item text-primary">
                                    Id: <span class="pull-right">{{ $store->tracking_id }}</span>
                                </li>
                                <li class="list-group-item">
                                    Name: <span class="pull-right">{{ $store->name }}</span>
                                </li>
                                <li class="list-group-item">
                                    Contact: <span class="pull-right">{{ $store->mobile }}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form action="" method="post" onsubmit="INGENIOUS.blockUI(true)">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label>Address</label>
                                            <textarea name="address" class="form-control" cols="30" rows="6" required>{{ $store->address }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>City</label>
                                            <input type="text" placeholder="Enter City" class="form-control square" name="city" value="{{ $store->city}}" required>
                                        </div>
                                        <div class="form-group">
                                            <label>State</label>
                                            <select class="form-control" name="state_id">
                                                @foreach ($states as $state)
                                                    <option value="{{ $state->id }}" {{ $store->state_id == $state->id ? 'selected' : ''}} >{{ $state->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Pincode</label>
                                            <input type="text" class="form-control square" placeholder="Enter Pincode" name="pincode" value="{{ $store->pincode }}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center mt-1">
                                    <button type="submit" class="btn btn-danger">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop