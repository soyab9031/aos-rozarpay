@extends('store-manager.template.layout')

@section('title')
    Wallet Fund Requests
@stop

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Wallet Fund Requests</h3>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="row mb-1">
                                <div class="col-md-12 text-right">
                                    <a href="{{ route('store-wallet-request-create') }}" class="btn btn-linkedin">Create New Request</a>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Created On</th>
                                        <th>Amount</th>
                                        <th>Payment Mode</th>
                                        <th>Ref. No.</th>
                                        <th>Bank Name</th>
                                        <th>Deposited On</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($wallet_requests as $index => $wallet_request)
                                        <tr>
                                            <td>{{ \App\Library\Helper::tableIndex($wallet_requests, $index) }}</td>
                                            <td>{{ $wallet_request->created_at->format('M d, Y h:i A') }}</td>
                                            <td>{{ number_format($wallet_request->amount, 2) }}</td>
                                            <td>{{ $wallet_request->payment_mode }}</td>
                                            <td>
                                                {{ $wallet_request->reference_number }}
                                                @if($wallet_request->image)
                                                    <a href="{{ env('PAYMENT_RECEIPT_IMAGE_URL') . $wallet_request->image }}" target="_blank" class="badge badge-info">
                                                        Reference Image
                                                    </a>
                                                @endif
                                            </td>
                                            <td>{{ $wallet_request->bank_name }}</td>
                                            <td>{{ \Carbon\Carbon::parse($wallet_request->deposited_at)->format('M d, Y h:i A')  }}</td>
                                            <td>
                                                @if($wallet_request->status == 1)
                                                    <span class="badge badge-warning">Pending</span>
                                                @elseif($wallet_request->status == 2)
                                                    <span class="badge badge-success">Approved</span>
                                                @else
                                                    <span class="badge badge-danger">Rejected</span>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                                {{ $wallet_requests->links() }}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop