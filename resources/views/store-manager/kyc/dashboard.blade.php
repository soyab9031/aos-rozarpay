@extends('store-manager.template.layout')
@section('title')
    My KYC Details
@stop
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">
                My KYC Details
            </h3>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-md-8 offset-md-2 offset-lg-2">
                <div class="card">
                    <div class="card-header p-1">
                        <h4 class="card-title">My KYC Status</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-footer p-1">
                            <p class="text-center">
                                In order to serve you better and to avoid any hindrance
                                in account operations, we sincerely urge all our
                                customers to update their KYC documents.
                            </p>
                            <ul class="list-group">
                                <li class="list-group-item text-dark" onclick="window.location.href='{{ route('store-kyc-pancard') }}'">
                                    PANCARD
                                    <span class="pull-right font-medium-2">
                                        @if($store_status->pancard == \App\Models\StoreManager\StoreStatus::KYC_PENDING)
                                            <span class="badge bg-pink"><i class="la la-exclamation-triangle"></i> PENDING</span>
                                        @elseif($store_status->pancard == \App\Models\StoreManager\StoreStatus::KYC_INPROGRESS)
                                            <span class="badge badge-warning"><i class="la la-refresh"></i> In Progress</span>
                                        @elseif($store_status->pancard == \App\Models\StoreManager\StoreStatus::KYC_VERIFIED)
                                            <span class="badge badge-success"><i class="la la-check-circle"></i> Verified </span>
                                        @else
                                            <span class="badge badge-danger"><i class="la la-close"></i> Rejected</span>
                                        @endif
                                    </span>
                                </li>
                                {{-- TODO: Add Address Module HERE --}}
                                <li class="list-group-item text-dark" onclick="window.location.href='{{ route('store-kyc-bank') }}'">
                                    BANK DETAILS
                                    <span class="pull-right font-medium-2">
                                        @if($store_status->bank_account == \App\Models\StoreManager\StoreStatus::KYC_PENDING)
                                            <span class="badge bg-pink"><i class="la la-exclamation-triangle"></i> PENDING</span>
                                        @elseif($store_status->bank_account == \App\Models\StoreManager\StoreStatus::KYC_INPROGRESS)
                                            <span class="badge badge-warning"><i class="la la-refresh"></i> In Progress</span>
                                        @elseif($store_status->bank_account == \App\Models\StoreManager\StoreStatus::KYC_VERIFIED)
                                            <span class="badge badge-success"><i class="la la-check-circle"></i> Verified </span>
                                        @else
                                            <span class="badge badge-danger"><i class="la la-close"></i> Rejected</span>
                                        @endif
                                    </span>
                                </li>
                                <li class="list-group-item text-dark" onclick="window.location.href='{{ route('store-kyc-aadhar') }}'">
                                    AADHAAR NUMBER
                                    <span class="pull-right font-medium-2">
                                        @if($store_status->aadhar_card == \App\Models\StoreManager\StoreStatus::KYC_PENDING)
                                            <span class="badge bg-pink"><i class="la la-exclamation-triangle"></i> PENDING</span>
                                        @elseif($store_status->aadhar_card == \App\Models\StoreManager\StoreStatus::KYC_INPROGRESS)
                                            <span class="badge badge-warning"><i class="la la-refresh"></i> In Progress</span>
                                        @elseif($store_status->aadhar_card == \App\Models\StoreManager\StoreStatus::KYC_VERIFIED)
                                            <span class="badge badge-success"><i class="la la-check-circle"></i> Verified </span>
                                        @else
                                            <span class="badge badge-danger"><i class="la la-close"></i> Rejected</span>
                                        @endif
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-css')
    <style>
        .list-group-item:hover, .list-group-item:active, .list-group-item:focus {
            background: #f5f5f5;
            cursor: pointer;
        }
    </style>
@stop