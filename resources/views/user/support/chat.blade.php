@extends('user.template.layout')

@section('title', 'Support ticket for ' . $support->category->name)

@section('content')

    <div class="content-body">
        <div class="row">
            <div class="col-md-10">
                <section>
                    <div class="card chat-application">
                        <div class="card-content">
                            <div class="card-body" style="height: 100%;">
                                <section class="chat-app-window">
                                    <div class="mb-1 bg-pink white text-bold-700" style="padding: 0.5rem">
                                        Support ticket for  {{ $support->category->name }}
                                    </div>
                                    <div class="chats">
                                        @foreach($details as $detail)
                                            <div class="chat {{ $detail->type == \App\Models\Support::ADMIN_TO_USER ? 'chat-left' : null }}">
                                                <div class="chat-avatar">
                                                    <a class="avatar">
                                                        @if($detail->type == \App\Models\Support::ADMIN_TO_USER)
                                                            <img src="/user-assets/images/icons/support.svg" class="box-shadow-4">
                                                        @else
                                                            <img src="/user-assets/images/icons/user-tie.svg" class="box-shadow-4">
                                                        @endif
                                                    </a>
                                                </div>
                                                <div class="chat-body">
                                                    <div class="chat-content" style="padding: 0.5rem;">
                                                        @if($detail->image)
                                                            <a href="{{ $detail->image }}" target="_blank">
                                                                <img src="{{ $detail->image }}" style="width: 15rem; border-radius: 0.5rem;">
                                                            </a>
                                                        @endif
                                                        <span class="font-small-1 {{ $detail->type == \App\Models\Support::ADMIN_TO_USER ? 'text-black-50' : 'text-white-50' }}">
                                                            {{ $detail->created_at->format('M d, Y H:i A') }}
                                                        </span>
                                                        <p class="text-left">{{ $detail->message }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        @if($support->status == \App\Models\Support::CLOSED)
                                            <div class="mb-1 bg-pink white text-bold-700" style="padding: 0.5rem">
                                                Ticket is closed
                                            </div>
                                        @endif
                                    </div>
                                </section>
                                <section class="chat-app-form bg-white">
                                    @if($support->status == \App\Models\Support::OPEN)
                                        <form class="chat-app-input d-flex" action="" method="post" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <fieldset class="form-group col-7 m-0 p-0">
                                                <input type="text" name="message" class="form-control" placeholder="Type your message..." autocomplete="off" required>
                                            </fieldset>
                                            <fieldset class="form-group col-4 m-0 p-0">
                                                <input type="file" name="image" id="supportImage" placeholder="Image for Support Chat">
                                            </fieldset>
                                            <div class="form-group has-icon-left col-1 m-0 p-0">
                                                <button type="submit" class="btn btn-danger chat-btn">
                                                    <i class="la la-paper-plane-o d-xl-none"></i>
                                                    <span class="d-none d-lg-none d-xl-block">Send </span>
                                                </button>
                                            </div>
                                        </form>
                                    @endif
                                </section>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-2">
                <section>
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body text-center">
                                <div class="mb-1 secondary">Support ticket: <div class="text-bold-700">{{ $support->category->name }}</div></div>
                                @if($support->status == \App\Models\Support::OPEN)
                                    <a href="" class="btn btn-primary mb-1" title="Refresh">
                                        <i class="la la-refresh d-xl-none"></i>
                                        <span class="d-none d-lg-none d-xl-block">Refresh</span>
                                    </a>
                                @else
                                    <a href="{{ route('user-support-view') }}" class="btn btn-primary mb-1" title="Refresh">
                                        <i class="la la-refresh d-xl-none"></i>
                                        <span class="d-none d-lg-none d-xl-block">Create Support Ticket </span>
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')
    <script>
        var height = 0;
        $('div.chat').each(function(i, value){
            height += parseInt($(this).height());
        });

        height += '';

        $('section.chat-app-window').animate({scrollTop: height});

        $('#supportImage').filer({ limit: 1, maxSize: 2, extensions: ['jpg', 'jpeg', 'png'], changeInput: true, showThumbs: false });
    </script>
@stop

@section('import-javascript')
    <script src="/user-assets/plugins/jquery.filer/js/jquery.filer.min.js"></script>
@stop

@section('import-css')
    <link rel="stylesheet" href="/user-assets/plugins/jquery.filer/css/jquery.filer.css">
@stop
