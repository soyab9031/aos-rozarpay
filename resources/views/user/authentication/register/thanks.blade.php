@extends('user.template.registration.layout')

@section('title', 'Registration Completed at ')

@section('content')
    <section class="flexbox-container">
        <div class="col-12 d-flex align-items-center justify-content-center">
            <div class="col-md-4 col-10 box-shadow-2 p-0">
                <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                    <div class="card-header border-0">
                        <div class="text-center mb-1">
                            <img src="/user-assets/images/company/logo.png" alt="{{ config('project.brand') }}" width="200px">
                        </div>
                        @if(session('success'))
                            <div class="alert alert-success">{{ session('success') }}</div>
                        @endif
                        <h2 class="text-center">Welcome to {{ config('project.brand') }} family</h2>
                    </div>
                    <div class="card-content">

                        <div class="card-body">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Name: <span class="pull-right">{{ $user->detail->full_name }}</span>
                                </li>
                                <li class="list-group-item bg-dark text-white">
                                    Tracking ID: <span class="pull-right">{{ $user->tracking_id }}</span>
                                </li>
                                <li class="list-group-item">
                                    Username: <span class="pull-right">{{ $user->username }}</span>
                                </li>
                                <li class="list-group-item">
                                    Password: <span class="pull-right">{{ $user->password }}</span>
                                </li>
                            </ul>
                            <div class="text-center mt-2">
                                <a href="{{ route('user-login') }}" class="btn btn-glow btn-bg-gradient-x-red-pink mr-1 mb-1 continue">
                                    Visit Your Account <i class="la la-arrow-circle-o-right"></i>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@stop