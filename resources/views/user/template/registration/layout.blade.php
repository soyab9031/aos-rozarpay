<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="Tymk Softwares">
    <title>@yield('title') - {{ config('project.brand') }}</title>
    <link rel="apple-touch-icon" href="/user-assets/images/company/favicon.png">
    <link rel="shortcut icon" type="image/x-icon" href="/user-assets/images/company/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/fonts/feather/style.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/fonts/line-awesome/css/line-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/colors.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/components.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/style.css?v=oct2020v1">
    <link href="/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
    @yield('import-css')
    @yield('page-css')
    <style>
        .card-body {  padding: 1rem;  }
        label{
            color: #fff !important;
        }
        ::-webkit-input-placeholder { /* Chrome */
            color: #fff !important;
        }
        :-ms-input-placeholder { /* IE 10+ */
            color: #fff !important;
        }
        ::-moz-placeholder { /* Firefox 19+ */
            color: #fff !important;
            opacity: 1;
        }
        :-moz-placeholder { /* Firefox 4 - 18 */
            color: #fff !important;
            opacity: 1;
        }
        ul li{
            background: #4E4E50 !important;
        }
    </style>
</head>
<body class="vertical-layout vertical-menu 1-column bg-full-screen-image menu-expanded blank-page blank-page" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="1-column">
@if(env('APP_ENV') == 'local')
    <img style="position: fixed;z-index: 99999;width: 150px;margin: auto;top: -2px;left: 0;right: 0;opacity: 0.75;" src="/user-assets/images/ribbon.png" alt="TYMK Software Dev Mode">
@endif
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
        </div>
        <div class="content-body">
            @yield('content')
        </div>
    </div>
</div>
<script src="/user-assets/js/vendors.min.js" type="text/javascript"></script>
<script src="/user-assets/js/app-menu.js" type="text/javascript"></script>
<script src="/user-assets/js/app.js?v=oct2020V1" type="text/javascript"></script>
<script src="/user-assets/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<script src="/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="/plugins/sweetalert/sweetalert.min.js"></script>
@yield('page-javascript')
</body>
</html>
