<nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="collapse navbar-collapse show" id="navbar-mobile">
                <ul class="nav navbar-nav mr-auto float-left">
                    <li class="nav-item mobile-menu d-md-none mr-auto">
                        <a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#">
                            <i class="ft-menu font-large-1"></i>
                        </a>
                    </li>
                    <li class="nav-item d-none d-md-block">
                        <a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a>
                    </li>
                </ul>
                <ul class="nav navbar-nav float-right">
                    <li class="dropdown dropdown-notification nav-item mr-1">
                        <a class="nav-link nav-link-label" href="{{ route('website-cart') }}" target="_blank">
                            <i class="ficon ft-shopping-cart bell-shake" id="notification-navbar-link"></i>
                            <span class="badge badge-pill badge-sm badge-success badge-up badge-glow">
                                {{ Session::get('cart') ? count(Session::get('cart')) : 0 }}
                            </span></a>
                    </li>
                    <li class="dropdown dropdown-user nav-item">
                        {{-- Google Translate --}}
                        <div id="google_translate_element"></div>
                    </li>
                    <li class="dropdown dropdown-user nav-item">
                        <a class="dropdown-toggle nav-link dropdown-user-link text-center" href="#"
                           data-toggle="dropdown">
                            <span class="avatar avatar-outline" style="width: 45px!important;">
                                 @if(Session::get('user')->detail->image == null)
                                    <img src="/user-assets/images/icons/user-tie.svg" alt="">
                                @else
                                    <img height="100px"
                                         src="{{ env('USER_PROFILE_IMAGE_URL').Session::get('user')->detail->image }}"
                                         alt="">
                                @endif
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <div class="arrow_box_right">
                                <a class="dropdown-item" href="#">
                                   <span class="text-bold-700">
                                       {{ \Str::limit(Session::get('user')->detail->first_name, 15) }}
                                   </span>
                                </a>
                                <a class="dropdown-item header-tracking-id" href="javascript:void(0)"
                                   title="Click to copy" data-clipboard-text="{{ Session::get('user')->tracking_id }}"
                                   onclick="INGENIOUS.copyToClipboard('.header-tracking-id', 'Tracking ID has been copied to clipboard')">
                                    TID: {{ Session::get('user')->tracking_id }}
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('user-account-profile') }}"><i
                                            class="ft-user"></i> Edit Profile</a>
                                <a class="dropdown-item" href="{{ route('user-logout') }}"><i class="ft-power"></i>
                                    Logout</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>

