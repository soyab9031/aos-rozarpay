@extends('user.template.layout')

@section('title', 'Receipt')

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Purchase Receipt</h3>
        </div>
    </div>

    <div class="content-body">
        <section class="card">

            <div id="invoice-template" class="card-body">

                <div id="invoice-company-details" class="row">
                    <div class="col-md-6 col-sm-12 text-left text-md-left">
                        <img src="/user-assets/images/company/logo.png" alt="{{ config('project.brand') }}" class="mb-2" width="100">
                        <ul class="px-0 list-unstyled">
                            <li class="text-bold-700">{{ collect($setting->value)->where('title','company_name')->first()->value }}</li>
                            <li>{{ collect($setting->value)->where('title','address')->first()->value }}</li>
                            <li>{{ collect($setting->value)->where('title','city')->first()->value }} - {{ collect($setting->value)->where('title','pincode')->first()->value }},</li>
                            <li>{{ collect($setting->value)->where('title','state')->first()->value }}, {{ collect($setting->value)->where('title','country')->first()->value }}</li>
                            <li>GST: {{ collect($setting->value)->where('title','gst_no')->first()->value }}</li>
                        </ul>

                    </div>
                    <div class="col-md-6 col-sm-12 text-center text-md-right">
                        <h2>RECEIPT</h2>
                    </div>
                </div>

                <!-- Invoice Items Details -->
                <div id="invoice-items-details" class="pt-2">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="pull-left m-t-30">
                                <p><b>Customer Name</b></p>
                                <address class="line-h-24">
                                    {{ $user->detail->full_name }} <br>
                                    {{ $user->address->address }} <br>
                                    {{ $user->address->city }} - {{ $user->address->state->name }}
                                    - {{ $user->address->pincode }} <br>
                                    Contact: {{ $user->mobile }}
                                </address>
                            </div>
                        </div>
                        <div class="col-md-3 offset-md-3 offset-lg-3">
                            <div class="mt-2 text-right">
                                <p><b>Order Date:</b> {{ $user->created_at->format('M d, Y') }}</p>
                                <p><b>Order Status:</b> <span class="badge badge-danger">Paid</span></p>
                                <p><b>ID:</b> {{ $user->tracking_id }}</p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Item</th>
                                    <th>GST</th>
                                    <th>HSN/SAC</th>
                                    <th>Qty</th>
                                    <th>Rate/Item</th>
                                    <th>SGST</th>
                                    <th>CGST</th>
                                    <th class="text-right">Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($user->package->items as $index => $item)
                                    <tr>
                                        <td>{{ $index+1 }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->gst->percentage }}%</td>
                                        <td>{{ $item->gst->code }}</td>
                                        <td>1</td>
                                        <td>{{ number_format($item->actual_amount, 2) }}</td>
                                        <td>{{ number_format($item->tax_amount/2, 2) }}</td>
                                        <td>{{ number_format($item->tax_amount/2, 2) }}</td>
                                        <td class="text-right">{{ number_format($item->amount, 2) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-5 offset-7">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td>Taxable Amount</td>
                                        <td class="text-right">Rs. {{ number_format(collect($user->package->items)->sum('actual_amount'), 2) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Total Tax</td>
                                        <td class="text-right">Rs. {{ number_format(collect($user->package->items)->sum('tax_amount'), 2) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-bold-800">Total</td>
                                        <td class="pink text-bold-800 text-right">
                                            Rs. {{ number_format(collect($user->package->items)->sum('amount'), 2) }}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-12 text-center">
                            <a href="javascript:window.print()" class="btn btn-instagram btn-lg my-1 btn-print"><i class="la la-print"></i>
                                Print Receipt
                            </a>
                            <p>
                                <small class="text-danger">*This is Computer generated receipt.No Signature or Stamp is required.</small>
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>
@stop