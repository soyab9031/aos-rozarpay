@extends('user.template.layout')

@section('title', 'My Identity Card')

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">My Identity Card</h3>
        </div>
    </div>

    <div class="content-body">
        <section class="card">
            <div class="row">
                <div class="col-md-6 offset-md-3 offset-lg-3 p-1">
                    <div class="identity-card">
                        <div class="header">
                            <div class="logo">
                                <img alt="Ingenious" src="/user-assets/images/company/logo.png">
                            </div>
                        </div>
                        <div class="body">
                            <div class="user-image">
                                @if($user->detail->image == null)
                                    <img src="/user-assets/images/icons/user-tie.svg" width="100" height="100" alt="" style="border-radius: 50%">
                                @else
                                    <img src="{{ env('USER_PROFILE_IMAGE_URL').$user->detail->image }}" width="100" height="100" alt="" style="border-radius: 50%">
                            @endif
                            </div>
                            <div class="identity-card-content">
                                <h4 class="text-center mb-2">Identity Card</h4>
                                <ul>
                                    <li><b>Name:</b> {{ $user->detail->full_name }}</li>
                                    <li><b>Company Id:</b> {{ $user->tracking_id }}</li>
                                    <li><b>Mobile:</b> {{ $user->mobile }}</li>
                                    <li><b>Address:</b> {{ $user->address->address }}</li>
                                    <li><b>City:</b> {{ $user->address->city }}</li>
                                </ul>
                            </div>
                            <div class="identity-card-content-footer text-center">
                                {{ \Str::replaceLast('/', '', str_replace('http://', 'www.', config('project.url'))) }}
                            </div>
                        </div>
                    </div>
                    <div class="text-center mt-3 mb-3">
                        <a href="javascript:window.print()" class="btn btn-facebook btn-print"><i class="la la-print"></i>Print</a>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
