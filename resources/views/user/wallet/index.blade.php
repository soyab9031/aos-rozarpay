@extends('user.template.layout')

@section('title')
    @if($type)
        Wallet Details of {{ strtoupper(str_replace('-', ' ', $type)) }}
    @else
        Wallet Report
    @endif
@stop

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">
                @if($type)
                    Wallet of {{ strtoupper(str_replace('-', ' ', $type)) }}
                @else
                    Wallet Report
                @endif
            </h3>
        </div>
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Created At</th>
                                <th>Income Type</th>
                                <th>Total</th>
                                <th class="bg-purple bg-lighten-4 black">TDS</th>
                                <th class="bg-purple bg-lighten-4 black">Admin</th>
                                <th>Amount</th>
                                <th width="20%">Remarks</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($wallets->count() == 0)
                                <tr>
                                    <td colspan="10" class="text-center">
                                        Right Now, You have no any wallet information.<br>
                                        After Core Calculations, {{ strtoupper(str_replace('-', ' ', $type)) }} Income will be displayed here...!!
                                    </td>
                                </tr>
                            @endif
                            @foreach($wallets as $index => $wallet)
                                <tr>
                                    <td>{{ \App\Library\Helper::tableIndex($wallets, $index) }}</td>
                                    <td>{{ $wallet->created_at->format('M d, Y') }}</td>
                                    <td>{{ $wallet->income_type_name }}</td>
                                    <td>{{ $wallet->total }}</td>
                                    <td>{{ $wallet->tds }}</td>
                                    <td>{{ $wallet->admin_charge }}</td>
                                    <td>{{ $wallet->amount }}</td>
                                    <td>{{ $wallet->remarks }}</td>
                                </tr>
                            @endforeach
                            <tr class="bg-danger bg-darken-4 text-white">
                                <td colspan="3" class="text-right">Total</td>
                                <td>{{ $wallets->sum('total') }}</td>
                                <td>{{ $wallets->sum('tds') }}</td>
                                <td>{{ $wallets->sum('admin_charge') }}</td>
                                <td>{{ $wallets->sum('amount') }}</td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                        {{ $wallets->appends(['type' => $type])->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
