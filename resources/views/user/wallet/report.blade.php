@extends('user.template.layout')

@section('title')
    Total Wallet Report
@stop

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Total Wallet Report</h3>
        </div>
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th colspan="10">
                                    <span class="success"><i class="la la-square"></i> Credit </span>
                                    <span class="danger"><i class="la la-square"></i> Debit</span>
                                </th>
                            </tr>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Total</th>
                                <th class="bg-purple bg-lighten-4 black">TDS</th>
                                <th class="bg-purple bg-lighten-4 black">Admin</th>
                                <th>Amount</th>
                                <th>Remarks</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($wallets->count() == 0)
                                <tr>
                                    <td colspan="10" class="text-center">
                                        Right Now, You have no any wallet information.<br>
                                        After Core Calculations, Income will be displayed here...!!
                                    </td>
                                </tr>
                            @endif
                            @foreach($wallets as $index => $wallet)
                                <tr class="{{ $wallet->type == 1 ? 'bg-success' : 'bg-danger' }} bg-lighten-3">
                                    <td>{{ \App\Library\Helper::tableIndex($wallets, $index) }}</td>
                                    <td>{{ $wallet->created_at->format('M d, Y') }}</td>
                                    <td>{{ $wallet->total }}</td>
                                    <td>{{ $wallet->tds }}</td>
                                    <td>{{ $wallet->admin_charge }}</td>
                                    <td>{{ $wallet->amount }}</td>
                                    <td>{{ $wallet->remarks }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $wallets->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
