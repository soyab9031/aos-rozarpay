@extends('user.template.layout')

@section('title')
    My Team List of {{ $request_leg == 'L' ? 'Left' : 'Right' }} Leg
@stop

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">My Team List of {{ $request_leg == 'L' ? 'Left' : 'Right' }} Leg</h3>
        </div>
    </div>
    <div class="content-body">
        <section>
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <form action="" method="get">
                            <div class="row">
                                <div class="col-md-5">
                                    <label>Search Team Member at {{ $request_leg == 'L' ? 'Left' : 'Right' }} Leg</label>
                                    <input type="text" class="form-control" name="search" placeholder="Search Keyword: Username or Tracking Id" value="{{ Request::get('search') }}">
                                </div>
                                <div class="col-md-2" style="margin-top: 2rem;">
                                    <button class="btn btn-block btn-glow btn-bg-gradient-x-website-red">Search</button>
                                </div>
                                <div class="col-md-1" style="margin-top: 2rem;">
                                    <a href="{{ route('user-business-children', ['leg' => $request_leg]) }}" title="Refresh" class="btn btn-icon btn-primary mr-1"><i class="ft-refresh-ccw"></i></a>
                                </div>
                                <div class="col-4 text-right">
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a href="{{ route('user-business-children', ['leg' => 'L']) }}" class="btn {{ $request_leg == 'L' ? 'btn-danger' : 'btn-outline-danger' }}">
                                            Left Leg
                                        </a>
                                        <a href="{{ route('user-business-children', ['leg' => 'R']) }}" class="btn {{ $request_leg == 'R' ? 'btn-primary' : 'btn-outline-primary' }}">Right Leg</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header p-1">
                    <h4 class="card-title float-left"> {{ $user->detail->full_name }}
                        <span class="blue-grey lighten-2 font-small-3 mb-0">({{ $user->tracking_id }})</span>
                    </h4>
                    <span class="float-right m-0">
                        Activated At: <span class="badge badge-pill badge-info">
                            {{ $user->paid_at ? \Carbon\Carbon::parse($user->paid_at)->format('M d, Y') : 'N.A' }}
                        </span>
                    </span>
                </div>
                <div class="card-content collapse show">
                    <div class="card-footer text-center p-1">
                        <div class="row">
                            <div class="col-md-3 col-12 border-right-blue-grey border-right-lighten-5 text-center">
                                <p class="blue-grey lighten-2 mb-0">Left Members</p>
                                <p class="font-medium-5 text-bold-400">
                                    {{ $user_binary_total ? number_format($user_binary_total->left) : 0 }}
                                </p>
                            </div>
                            <div class="col-md-3 col-12 border-right-blue-grey border-right-lighten-5 text-center">
                                <p class="blue-grey lighten-2 mb-0">Right Members</p>
                                <p class="font-medium-5 text-bold-400">
                                    {{ $user_binary_total ? number_format($user_binary_total->right) : 0 }}
                                </p>
                            </div>
                            <div class="col-md-3 col-12 border-right-blue-grey border-right-lighten-5 text-center">
                                <p class="blue-grey lighten-2 mb-0">Total Left PV</p>
                                <p class="font-medium-5 text-bold-400">
                                    {{ $user_binary_total ? number_format($user_binary_total->total_left) : 0 }}
                                </p>
                            </div>
                            <div class="col-md-3 col-12 text-center">
                                <p class="blue-grey lighten-2 mb-0">Total Right PV</p>
                                <p class="font-medium-5 text-bold-400">
                                    {{ $user_binary_total ? number_format($user_binary_total->total_right) : 0 }}
                                </p>
                            </div>
                        </div>
                        @if($user->sponsorBy)
                            <hr>
                            <span class="text-muted">
                                <a href="#" class="danger darken-2">Sponsor Upline</a>
                                {{ $user->sponsorBy->detail->full_name }} ({{ $user->sponsorBy->tracking_id }})
                            </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Tree</th>
                                    <th>Joined At</th>
                                    <th>Activated At</th>
                                    <th>Package</th>
                                    <th>Name</th>
                                    <th>Sponsor Upline</th>
                                    <th>Placement Under</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($children) == 0)
                                    <tr>
                                        <td colspan="8" class="text-center">
                                            No Children available for Listing, If You have then please wait, Report will be generated soon. You can visit
                                            <a href="{{ route('user-business-binary-tree') }}">Tree</a> for better view
                                        </td>
                                    </tr>
                                @endif
                                @foreach($children as $index => $child)
                                    <tr style="font-size: 0.9rem !important;">
                                        <td>{{ \App\Library\Helper::tableIndex($children, $index) }}</td>
                                        <td>
                                            <a href="{{ route('user-business-binary-tree', ['tracking_id' => $child->user->tracking_id ]) }}" target="_blank"
                                               class="btn btn-social-icon text-danger btn-outline-adn btn-sm" title="Tree View"><span class="text-danger la la-sitemap"></span></a>
                                        </td>
                                        <td>{{ $child->user->created_at->format('M d, Y') }}</td>
                                        <td>{{ $child->user->paid_at ? \Carbon\Carbon::parse($child->user->paid_at)->format('M d, Y') : 'N.A' }}</td>
                                        <td>
                                            @if($child->user->package)
                                                {{ $child->user->package->name }} <br> (Rs. {{ $child->user->package->amount }})
                                            @else
                                                <span class="text-danger">Not Available</span>
                                            @endif
                                        </td>
                                        <td class="text-primary">
                                            {{ $child->user->detail->full_name }} <br> (ID: {{ $child->user->tracking_id }})
                                        </td>
                                        <td>{{ $child->user->sponsorBy->detail->full_name }} ({{ $child->user->sponsorBy->tracking_id }})</td>
                                        <td>{{ $child->user->parentBy->detail->full_name }} ({{ $child->user->parentBy->tracking_id }})</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @if(count($children) > 0)
                                {{ $children->appends(['search' => Request::get('search')])->links() }}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop