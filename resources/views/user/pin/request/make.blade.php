@extends('user.template.layout')

@section('title', 'Make Pin Request')

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Make Pin Request</h3>
        </div>
        <div class="content-header-left col-md-8 col-12 mb-2">
            <a href="{{ route('user-pin-request') }}" class="btn btn-primary btn-sm pull-right"><i
                        class="ft-corner-up-left"></i> Back</a>
        </div>
    </div>
    <div class="content-body">
        <form action="" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">
                                Pin Request General Details
                            </h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 ">
                                        <label for="">Pin Qty</label>
                                    </div>
                                    <div class="col-md-6 ">
                                        <label for="">Package Name</label>
                                    </div>
                                </div>
                                @foreach($packages as $index => $package)
                                    <div class="row mb-1">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select class="form-control" name="pin[{{ $index }}][qty]">
                                                    <option value="0">Select Pin Qty</option>
                                                    @for($i=1; $i <= 25; $i++)
                                                        <option value="{{ $i }}" {{ old('pin_quantity') == $i ? 'selected' : '' }} >{{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="hidden" name="pin[{{ $index }}][package_id]"
                                                   value="{{ $package->id }}">
                                            <input type="hidden" name="pin[{{ $index }}][package_name]"
                                                   value="{{ $package->name }}">
                                            <input type="hidden" name="pin[{{ $index }}][package_amount]"
                                                   value="{{ $package->amount }}">
                                            <input type="text" class="form-control"
                                                   value="{{ $package->name }} (Rs. {{ $package->amount }})" readonly>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">
                                Proof of Deposit
                            </h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Bank Name</label>
                                            <input type="text" name="bank_name" class="form-control" autocomplete="off"
                                                   value="{{ old('bank_name') }}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Reference Number</label>
                                            <input type="text" name="reference_number" class="form-control"
                                                   placeholder="Enter DD or Cheque No. or Reference No."
                                                   autocomplete="off" value="{{ old('reference_number') }}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Payment Mode</label>
                                            <select class="form-control" name="payment_mode">
                                                @foreach($payment_modes as $payment_mode)
                                                    <option value="{{ $payment_mode }}" {{ old('payment_mode') == $payment_mode ? 'selected' : '' }}>
                                                        {{ $payment_mode }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Deposit Date</label>
                                            <div class="input-group">
                                                <input type='text' class="form-control deposit_date" name="deposit_date"
                                                       value="{{ old('deposit_date') }}" placeholder="Deposit Date"
                                                       readonly>
                                                <div class="input-group-append">
										<span class="input-group-text">
											<span class="la la-calendar-o"></span>
										</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Deposit Time</label>
                                            <div class="input-group">
                                                <input type='text' class="form-control timepicker" name="deposit_time"
                                                       value="{{ old('deposit_time') }}" placeholder="Deposit Time"
                                                       readonly>
                                                <div class="input-group-append">
										<span class="input-group-text">
											<span class="la la-clock-o"></span>
										</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <fieldset class="form-group mt-1">
                                    <label>Upload Cash Receipt | NEFT Transfer Receipt | Cheque Copy Receipt</label>
                                    <input type="file" name="image" id="image">
                                    <p class="text-left">
                                        <small class="text-muted">Allowed File Formats : .jpg, .jpeg, .png and Maximum
                                            File Size Allowed : 2MB
                                        </small>
                                    </p>
                                </fieldset>
                                <div class="text-center m-t-10">
                                    <button class="btn btn-danger btn-md">Make Request</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop

@section('import-javascript')
    <script src="/user-assets/plugins/jquery.filer/js/jquery.filer.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
@stop

@section('import-css')
    <link rel="stylesheet" href="/user-assets/plugins/jquery.filer/css/jquery.filer.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
@stop

@section('page-javascript')
    <script>
        $('#image').filer({
            limit: 1,
            maxSize: 15,
            extensions: ['jpg', 'jpeg', 'png'],
            changeInput: true,
            showThumbs: true
        });

        $(".deposit_date").datepicker({
            autoclose: true,
            format: "dd-mm-yyyy",
            startDate: '',
            endDate: "-5m"
        });
        $('.timepicker').timepicker({});
    </script>
@stop
