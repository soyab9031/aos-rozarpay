<?php

use Illuminate\Database\Seeder;

class SupportCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            "Undelivered Cheque",
            "Commission Cheque Query",
            "Pan Card",
            "Address Update",
            "Name Correction",
            "Upgrade Package",
            "Account/Site Activation",
            "Transfer",
            "Renewal",
            "Pin Request",
            "TDS/TAX",
            "Stop Payment",
            "Online Transfer/NEFT",
            "Lost Password",
            "Register e-Mail Id/Phone",
            "Other"
        ];

        collect($categories)->map( function ($category) {

            \App\Models\SupportCategory::create([
               'name' => $category
            ]);

        });
    }
}
