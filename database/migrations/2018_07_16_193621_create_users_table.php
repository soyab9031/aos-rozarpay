<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->integer('sponsor_by')->unsigned()->nullable();
            $table->string('tracking_id', 10)->unique();
            $table->string('username', 100)->unique();
            $table->string('password');
            $table->string('wallet_password');
            $table->integer('pin_id')->unsigned()->nullable();
            $table->integer('wallet_id')->unsigned()->nullable();
            $table->string('leg', 10)->nullable();
            $table->string('email', 100);
            $table->string('mobile', 20);
            $table->integer('package_id')->unsigned()->nullable();
            $table->float('joining_amount', 12, 2)->default(0);
            $table->string('token', 100)->nullable();
            $table->string('last_logged_in_ip')->nullable();
            $table->dateTime('last_logged_in_at')->nullable();
            $table->dateTime('paid_at')->nullable();
            $table->dateTime('activated_at')->nullable();
            $table->timestamps();
            $table->index('wallet_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
