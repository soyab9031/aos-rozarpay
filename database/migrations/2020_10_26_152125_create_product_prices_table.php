<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->index()->unsigned();
            $table->string('code',10)->unique();
            $table->text('images')->nullable();
            $table->float('price', 10, 2);
            $table->float('distributor_price', 10, 2);
            $table->float('points', 10, 2);
            $table->integer('qty')->default(0);
            $table->string('barcode')->nullable();
            $table->string('gst')->comment('Json: percentage & hsn/sac code');
            $table->tinyInteger('deal')->comment('1: Deal, 2: New Arrival, 3:Recommended')->default(0);
            $table->tinyInteger('new_arrival')->comment('1: Deal, 2: New Arrival, 3:Recommended')->default(0);
            $table->tinyInteger('recommended')->comment('1: Deal, 2: New Arrival, 3:Recommended')->default(0);
            $table->tinyInteger('status')->comment('1: Active, 2: Inactive')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_prices');
    }
}
