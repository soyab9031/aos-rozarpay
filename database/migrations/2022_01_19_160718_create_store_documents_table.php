<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_documents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('store_id')->unsigned();
            $table->string('image');
            $table->string('secondary_image')->nullable();
            $table->tinyInteger('type')->comment('1: PanCard, 2: Aadhar Card, 3: Cancel Cheque, 4: Bank Passbook');
            $table->tinyInteger('status')->comment('1: Pending, 2: Verified, 3: Rejected')->default(1);
            $table->text('remarks')->nullable();
            $table->string('number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_documents');
    }
}
