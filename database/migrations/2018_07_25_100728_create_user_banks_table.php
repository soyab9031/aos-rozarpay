<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_banks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->string('bank_name',100)->nullable();
            $table->string('account_name',100)->nullable();
            $table->string('account_number',100)->nullable();
            $table->string('branch')->nullable();
            $table->string('ifsc', 20)->nullable();
            $table->string('city')->nullable();
            $table->tinyInteger('type')->comment('1: Saving, 2: Current')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_banks');
    }
}
