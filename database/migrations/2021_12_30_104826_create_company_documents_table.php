<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_documents', function (Blueprint $table) {
            $table->Increments('id');
            $table->integer('admin_id')->unsigned()->nullable();
            $table->string('name');
            $table->string('document');
            $table->tinyInteger('type')->comment('1: Pdf, 2: Images')->default(0);
            $table->tinyInteger('status')->comment('2: Inactive, 1: Active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_documents');
    }
}
