<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tracking_id', 10)->unique();
            $table->integer('user_id')->index()->unsigned()->nullable();
            $table->string('name',100);
            $table->string('email', 100);
            $table->string('mobile', 20)->unique();
            $table->date('birth_date');
            $table->tinyInteger('gender')->nullable()->comment('1: Male, 2: Female');
            $table->string('password');
            $table->string('last_logged_in_ip')->nullable();
            $table->dateTime('last_logged_in_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
