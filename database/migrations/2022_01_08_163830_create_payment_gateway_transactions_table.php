<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentGatewayTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_gateway_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->index();
            $table->string('provider', 100);
            $table->string('reference_id', 100)->nullable();
            $table->text('response')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1: Pending, 2: Success, 3: Failed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_gateway_transactions');
    }
}
