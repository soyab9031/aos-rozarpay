<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_statuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('store_id')->unsigned();
            $table->tinyInteger('bank_account')->comment('1: Pending, 2: Verified, 3:InProgress, 4:Rejected')->default(1);
            $table->tinyInteger('pancard')->comment('1: Pending, 2: Verified, 3:InProgress, 4:Rejected')->default(1);
            $table->tinyInteger('aadhar_card')->comment('1: Pending, 2: Verified, 3:InProgress, 4:Rejected')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_statuses');
    }
}
