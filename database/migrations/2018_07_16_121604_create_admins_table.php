<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('email', 100)->unique();
            $table->string('password');
            $table->string('mobile', 20);
            $table->string('token', 100);
            $table->string('last_logged_in_ip', 100)->nullable();
            $table->dateTime('last_logged_in_at')->nullable();
            $table->tinyInteger('type')->comment('1: Master, 2: Manager, 3: Employee');
            $table->tinyInteger('status')->default('1')->comment('1: Active, 2: Inactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('admins');
    }
}
